/*
Copyright (C) 1996-1997 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "quakedef.h"
#include "winquake.h"
#include "menu.h"
#include "teamplay.h"
#include "version.h"

// OfN
#include "gl_local.h"
#include "OfteN.h" // June 2023
#include "time.h" // June 2023
#ifdef _WIN32
#define strcasecmp(s1, s2) _stricmp  ((s1),   (s2))
#endif

#include "config_manager.h"

void SCR_RSShot_f (void);
void CL_ProcessServerInfo (void);
void SV_Serverinfo_f (void);
void Key_WriteBindings (FILE *f);
void S_StopAllSounds (qboolean clear);
void CL_Integration_f(void);

// OfN
char Integration_Weather_str[256];
char Integration_Fog_str[256];
char Integration_Sky_str[256];
char Integration_Shader_str[256]; // October 2021
char Integration_Misc_str[256]; // November 2023
#define MAX_INTEGRATION_TOKEN_STRLEN 32


//adds the current command line as a clc_stringcmd to the client message.
//things like kill, say, etc, are commands directed to the server,
//so when they are typed in at the console, they will need to be forwarded.
void Cmd_ForwardToServer (void) {
	char *s;

	if (cls.state == ca_disconnected) {
		Com_Printf ("Can't \"%s\", not connected\n", Cmd_Argv(0));
		return;
	}

	MSG_WriteByte (&cls.netchan.message, clc_stringcmd);
	// lowercase command
	for (s = Cmd_Argv(0); *s; s++)
		*s = (char) tolower(*s);
	SZ_Print (&cls.netchan.message, Cmd_Argv(0));
	if (Cmd_Argc() > 1) {
		SZ_Print (&cls.netchan.message, " ");
		SZ_Print (&cls.netchan.message, Cmd_Args());
	}
}

// don't forward the first argument
void CL_ForwardToServer_f (void) {
	// Added by VVD { OfN from EZQuake June 2023
	char* server_string, client_time_str[9];
	int		i, server_string_len;
	extern cvar_t	cl_crypt_rcon;
	time_t		client_time;
	// Added by VVD }

	if (cls.state == ca_disconnected) {
		Com_Printf ("Can't \"%s\", not connected\n", Cmd_Argv(0));
		return;
	}

	if (Q_strcasecmp(Cmd_Argv(1), "snap") == 0) {
		SCR_RSShot_f ();
		return;
	}

	// OfN October 2020 - Commented out in February 2021 - Readded in May 2023
	if (Q_strcasecmp(Cmd_Argv(1), "fileul") == 0) {
		CL_StartFileUpload();
		return;
	}
	// END OfN

	if (cls.demoplayback)
		return;		// not really connected

	if (Cmd_Argc() > 1) {
		MSG_WriteByte (&cls.netchan.message, clc_stringcmd);

		// Code from EZQuake June 2023
		// Added by VVD {
		if (cl_crypt_rcon.value && strcasecmp(Cmd_Argv(1), "techlogin") == 0 && Cmd_Argc() > 2)
		{
			time(&client_time);
			for (client_time_str[0] = i = 0; i < sizeof(client_time); i++) {
				char tmp[3];
				snprintf(tmp, sizeof(tmp), "%02X", (unsigned int)((client_time >> (i * 8)) & 0xFF));
				strlcat(client_time_str, tmp, sizeof(client_time_str));
			}

			server_string_len = Cmd_Argc() + strlen(Cmd_Argv(1)) + DIGEST_SIZE * 2 + 16;
			for (i = 3; i < Cmd_Argc(); ++i)
				server_string_len += strlen(Cmd_Argv(i));
			server_string = (char*)malloc(server_string_len);

			SHA1_Init();
			SHA1_Update((unsigned char*)Cmd_Argv(1));
			SHA1_Update((unsigned char*)" ");
			SHA1_Update((unsigned char*)Cmd_Argv(2));
			SHA1_Update((unsigned char*)client_time_str);
			SHA1_Update((unsigned char*)" ");
			for (i = 3; i < Cmd_Argc(); ++i)
			{
				SHA1_Update((unsigned char*)Cmd_Argv(i));
				SHA1_Update((unsigned char*)" ");
			}

			snprintf(server_string, server_string_len, "%s %s%s ",
				Cmd_Argv(1), SHA1_Final(), client_time_str);
			for (i = 3; i < Cmd_Argc(); ++i)
			{
				strlcat(server_string, Cmd_Argv(i), server_string_len);
				strlcat(server_string, " ", server_string_len);
			}
			SZ_Print(&cls.netchan.message, server_string);
			free(server_string);
		}
		else
			// Added by VVD }

		SZ_Print (&cls.netchan.message, Cmd_Args());
	}
}

//Handles both say and say_team
void CL_Say_f (void) {
	char *s;
	int tmp;
	qboolean qizmo = false;

	if (Cmd_Argc() < 2)
		return;

	if (cls.state == ca_disconnected) {
		Com_Printf ("Can't \"%s\", not connected\n", Cmd_Argv(0));
		return;
	}

	MSG_WriteByte (&cls.netchan.message, clc_stringcmd);
	// lowercase command
	for (s = Cmd_Argv(0); *s; s++)
		*s = (char) tolower(*s);
	SZ_Print (&cls.netchan.message, Cmd_Argv(0));

	if (CL_ConnectedToProxy()) {	
		for (s = Cmd_Argv(1); *s == ' '; s++)
			;
		if (!strncmp(s, ".stuff", 6) || !strncmp(s, ",stuff", 6) || strstr(s, ":stuff"))
			return;		

		qizmo = (!strncmp(s, "proxy:", 6) || s[0] == ',' || s[0] == '.');
	}

	
	if (!qizmo && !cl.paused && cl_floodprot.value && cl_fp_messages.value && cl_fp_persecond.value) {
		tmp = cl.whensaidhead - cl_fp_messages.value + 1;
		if (tmp < 0)
			tmp += 10;
		if (cl.whensaid[tmp] && (cls.realtime - cl.whensaid[tmp]) < (1.02 * cl_fp_persecond.value)) {
			Com_Printf("Flood Protection\n");
			return;
		}
	}
	

	SZ_Print (&cls.netchan.message, " ");

	s = TP_ParseMacroString (Cmd_Args());
	s = TP_ParseFunChars (s, true);
	if (*s && *s < 32) {
		SZ_Print (&cls.netchan.message, "\"");
		SZ_Print (&cls.netchan.message, s);
		SZ_Print (&cls.netchan.message, "\"");
	} else {
		SZ_Print (&cls.netchan.message, s);
	}

	
	if (!qizmo) {
		cl.whensaidhead++;
		if (cl.whensaidhead > 9)
			cl.whensaidhead = 0;
		cl.whensaid[cl.whensaidhead] = cls.realtime;
	}
	
}

void CL_Pause_f (void) {
	if (cls.demoplayback)
		cl.paused ^= PAUSED_DEMO;
	else
		Cmd_ForwardToServer();
}

//packet <destination> <contents>
//Contents allows \n escape character
void CL_Packet_f (void) {
	char send[2048], *in, *out;
	int i, l;
	netadr_t adr;

	if (Cmd_Argc() != 3) {
		Com_Printf ("packet <destination> <contents>\n");
		return;
	}

	if (!NET_StringToAdr (Cmd_Argv(1), &adr)) {
		Com_Printf ("Bad address\n");
		return;
	}

	if (adr.port == 0)
		adr.port = BigShort (PORT_SERVER);

	in = Cmd_Argv(2);
	out = send + 4;
	send[0] = send[1] = send[2] = send[3] = 0xff;

	l = strlen (in);
	for (i = 0; i < l; i++) {
		if (in[i] == '\\' && in[i+1] == 'n') {
			*out++ = '\n';
			i++;
		} else {
			*out++ = in[i];
		}
	}
	*out = 0;

	NET_SendPacket (NS_CLIENT, out-send, send, adr);
}

//Send the rest of the command line over as an unconnected command.
void CL_Rcon_f (void) {
	char message[1024] = {0};
	int i;
	netadr_t to;
	extern cvar_t rcon_password, rcon_address;

	message[0] = 255;
	message[1] = 255;
	message[2] = 255;
	message[3] = 255;
	message[4] = 0;

	/* ORIGINAL CODE BEFORE JUNE 2023
	strncat (message, "rcon ", sizeof(message) - strlen(message) - 1);

	if (rcon_password.string[0]) {
		strncat (message, rcon_password.string, sizeof(message) - strlen(message) - 1);
		strncat (message, " ", sizeof(message) - strlen(message) - 1);
	}

	for (i = 1; i < Cmd_Argc(); i++) {
		strncat (message, Cmd_Argv(i), sizeof(message) - strlen(message) - 1);
		strncat (message, " ", sizeof(message) - strlen(message) - 1);
	}*/

	// Code from EZQuake June 2023
	char client_time_str[9];
	time_t client_time;
	int i_from;
	//extern cvar_t cl_crypt_rcon;
	strlcat(message, "rcon ", sizeof(message));

	// Added by VVD {
	if (cl_crypt_rcon.value)
	{
		time(&client_time);
		for (client_time_str[0] = i = 0; i < sizeof(client_time); i++) {
			char tmp[3];
			snprintf(tmp, sizeof(tmp), "%02X", (unsigned int)((client_time >> (i * 8)) & 0xFF));
			strlcat(client_time_str, tmp, sizeof(client_time_str));
		}

		SHA1_Init();
		SHA1_Update((unsigned char*)"rcon ");
		if (rcon_password.string[0])
		{
			SHA1_Update((unsigned char*)rcon_password.string);
			SHA1_Update((unsigned char*)client_time_str);
			i_from = 1;
		}
		else // first arg must be pass in such case, so handle this
		{
			SHA1_Update((unsigned char*)Cmd_Argv(1));
			SHA1_Update((unsigned char*)client_time_str);
			i_from = 2;
		}
		SHA1_Update((unsigned char*)" ");
		for (i = i_from; i < Cmd_Argc(); i++)
		{
			SHA1_Update((unsigned char*)Cmd_Argv(i));
			SHA1_Update((unsigned char*)" ");
		}
		strlcat(message, SHA1_Final(), sizeof(message));
		strlcat(message, client_time_str, sizeof(message));
		strlcat(message, " ", sizeof(message));
	}
	else {
		i_from = 1;
		if (rcon_password.string[0]) {
			strlcat(message, rcon_password.string, sizeof(message));
			strlcat(message, " ", sizeof(message));
		}
	}
	for (i = i_from; i < Cmd_Argc(); i++)
	{
		strlcat(message, Cmd_Argv(i), sizeof(message));
		strlcat(message, " ", sizeof(message));
	}
	// End code from EZQuake

	if (cls.state >= ca_connected) {
		to = cls.netchan.remote_address;
	} else {
		if (!strlen(rcon_address.string)) {
			Com_Printf ("You must either be connected or set 'rcon_address' to issue rcon commands\n");
			return;
		}
		NET_StringToAdr (rcon_address.string, &to);
		if (to.port == 0)
			to.port = BigShort (PORT_SERVER);
	}

	NET_SendPacket (NS_CLIENT, strlen(message)+1, message, to);
}

void CL_Download_f (void){
	char *p, *q;

	if (cls.state == ca_disconnected) {
		Com_Printf ("Must be connected.\n");
		return;
	}

	if (Cmd_Argc() != 2) {
		Com_Printf ("Usage: %s <datafile>\n", Cmd_Argv(0));
		return;
	}

	Q_snprintfz (cls.downloadname, sizeof(cls.downloadname), "%s/%s", cls.gamedir, Cmd_Argv(1));

	p = cls.downloadname;
	while (1) {
		if ((q = strchr(p, '/')) != NULL) {
			*q = 0;
			Sys_mkdir(cls.downloadname);
			*q = '/';
			p = q + 1;
		} else
			break;
	}

	strcpy(cls.downloadtempname, cls.downloadname);
	cls.download = fopen (cls.downloadname, "wb");
	cls.downloadtype = dl_single;

	// OfN September 2020
	cls.downloadmethod = DL_QW; // by default its DL_QW, if server support DL_QWCHUNKED it will be changed.
	cls.downloadstarttime = Sys_DoubleTime();
	// OfN End

	MSG_WriteByte (&cls.netchan.message, clc_stringcmd);
	SZ_Print (&cls.netchan.message, va("download %s\n",Cmd_Argv(1)));
}

void CL_User_f (void) {
	int uid, i;

	if (Cmd_Argc() != 2) {
		Com_Printf ("Usage: %s <username / userid>\n", Cmd_Argv(0));
		return;
	}

	uid = atoi(Cmd_Argv(1));

	for (i = 0; i < MAX_CLIENTS; i++) {
		if (!cl.players[i].name[0])
			continue;
		if (cl.players[i].userid == uid	|| !strcmp(cl.players[i].name, Cmd_Argv(1)) ) {
			Info_Print (cl.players[i].userinfo);
			return;
		}
	}
	Com_Printf ("User not in server.\n");
}

void CL_Users_f (void) {
	int i, c;

	c = 0;
	Com_Printf ("userid frags name\n");
	Com_Printf ("------ ----- ----\n");
	for (i = 0; i < MAX_CLIENTS; i++) {
		if (cl.players[i].name[0]) {
			Com_Printf ("%6i %4i %s\n", cl.players[i].userid, cl.players[i].frags, cl.players[i].name);
			c++;
		}
	}

	Com_Printf ("%i total users\n", c);
}

void CL_Color_f (void) {
	extern cvar_t topcolor, bottomcolor;
	int top, bottom;

	if (Cmd_Argc() == 1) {
		Com_Printf ("\"color\" is \"%s %s\"\n",
			Info_ValueForKey (cls.userinfo, "topcolor"),
			Info_ValueForKey (cls.userinfo, "bottomcolor") );
		Com_Printf ("color <0-13> [0-13]\n");
		return;
	}

	if (Cmd_Argc() == 2) {
		top = bottom = atoi(Cmd_Argv(1));
	} else {
		top = atoi(Cmd_Argv(1));
		bottom = atoi(Cmd_Argv(2));
	}

	top &= 15;
	top = min(top, 13);
	bottom &= 15;
	bottom = min(bottom, 13);

	Cvar_SetValue (&topcolor, top);
	Cvar_SetValue (&bottomcolor, bottom);
}

//usage: fullinfo \name\unnamed\topcolor\0\bottomcolor\1, etc
void CL_FullInfo_f (void) {
	char key[512], value[512], *o, *s;

	if (Cmd_Argc() != 2) {
		Com_Printf ("fullinfo <complete info string>\n");
		return;
	}

	s = Cmd_Argv(1);
	if (*s == '\\')
		s++;
	while (*s) {
		o = key;
		while (*s && *s != '\\')
			*o++ = *s++;
		*o = 0;

		if (!*s) {
			Com_Printf ("MISSING VALUE\n");
			return;
		}

		o = value;
		s++;
		while (*s && *s != '\\')
			*o++ = *s++;
		*o = 0;

		if (*s)
			s++;

		if (!Q_strcasecmp(key, pmodel_name) || !Q_strcasecmp(key, emodel_name))
			continue;

		Info_SetValueForKey (cls.userinfo, key, value, MAX_INFO_STRING);
	}
}

//Allow clients to change userinfo
void CL_SetInfo_f (void) {
	if (Cmd_Argc() == 1) {
		Info_Print (cls.userinfo);
		return;
	}
	if (Cmd_Argc() != 3) {
		Com_Printf ("Usage: %s [ <key> <value> ]\n", Cmd_Argv(0));
		return;
	}
	if (!Q_strcasecmp(Cmd_Argv(1), pmodel_name) || !strcmp(Cmd_Argv(1), emodel_name))
		return;

	Info_SetValueForKey (cls.userinfo, Cmd_Argv(1), Cmd_Argv(2), MAX_INFO_STRING);
	if (cls.state >= ca_connected)
		Cmd_ForwardToServer ();
}


void CL_UserInfo_f (void) {
	if (Cmd_Argc() != 1) {
		Com_Printf("%s : no arguments expected\n", Cmd_Argv(0));
		return;
	}
	Info_Print (cls.userinfo);
}

void SV_Quit_f (void);

void CL_Quit_f (void) {
	extern cvar_t cl_confirmquit;

#ifndef CLIENTONLY
	if (dedicated)
		SV_Quit_f ();
	else
#endif
	{
		if (cl_confirmquit.value)
			M_Menu_Quit_f ();
		else
			Host_Quit ();
	}
}

#ifdef _WINDOWS
void CL_Windows_f (void) {
	SendMessage(mainwindow, WM_SYSKEYUP, VK_TAB, 1 | (0x0F << 16) | (1<<29));
}
#endif

void CL_Serverinfo_f (void) {
#ifndef CLIENTONLY
	if (cls.state < ca_connected || com_serveractive) {
		SV_Serverinfo_f();
		return;
	}
#endif

	if (cls.state >= ca_onserver && cl.serverinfo)
		Info_Print (cl.serverinfo);
	else		
		Com_Printf ("Can't \"%s\", not connected\n", Cmd_Argv(0));
}


//============================================================================

void CL_WriteConfig (char *name) {
	FILE *f;

	if (!(f = fopen (va("%s/%s", cls.gamedir, name), "w"))) {
		Com_Printf ("Couldn't write %s.\n", name);
		return;
	}

	fprintf(f, "// Generated by ProzacQW\n");//FuhQuake\n");
	fprintf (f, "\n// Key bindings\n");
	Key_WriteBindings (f);
	fprintf (f, "\n// Variables\n");
	Cvar_WriteVariables (f);
	fprintf (f, "\n// Aliases\n");
	Cmd_WriteAliases (f);

	fclose (f);
}

//Writes key bindings and archived cvars to config.cfg
void CL_WriteConfiguration (void) {
	if (host_initialized && cfg_legacy_write.value)
		CL_WriteConfig ("config.cfg");
}

//Writes key bindings and archived cvars to a custom config file
void CL_WriteConfig_f (void) {
	char name[MAX_OSPATH];

	if (Cmd_Argc() != 2) {
		Com_Printf ("Usage: %s <filename>\n", Cmd_Argv(0));
		return;
	}

	Q_strncpyz (name, Cmd_Argv(1), sizeof(name));
	COM_ForceExtension (name, ".cfg");

	Com_Printf ("Writing %s\n", name);

	CL_WriteConfig (name);
}

void CL_IntegrationStr_f(void);

void CL_InitCommands (void) {
	// general commands
	Cmd_AddCommand ("cmd", CL_ForwardToServer_f);
	Cmd_AddCommand ("download", CL_Download_f);
	Cmd_AddCommand ("packet", CL_Packet_f);
	Cmd_AddCommand ("pause", CL_Pause_f);
	Cmd_AddCommand ("quit", CL_Quit_f);
	Cmd_AddCommand ("rcon", CL_Rcon_f);
	Cmd_AddCommand ("say", CL_Say_f);
	Cmd_AddCommand ("say_team", CL_Say_f);
	Cmd_AddCommand ("serverinfo", CL_Serverinfo_f);
	Cmd_AddCommand ("skins", Skin_Skins_f);
	Cmd_AddCommand ("allskins", Skin_AllSkins_f);
	Cmd_AddCommand ("user", CL_User_f);
	Cmd_AddCommand ("users", CL_Users_f);
	Cmd_AddCommand ("version", CL_Version_f);
	Cmd_AddCommand ("writeconfig", CL_WriteConfig_f);

	// client info setting
	Cmd_AddCommand ("color", CL_Color_f);
	Cmd_AddCommand ("fullinfo", CL_FullInfo_f);
	Cmd_AddCommand ("setinfo", CL_SetInfo_f);
	Cmd_AddCommand ("userinfo", CL_UserInfo_f);

	// forward to server commands
	Cmd_AddCommand ("kill", NULL);
	Cmd_AddCommand ("god", NULL);
	Cmd_AddCommand ("give", NULL);
	Cmd_AddCommand ("noclip", NULL);
	Cmd_AddCommand ("fly", NULL);

	// OfN - Integration special command;
	Cmd_AddCommand("integration_issue", CL_Integration_f);
	Cmd_AddCommand("integration_str", CL_IntegrationStr_f);

	//  Windows commands
#ifdef _WINDOWS
	Cmd_AddCommand ("windows", CL_Windows_f);
#endif

	Integration_Weather_str[0] = '\0';
	Integration_Fog_str[0] = '\0';
	Integration_Sky_str[0] = '\0';
	Integration_Shader_str[0] = '\0';
	Integration_Misc_str[0] = '\0';
}

/*
==============================================================================
SERVER COMMANDS

Server commands are commands stuffed by server into client's cbuf
We use a separate command buffer for them -- there are several
reasons for that:
1. So that partially stuffed commands are always executed properly
2. Not to let players cheat in TF (v_cshift etc don't work in console)
3. To hide some commands the user doesn't need to know about, like
changing, fullserverinfo, nextul, stopul
==============================================================================
*/

//Just sent as a hint to the client that they should drop to full console
void CL_Changing_f (void) {
	cl.intermission = 0;

	if (cls.download)  // don't change when downloading
		return;

	S_StopAllSounds (true);
	cls.state = ca_connected;	// not active anymore, but not disconnected

	Com_Printf ("\nChanging map...\n");
}

//Sent by server when serverinfo changes
void CL_FullServerinfo_f (void) {
	char *p;

	if (Cmd_Argc() != 2)
		return;

	Q_strncpyz (cl.serverinfo, Cmd_Argv(1), sizeof(cl.serverinfo));

	p = Info_ValueForKey (cl.serverinfo, "*cheats");
	if (*p)
		Com_Printf ("== Cheats are enabled ==\n");

	CL_ProcessServerInfo ();
}

void CL_Fov_f (void) {
	extern cvar_t scr_fov, default_fov;

	if (Cmd_Argc() == 1) {
		Com_Printf ("\"fov\" is \"%s\"\n", scr_fov.string);
		return;
	}

	if (Q_atof(Cmd_Argv(1)) == 90.0 && default_fov.value)
		Cvar_SetValue (&scr_fov, default_fov.value);
	else
		Cvar_Set (&scr_fov, Cmd_Argv(1));
}

void CL_R_DrawViewModel_f (void) {
	extern cvar_t cl_filterdrawviewmodel;

	if (cl_filterdrawviewmodel.value)
		return;
	Cvar_Command ();
}

typedef struct {
	char	*name;
	void	(*func) (void);
} svcmd_t;

svcmd_t svcmds[] = {
	{"changing", CL_Changing_f},
	{"fullserverinfo", CL_FullServerinfo_f},
	{"nextul", CL_NextUpload},
	{"stopul", CL_StopUpload},
	{"fov", CL_Fov_f},
	{"r_drawviewmodel", CL_R_DrawViewModel_f},
	{"fileul", CL_StartFileUpload}, // October 2020 - Readded May 2023
	{NULL, NULL}
};

//Called by Cmd_ExecuteString if cbuf_current == &cbuf_svc
qboolean CL_CheckServerCommand () {
	svcmd_t	*cmd;
	char *s;

	s = Cmd_Argv (0);
	for (cmd = svcmds; cmd->name; cmd++) {
		if (!strcmp (s, cmd->name) ) {
			cmd->func();
			return true;
		}
	}

	return false;
}

// OfN - This is very particular, handles the server and client integration
void CL_ExecuteWeatherIntegration(void);
void CL_ExecuteFogIntegration(void);
void CL_ExecuteSkyIntegration(void);
void CL_ExecuteShaderIntegration(void);
void CL_ExecuteMiscIntegration(void);

void CL_Integration_f(void)
{
	int numItems = Cmd_Argc();

	Integration_Weather_str[0] = '\0';
	Integration_Fog_str[0] = '\0';
	Integration_Sky_str[0] = '\0';
	Integration_Shader_str[0] = '\0';
	Integration_Misc_str[0] = '\0';

	char current_token[MAX_INTEGRATION_TOKEN_STRLEN];

	if (!integration_weather.value && !integration_fog.value && !integration_sky.value && !integration_shader.value && !integration_misc.value)
	{
		Com_DPrintf("Integration information received, but ignored!\n");
		return;
	}

	int i;

	for (i = 1; i < numItems; i++) 
	{		
		if (strlen(Cmd_Argv(i)) > MAX_INTEGRATION_TOKEN_STRLEN)
			continue;
		strcpy(current_token, Cmd_Argv(i));
		if (strlen(current_token) < 4)
			continue;

		if (current_token[0] == 'f')
		{
			strcat(Integration_Fog_str, current_token);
			strcat(Integration_Fog_str, " ");
		}
		else if (current_token[0] == 'w')
		{
			strcat(Integration_Weather_str, current_token);
			strcat(Integration_Weather_str, " ");
		}
		else if (current_token[0] == 's')
		{
			strcat(Integration_Sky_str, current_token);
			strcat(Integration_Sky_str, " ");
		}
		else if (current_token[0] == 'h')
		{
			strcat(Integration_Shader_str, current_token);
			strcat(Integration_Shader_str, " ");
		}
		// november 2023
		else if (current_token[0] == 'm')
		{
			strcat(Integration_Misc_str, current_token);
			strcat(Integration_Misc_str, " ");
		}
	}

	if (integration_fog.value)
		CL_ExecuteFogIntegration();
	if (integration_weather.value)
		CL_ExecuteWeatherIntegration();
	if (integration_sky.value)
		CL_ExecuteSkyIntegration();
	if (integration_shader.value)
		CL_ExecuteShaderIntegration();
	if (integration_misc.value)
		CL_ExecuteMiscIntegration();
	
	Com_DPrintf("Integration information received and applied from the server.\n");
}

void CL_ExecuteFogIntegration(void)
{
	qboolean use_fog = false;
	float tmp_fmode_value = 0;

	Cvar_Set(&gl_fog_skydist, gl_fog_skydist.defaultvalue); // April 2022 - Sets default fog dist as default ^_^ always, when integration

	if (strlen(Integration_Fog_str) < 4)
		use_fog = false;
	else
	{
		use_fog = true;
		int len_token = 0;
		char cur_token[MAX_INTEGRATION_TOKEN_STRLEN];
		cur_token[0] = '\0';
		char value[MAX_INTEGRATION_TOKEN_STRLEN];
		int fuse = 0;
		float fvalue = 0;		

		qboolean safety = false;

		// SAFETY CHECK
		for (int z = 0; z < strlen(Integration_Fog_str); z++)
		{
			if (Integration_Fog_str[z] == ' ')
				safety = true;
		}
		if (!safety)
			return;
		
		for (int i = 0; i < strlen(Integration_Fog_str) && fuse < 20; fuse++)
		{
			len_token = 0;
			value[0] = '\0';
						
			if (Integration_Fog_str[i] != 'f')
				break;
			
			int z;
			for (z = 0; z < MAX_INTEGRATION_TOKEN_STRLEN; z++) // FIXME: this in unelegant and unsafe
			{
				if (Integration_Fog_str[i + len_token] != ' ')
					len_token++;
				else
					break;
			}
			
			if (len_token < 4)
				break;

			if (len_token > MAX_INTEGRATION_TOKEN_STRLEN - 1)
				break;

			if (Integration_Fog_str[i + 2] != ':')
				break;

			int x;

			for (x = 0; x < len_token; x++)
				cur_token[x] = Integration_Fog_str[i + x];
			
			cur_token[x] = '\0';

			if (cur_token[0] == 'f')
			{
				if (cur_token[1] == 'm')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y - 3] = '\0';

					fvalue = atof(value);

					fvalue = bound(0, fvalue, 2);
					//Cvar_SetValue(&gl_fog_mode, fvalue);

					tmp_fmode_value = fvalue;
				}
				else if (cur_token[1] == 'd')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y - 3] = '\0';

					fvalue = atof(value);

					fvalue = bound(0.0f, fvalue, 1.0f);
					Cvar_SetValue(&gl_fog_density, fvalue);
				}
				else if (cur_token[1] == 's')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y - 3] = '\0';

					fvalue = atof(value);

					fvalue = bound(128, fvalue, 8192);
					Cvar_SetValue(&gl_fog_skydist, fvalue);
				}
				else if (cur_token[1] == 'r')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y-3] = '\0';

					fvalue = atof(value);
					Cvar_SetValue(&gl_fog_red, fvalue);
				}
				else if (cur_token[1] == 'g')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y-3] = '\0';

					fvalue = atof(value);
					Cvar_SetValue(&gl_fog_green, fvalue);
				}
				else if (cur_token[1] == 'b')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y-3] = '\0';

					fvalue = atof(value);
					Cvar_SetValue(&gl_fog_blue, fvalue);
				}
				else if (cur_token[1] == 'n')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y-3] = '\0';

					fvalue = atof(value);
					Cvar_SetValue(&gl_fog_near, fvalue);
				}
				else if (cur_token[1] == 'f')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y-3] = '\0';
					
					fvalue = atof(value);
					Cvar_SetValue(&gl_fog_far, fvalue);
				}
			}
			
			i += len_token+1; 
		}
	}

	// New in 0.31
	Cvar_SetValue(&gl_fog_mode, tmp_fmode_value);

	if (use_fog)
		Cvar_Set(&gl_fog, "1");
	else
		Cvar_Set(&gl_fog, "0");
}

void CL_ExecuteWeatherIntegration(void)
{
	int use_weather = 0;

	if (strlen(Integration_Weather_str) < 4)
		use_weather = 0;
	else
	{
		int len_token = 0;
		char cur_token[MAX_INTEGRATION_TOKEN_STRLEN];
		cur_token[0] = '\0';
		char value[MAX_INTEGRATION_TOKEN_STRLEN];
		int fuse = 0;
		float fvalue = 0;

		qboolean safety = false;

		// SAFETY CHECK
		for (int z = 0; z < strlen(Integration_Weather_str); z++)
		{
			if (Integration_Weather_str[z] == ' ')
				safety = true;
		}
		if (!safety)
			return;

		for (int i = 0; i < strlen(Integration_Weather_str) && fuse < 20; fuse++)
		{
			len_token = 0;
			value[0] = '\0';

			if (Integration_Weather_str[i] != 'w')
				break;

			int z;
			for (z = 0; z < MAX_INTEGRATION_TOKEN_STRLEN; z++) // FIXME: this in unelegant and unsafe
			{
				if (Integration_Weather_str[i + len_token] != ' ')
					len_token++;
				else
					break;
			}

			if (len_token < 4)
				break;

			if (len_token > MAX_INTEGRATION_TOKEN_STRLEN - 1)
				break;

			if (Integration_Weather_str[i + 2] != ':')
				break;

			int x;

			for (x = 0; x < len_token; x++)
				cur_token[x] = Integration_Weather_str[i + x];

			cur_token[x] = '\0';

			if (cur_token[0] == 'w')
			{
				if (cur_token[1] == 'm')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y - 3] = '\0';

					fvalue = atof(value);
					//Cvar_SetValue(&weather_mode, fvalue);
					use_weather = fvalue;
				}
				else if (cur_token[1] == 'r')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y - 3] = '\0';

					//fvalue = atof(value);
					fvalue = (byte)(bound(0, atof(value), 255));
					Cvar_SetValue(&weather_rain_red, fvalue);
				}
				else if (cur_token[1] == 'g')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y - 3] = '\0';

					//fvalue = atof(value);
					fvalue = (byte)(bound(0, atof(value), 255));
					Cvar_SetValue(&weather_rain_green, fvalue);
				}
				else if (cur_token[1] == 'b')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y - 3] = '\0';

					fvalue = (byte)(bound(0, atof(value), 255));
					Cvar_SetValue(&weather_rain_blue, fvalue);
				}
			}

			i += len_token + 1;
		}
	}

	if (use_weather==1)
		Cvar_Set(&weather_mode, "1");
	else if (use_weather == 2)
		Cvar_Set(&weather_mode, "2");
	else
		Cvar_Set(&weather_mode, "0");
}

//extern qboolean SKYFileLoaded;
extern qboolean CustomSKYnonServer;

void CL_ExecuteSkyIntegration(void)
{
	//if (SKYFileLoaded)
		//return;

	if (CustomSKYnonServer)
	{
		CustomSKYnonServer = false; // Kinda hackish to reset it here, i know...
		return;
	}

	qboolean use_sky = false;
	char value[MAX_INTEGRATION_TOKEN_STRLEN];
	value[0] = '\0';

	if (strlen(Integration_Sky_str) < 4)
		use_sky = false;
	else
	{		
		int len_token = 0;
		char cur_token[MAX_INTEGRATION_TOKEN_STRLEN];
		cur_token[0] = '\0';
		int fuse = 0;
		
		qboolean safety = false;

		// SAFETY CHECK
		for (int z = 0; z < strlen(Integration_Sky_str); z++)
		{
			if (Integration_Sky_str[z] == ' ')
				safety = true;
		}
		if (!safety)
			return;

		for (int i = 0; i < strlen(Integration_Sky_str) && fuse < 24; fuse++)
		{
			len_token = 0;			

			if (Integration_Sky_str[i] != 's')
				break;

			int z;
			for (z = 0; z < MAX_INTEGRATION_TOKEN_STRLEN; z++) // FIXME: this in unelegant and unsafe
			{
				if (Integration_Sky_str[i + len_token] != ' ')
					len_token++;
				else
					break;
			}

			if (len_token < 4)
				break;

			if (len_token > MAX_INTEGRATION_TOKEN_STRLEN - 1)
				break;

			if (Integration_Sky_str[i + 2] != ':')
				break;

			int x;

			for (x = 0; x < len_token; x++)
				cur_token[x] = Integration_Sky_str[i + x];

			cur_token[x] = '\0';

			if (cur_token[0] == 's')
			{
				if (cur_token[1] == 'n')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y - 3] = '\0';
					use_sky = true;
				}
			}
			i += len_token + 1;
		}
	}

	if (use_sky)
		Cvar_Set(&r_skyname, value);
	else
		Cvar_Set(&r_skyname, "");
}

void CL_ExecuteShaderIntegration(void)
{
	qboolean use_shader = false;
	char value[MAX_INTEGRATION_TOKEN_STRLEN];
	value[0] = '\0';

	Cvar_Set(&shader_ref_balance, shader_ref_balance.defaultvalue);
	Cvar_Set(&shader_tex_balance, shader_tex_balance.defaultvalue);
	Cvar_Set(&shader_wavesize, shader_wavesize.defaultvalue);
	Cvar_Set(&shader_wavestrength, shader_wavestrength.defaultvalue);
	Cvar_Set(&shader_waverate, shader_waverate.defaultvalue);
	Cvar_Set(&shader_color, shader_color.defaultvalue);
	Cvar_Set(&shader_col_balance, shader_col_balance.defaultvalue);
	Cvar_Set(&shader_fresnel, shader_fresnel.defaultvalue);
	Cvar_Set(&shader_rip_color, shader_rip_color.defaultvalue);
	Cvar_Set(&shader_rip_balance, shader_rip_balance.defaultvalue);
	Cvar_Set(&shader_light_x, shader_light_x.defaultvalue);
	Cvar_Set(&shader_light_y, shader_light_y.defaultvalue);
	Cvar_Set(&shader_light_z, shader_light_z.defaultvalue);
	Cvar_Set(&shader_rip_shine, shader_rip_shine.defaultvalue);
	Cvar_Set(&shader_rip_reflect, shader_rip_reflect.defaultvalue);
	Cvar_Set(&shader_displacement, shader_displacement.defaultvalue);
	Cvar_Set(&shader_rip_distortion, shader_rip_distortion.defaultvalue);
	Cvar_Set(&shader_rip_source, shader_rip_source.defaultvalue);

	if (strlen(Integration_Shader_str) < 4)
		use_shader = false;
	else
	{
		int len_token = 0;
		char cur_token[MAX_INTEGRATION_TOKEN_STRLEN];
		cur_token[0] = '\0';
		int fuse = 0;
		float fvalue = 0;

		qboolean safety = false;

		// SAFETY CHECK
		for (int z = 0; z < strlen(Integration_Shader_str); z++)
		{
			if (Integration_Shader_str[z] == ' ')
				safety = true;
		}
		if (!safety)
			return;

		for (int i = 0; i < strlen(Integration_Shader_str) && fuse < 32; fuse++) // fuse limit was 24
		{
			len_token = 0;

			if (Integration_Shader_str[i] != 'h')
				break;

			int z;
			for (z = 0; z < MAX_INTEGRATION_TOKEN_STRLEN; z++) // FIXME: this in unelegant and unsafe
			{
				if (Integration_Shader_str[i + len_token] != ' ')
					len_token++;
				else
					break;
			}

			if (len_token < 4)
				break;

			if (len_token > MAX_INTEGRATION_TOKEN_STRLEN - 1)
				break;

			if (Integration_Shader_str[i + 2] != ':')
				break;

			int x;

			for (x = 0; x < len_token; x++)
				cur_token[x] = Integration_Shader_str[i + x];

			cur_token[x] = '\0';

			if (cur_token[0] == 'h')
			{
				if (cur_token[1] == 'm')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y - 3] = '\0';

					fvalue = atof(value);
					use_shader = fvalue != 0;
				}
				else if (cur_token[1] == 'f')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y - 3] = '\0';

					fvalue = (float)(bound(0, atof(value), 1));
					Cvar_SetValue(&shader_ref_balance, fvalue);
				}
				else if (cur_token[1] == 'w')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y - 3] = '\0';

					fvalue = (float)(bound(0, atof(value), 1));
					Cvar_SetValue(&shader_tex_balance, fvalue);
				}
				else if (cur_token[1] == 'v')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y - 3] = '\0';

					fvalue = (float)atof(value);
					Cvar_SetValue(&shader_wavesize, fvalue);
				}
				else if (cur_token[1] == 's')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y - 3] = '\0';

					fvalue = (float)atof(value);
					Cvar_SetValue(&shader_wavestrength, fvalue);
				}
				else if (cur_token[1] == 'r')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y - 3] = '\0';

					fvalue = (float)atof(value);
					Cvar_SetValue(&shader_waverate, fvalue);
				}
				else if (cur_token[1] == 'c')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y - 3] = '\0';

					Cvar_Set(&shader_color, value);
				}
				else if (cur_token[1] == 'b')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y - 3] = '\0';

					fvalue = (float)(bound(0, atof(value), 1));
					Cvar_SetValue(&shader_col_balance, fvalue);
				}
				else if (cur_token[1] == 'n')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y - 3] = '\0';

					fvalue = (float)atof(value);
					Cvar_SetValue(&shader_fresnel, fvalue);
				}
				else if (cur_token[1] == 'p')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y - 3] = '\0';

					Cvar_Set(&shader_rip_color, value);
				}
				else if (cur_token[1] == 'a')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y - 3] = '\0';

					fvalue = (float)(bound(0, atof(value), 1));
					Cvar_SetValue(&shader_rip_balance, fvalue);
				}
				else if (cur_token[1] == 'x')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y - 3] = '\0';

					fvalue = (float)(bound(-4095, atof(value), 4095));
					Cvar_SetValue(&shader_light_x, fvalue);
				}
				else if (cur_token[1] == 'y')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y - 3] = '\0';

					fvalue = (float)(bound(-4095, atof(value), 4095));
					Cvar_SetValue(&shader_light_y, fvalue);
				}
				else if (cur_token[1] == 'z')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y - 3] = '\0';

					fvalue = (float)(bound(-4095, atof(value), 4095));
					Cvar_SetValue(&shader_light_z, fvalue);
				}
				else if (cur_token[1] == 'h')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y - 3] = '\0';

					fvalue = (float)atof(value);
					Cvar_SetValue(&shader_rip_shine, fvalue);
				}
				else if (cur_token[1] == 'i')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y - 3] = '\0';

					fvalue = (float)atof(value);
					Cvar_SetValue(&shader_rip_reflect, fvalue);
				}
				else if (cur_token[1] == 'd')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y - 3] = '\0';

					fvalue = (float)atof(value);
					Cvar_SetValue(&shader_displacement, fvalue);
				}
				else if (cur_token[1] == 't')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y - 3] = '\0';

					fvalue = (float)atof(value);
					Cvar_SetValue(&shader_rip_distortion, fvalue);
				}
				else if (cur_token[1] == 'e')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y - 3] = '\0';

					fvalue = (float)(bound(0, atof(value), 1));
					Cvar_SetValue(&shader_rip_source, fvalue);
				}
			}

			i += len_token + 1;
		}
	}

	if (use_shader)
		Cvar_SetValue(&r_watershader, 1);
	else
		Cvar_SetValue(&r_watershader, 0);
}

void CL_ExecuteMiscIntegration(void)
{
	qboolean use_outlines = false;
	qboolean use_bloom = true;
	qboolean use_detail = true;
	qboolean use_caustics = true;

	Cvar_Set(&r_bloom_intensity, r_bloom_intensity.defaultvalue);
	Cvar_Set(&r_outlines_color, r_outlines_color.defaultvalue);
	Cvar_Set(&r_outlines_factor, r_outlines_factor.defaultvalue);
	
	if (strlen(Integration_Misc_str) < 4)
		;
	else
	{
		int len_token = 0;
		char cur_token[MAX_INTEGRATION_TOKEN_STRLEN];
		cur_token[0] = '\0';
		char value[MAX_INTEGRATION_TOKEN_STRLEN];
		int fuse = 0;
		float fvalue = 0;

		qboolean safety = false;

		// SAFETY CHECK
		for (int z = 0; z < strlen(Integration_Misc_str); z++)
		{
			if (Integration_Misc_str[z] == ' ')
				safety = true;
		}
		if (!safety)
			return;

		for (int i = 0; i < strlen(Integration_Misc_str) && fuse < 36; fuse++)
		{
			len_token = 0;
			value[0] = '\0';

			if (Integration_Misc_str[i] != 'm')
				break;

			int z;
			for (z = 0; z < MAX_INTEGRATION_TOKEN_STRLEN; z++) // FIXME: this in unelegant and unsafe
			{
				if (Integration_Misc_str[i + len_token] != ' ')
					len_token++;
				else
					break;
			}

			if (len_token < 4)
				break;

			if (len_token > MAX_INTEGRATION_TOKEN_STRLEN - 1)
				break;

			if (Integration_Misc_str[i + 2] != ':')
				break;

			int x;

			for (x = 0; x < len_token; x++)
				cur_token[x] = Integration_Misc_str[i + x];

			cur_token[x] = '\0';

			if (cur_token[0] == 'm')
			{
				if (cur_token[1] == 'l')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y - 3] = '\0';

					fvalue = atof(value);

					fvalue = bound(0, fvalue, 5);
					if (fvalue == 0)
						use_outlines = false;
					else
					{
						use_outlines = true;
						Cvar_SetValue(&r_outlines_width, fvalue);
					}
				}
				else if (cur_token[1] == 'c')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y - 3] = '\0';

					if (strlen(value) == 3)
						Cvar_Set(&r_outlines_color, value);
				}
				else if (cur_token[1] == 'w')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y - 3] = '\0';

					fvalue = atof(value);
					if (fvalue != 0.0f)
						use_caustics = true;
					else
						use_caustics = false;					
				}
				else if (cur_token[1] == 'd')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y - 3] = '\0';

					fvalue = atof(value);

					fvalue = bound(0, fvalue, 5);
					if (fvalue != 0.0f)
						use_detail = true;
					else
						use_detail = false;
				}
				else if (cur_token[1] == 'b')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y - 3] = '\0';

					fvalue = atof(value);

					fvalue = bound(0, fvalue, 5);
					if (fvalue != 0.0f)
						use_bloom = true;
					else
						use_bloom = false;
				}
				else if (cur_token[1] == 'i')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y - 3] = '\0';

					fvalue = atof(value);

					Cvar_SetValue(&r_bloom_intensity, fvalue);
				}
				else if (cur_token[1] == 'f')
				{
					int y;
					for (y = 3; y < len_token; y++)
						value[y - 3] = cur_token[y];
					value[y - 3] = '\0';

					fvalue = atof(value);

					Cvar_SetValue(&r_outlines_factor, fvalue);
				}
			}

			i += len_token + 1;
		}
	}

	if (use_outlines)
		Cvar_SetValue(&r_outlines, 1);
	else
		Cvar_SetValue(&r_outlines, 0);

	if (use_bloom)
		Cvar_SetValue(&r_bloom, 1);
	else
		Cvar_SetValue(&r_bloom, 0);

	if (use_detail)
		Cvar_SetValue(&gl_detail, 1);
	else
		Cvar_SetValue(&gl_detail, 0);

	if (use_caustics)
		Cvar_SetValue(&gl_caustics, 1);
	else
		Cvar_SetValue(&gl_caustics, 0);
}


void CL_IntegrationStr_f(void)
{
	Com_Printf("Current Integration strings are...\nWeather: %s\nFog: %s\nSky: %s\nShader: %s\nMisc: %s\n", Integration_Weather_str,Integration_Fog_str,Integration_Sky_str,Integration_Shader_str,Integration_Misc_str);
}