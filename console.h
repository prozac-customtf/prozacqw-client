/*
Copyright (C) 1996-1997 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

//
// console
//

#ifndef	_CONSOLE_H_

#define _CONSOLE_H_

// From EZQuake Color structs
typedef int color_t;

extern const color_t COLOR_WHITE;
typedef struct clrinfo_s
{
	color_t c;	// Color.
	//int i;		// Index when this colors starts.
} clrinfo_t;
// End OfN

#define		CON_TEXTSIZE	32768
typedef struct
{
	char	*text;
	int		maxsize;
	int		current;		// line where next message will be printed
	int		x;				// offset in current line for next print
	int		display;		// bottom of console displays this line
	int		numlines;		// number of non-blank text lines, used for backscroling
	clrinfo_t* clr; // clr[maxsize]
} console_t;

extern	console_t	con;
extern	int			con_ormask;

extern int con_totallines;
extern qboolean con_initialized, con_suppress;
extern	int	con_notifylines;		// scan lines to clear for notify lines

void Con_CheckResize (void);
void Con_Init (void);
void Con_Shutdown (void);
void Con_DrawConsole (int lines);
void Con_Print (char *txt);
void Con_Clear_f (void);
void Con_DrawNotify (void);
void Con_ClearNotify (void);
void Con_ToggleConsole_f (void);
void Con_ColorChat_Event(int teamcolornum, int nameoffset, int startline); // OfN

// Copied from fragstats.c
typedef enum msgtype_s {
	mt_fragged,
	mt_frags,
	mt_tkills,
	mt_tkilled,

	mt_death,
	mt_suicide,
	mt_frag,
	mt_tkill,
	mt_flagtouch,
	mt_flagdrop,
	mt_flagcap
}
msgtype_t;

void Con_FireFragEvent(msgtype_t type, char* killer, char* victim, char* weapon, qboolean noweap); // OfN
void Con_CheckFragEventOffs(void);
void Con_DrawFragEvents(void); // OfN



extern int con_linewidth; // OfN Moved here from console.c for external use

#endif		//_CONSOLE_H_
