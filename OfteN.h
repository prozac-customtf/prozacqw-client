/*=======================================================================================//
 OfteN.h - Basically all things I don't know where to put - This was signed on 29-12-2020
 ----------------------------------------------------------------------------------------
 Written by Sergi Fumanya Grunwaldt - For ProzacQW/Prozac QuakeWorld 0.36 and upwards...
//=======================================================================================*/

#ifndef _OFTEN_H_
#define _OFTEN_H_

// RGBA Value byte array
typedef byte col_t[4];

// Holds a string describing the Glu library being used
extern const char* GluVersion;

// Function called on startup of the client 
void OfN_InitClient(void);

// Function called on shutdown of the client
void OfN_ShutdownClient(void);

// Draws a cylinder given the coords of 2 vectors, specifying thickness and its color (uses raw C code)
void OfN_drawCylinder(float x1, float y1, float z1, float x2, float y2, float z2, float thick, col_t color);

// Draws a sphere using the specified number of vertices, color and radius (uses the Glu library)
void OfN_drawSphere(vec3_t org, double radius, int vertnum, col_t color);

// Draws a circular-shaped disk given its origin, radius, color and angles to be rotated to (uses the Glu library)
void OfN_drawDisk(vec3_t org, float anglex, float angley, float anglez, double innerRadius, double outerRadius, int vertnum, col_t color);

// Draws an arc-shaped plane object at the origin specified, with the given amount of vertices and color (uses the Glu library)
void OfN_drawPartialDisk(vec3_t org, float anglex, float angley, float anglez, double innerRadius, double outerRadius, int vertnum, col_t color, double startangle, double sweepangle);

// Utility function to calculate the yaw angle based on 2 vectors difference
float VectoYaw(vec3_t vector1, vec3_t vector2);

// Utility function to calculate the pitch angle based on 2 vectors difference
float VectoPitch(vec3_t vector1, vec3_t vector2);

// CORONA's ======================//
// For coronas
typedef enum
{
	C_FLASH,			// Explosion type - Basically just fades out
	C_SMALLFLASH,		// Muzzleflash
	C_BLUEFLASH,		// Blob expl
	C_FREE,				// Unused
	C_LIGHTNING,		// Used on lightning beams
	C_SMALLLIGHTNING,	// Used on lightning beams
	C_FIRE,				// Used in torches
	C_ROCKETLIGHT,		// A rockets glow...
	C_GREENLIGHT,		// Detpack
	C_REDLIGHT,			// Detpack about to blow
	C_BLUESPARK,		// Gibbed building spark
	C_YELLOWSPARK,      // OfN - May 2021 for machine yellow sparks
	C_GUNFLASH,			// Shotgun hit effect
	C_EXPLODE,			// Animated explosion
	C_WHITELIGHT,		// Gunshot #2 effect
	C_WIZLIGHT,			// CREATURE TRACER LIGHTS
	C_KNIGHTLIGHT,
	C_VORELIGHT,
} coronatype_t;

// Coronas by Vult, from EZQuake (technically from AMFQuake)
extern int CoronaCount, CoronaCountHigh;

void NewCorona(coronatype_t type, vec3_t origin);
void R_DrawCoronas(void);
void InitCoronas(void);
void NewStaticLightCorona(coronatype_t type, vec3_t origin, entity_t* serialhint);

float CL_TraceLine(vec3_t start, vec3_t end, vec3_t impact, vec3_t normal);

void GL_PolygonOffset(float factor, float units);

// bloom cvars
extern cvar_t r_bloom;
extern cvar_t r_bloom_darken;
extern cvar_t r_bloom_alpha;
extern cvar_t r_bloom_diamond_size;
extern cvar_t r_bloom_intensity;
extern cvar_t r_bloom_sample_size;
extern cvar_t r_bloom_fast_sample;
void R_BloomBlend(void);
void R_InitBloomTextures(void);


// WATER SHADER =======================//

// Retrieves all the function addresses for OpenGL extensions needed for shaders
void RetrieveGlextShaderFuncs(void);

// Compiles the shader and links the program on the GPU
void CompileShaders(void);

// Creates the frame buffer objects on startup
void CreateFBOs(void);

// Checks for errors on shaders source code, reported by the OpenGL driver
qboolean CheckShaderError(GLuint);

// Checks for error on linking the shader program from the OpenGL driver
qboolean CheckProgramError(GLuint);

// Enables the water shader just prior to emitting the surface polys
void ShaderBegin(void);

// Restores non-shader operation
void ShaderEnd(void);

// Sets the uniforms and the textures needed for the water shader, just before drawing
void ShaderSetup(msurface_t* fa);

// If this is 'true' the shader extensions are available
extern qboolean ShaderExtPresent;

// Enum to make aware the rendering routines if we are on a special render
typedef enum { SR_NORMAL, SR_REFRACTION, SR_REFLECTION, SR_FLATNORMALS } specialrender_t; // September 2021

#ifdef _WIN32
extern int specialrender;
#else
extern specialrender_t specialrender;
#endif

// ID name for the water shader program
extern int ShaderProgs;

void Matrix4x4_CM_Projection_Inf(float* proj, float fovx, float fovy, float neard);

typedef struct
{
	vec3_t	normal;
	float	dist;
} plane_t;

typedef struct
{
	float x;
	float y;
	float z;
	float w;
} Vector4D;

void ModifyProjectionMatrix(Vector4D* clipPlane);

// Backup values for after reflection render pass
extern vec3_t OriginalVpn;
extern vec3_t OriginalUp;
extern vec3_t OriginalRight;
extern refdef_t OriginalRef;
extern float OriginalProj[16];

// Stores/backs-up all the relevant original details before reflection mirrored pass
void StoreOriginalOrgAndAngles(void);

// Restores original stuff after reflection pass and before refraction
void RestoreOriginalOrgAndAngles(void);

// Binds a specific frame buffer object (FBO)
void bindFrameBuffer(int frameBuffer);

// Restores normal rendering mode, not any FBO
void unbindCurrentFrameBuffer(void);

// From fte
void R_MirrorMatrix(plane_t* plane);
void R_ObliqueNearClip(float* viewmat, mplane_t* wplane);
void Matrix4x4_CM_ModelViewMatrixFromAxis(float* modelview, const vec3_t pn, const vec3_t right, const vec3_t up, const vec3_t vieworg);

extern int reflectionFrameBuffer;
extern int reflectionTexture;
extern int reflectionDepthTexture;

extern int refractionFrameBuffer;
extern int refractionTexture;
extern int refractionDepthTexture;

qboolean IsUnderWaterShader(vec3_t org);

size_t strlcat(char* dst, const char* src, size_t siz);

extern cvar_t cl_crypt_rcon;// = { "","" };
// Sha1 code from ezquake
/*
SHA-1 in C
By Steve Reid <steve@edmweb.com>
100% Public Domain

Test Vectors (from FIPS PUB 180-1)
"abc"
  A9993E36 4706816A BA3E2571 7850C26C 9CD0D89D
"abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq"
  84983E44 1C3BD26E BAAE4AA1 F95129E5 E54670F1
A million repetitions of "a"
  34AA973C D4C4DAA4 F61EEB2B DBAD2731 6534016F

	$Id: sha1.h,v 1.9 2007-08-08 00:53:29 disconn3ct Exp $
*/

#define SHA1HANDSOFF /* Copies data before messing with it. */
#ifndef _SHA1
#define _SHA1
typedef struct {
	unsigned int state[5];
	unsigned int count[2];
	unsigned char buffer[64];
} SHA1_CTX;

#define DIGEST_SIZE 20
void SHA1Transform(unsigned int state[5], unsigned char buffer[64]);
void SHA1Init(SHA1_CTX* context);
void SHA1Update(SHA1_CTX* context, unsigned char* data, unsigned int len);
void SHA1Final(unsigned char digest[DIGEST_SIZE], SHA1_CTX* context);

#define rol(value, bits) (((value) << (bits)) | ((value) >> (32 - (bits))))

/* blk0() and blk() perform the initial expand. */
/* I got the idea of expanding during the round function from SSLeay */
/*
#ifdef __BIG_ENDIAN
#define blk0(i) block->l[i]
#else
#define blk0(i) (block->l[i] =	(rol(block->l[i], 24) & 0xFF00FF00) | \
								(rol(block->l[i],  8) & 0x00FF00FF))
#endif
*/

#define blk0(i) (block->l[i] = BigLong(block->l[i]))

#define blk(i) (block->l[i&15] = rol(block->l[(i+13)&15]^block->l[(i+8)&15] \
    ^block->l[(i+2)&15]^block->l[i&15],1))

/* (R0+R1), R2, R3, R4 are the different operations used in SHA1 */
#define R0(v,w,x,y,z,i) z+=((w&(x^y))^y)+blk0(i)+0x5A827999+rol(v,5);w=rol(w,30);
#define R1(v,w,x,y,z,i) z+=((w&(x^y))^y)+blk(i)+0x5A827999+rol(v,5);w=rol(w,30);
#define R2(v,w,x,y,z,i) z+=(w^x^y)+blk(i)+0x6ED9EBA1+rol(v,5);w=rol(w,30);
#define R3(v,w,x,y,z,i) z+=(((w|x)&y)|(w&x))+blk(i)+0x8F1BBCDC+rol(v,5);w=rol(w,30);
#define R4(v,w,x,y,z,i) z+=(w^x^y)+blk(i)+0xCA62C1D6+rol(v,5);w=rol(w,30);

//VVD: SHA1 crypt
char* SHA1(char* string);
void SHA1_Init(void);
void SHA1_Update(unsigned char* data);
char* SHA1_Final(void);
char* bin2hex(unsigned char* d);
#endif //_SHA1

void R_OutlinesSetup(void);
qboolean CompileOutlinesShaders(void);
void R_DrawSurfaceNormalsForOutlines(void);
void R_OutlinesDraw(void);

extern cvar_t      r_outlines;
extern cvar_t      r_outlines_width;
extern cvar_t      r_outlines_color;
extern cvar_t      r_outlines_factor;

//#define DOC_CHEATS

#ifdef DOC_CHEATS
extern cvar_t doc_cheats;
#endif

#endif // !_OFTEN_H_
