/*=======================================================================================//
 OfteN.c - Basically all things I don't know where to put - This was signed on 29-12-2020
 ----------------------------------------------------------------------------------------
 Written by Sergi Fumanya Grunwaldt - For ProzacQW/Prozac QuakeWorld 0.36 and upwards...
//=======================================================================================*/

#include "quakedef.h"
#include "pmove.h"
#include "OfteN.h"
#include "gl_local.h"

#ifdef _WIN32
#include "GL/Glu.h"
#else
#include <GL/glu.h>
#endif

#ifndef _WIN32
//#define GL_GLEXT_PROTOTYPES
#endif

#include "glext.h"

//========== DEBUG FLAG (Adds Testing command) ========
//#define OFN_DEBUG
//========== DEBUG FLAG (Adds Testing command) ========

#ifdef DOC_CHEATS
cvar_t doc_cheats = { "doc_cheats", "0", 0 };
#endif

#define C_CYLINDER_NUM_VERTICES    16 // No point in making cylinders more detailed

const char* GluVersion;

int	coronatexture;
int	gunflashtexture;
int	explosionflashtexture1;
int	explosionflashtexture2;
int	explosionflashtexture3;
int	explosionflashtexture4;
int	explosionflashtexture5;
int	explosionflashtexture6;
int	explosionflashtexture7;

int CoronaCount, CoronaCountHigh;

qboolean ShaderExtPresent = false;
int ShaderProgs = 0;

int reflectionFrameBuffer = 0;
int reflectionTexture = 0;
int reflectionDepthTexture = 0;

int refractionFrameBuffer = 0;
int refractionTexture = 0;
int refractionDepthTexture = 0;

int dudvMapTexture = 0;
int normalMapTexture = 0;

// November 2023
int ShaderProgsOutlines = 0;
int outlinesFlatNormalsTexture = 0;
int backupOutlinesTexture = 0;
int outlinesDepthTexture = 0;
int outlinesFrameBuffer = 0;

vec3_t OriginalVpn;
vec3_t OriginalUp;
vec3_t OriginalRight;
refdef_t OriginalRef;

// GLExtensions stuff for shaders
//#ifdef _WIN32
PFNGLCREATESHADERPROC glCreateShader;
PFNGLSHADERSOURCEPROC glShaderSource;
PFNGLCOMPILESHADERPROC glCompileShader;
PFNGLCREATEPROGRAMPROC glCreateProgram;
PFNGLATTACHSHADERPROC glAttachShader;
PFNGLDELETESHADERPROC glDeleteShader;
PFNGLLINKPROGRAMPROC glLinkProgram;
PFNGLUSEPROGRAMPROC glUseProgram;
PFNGLDELETEPROGRAMPROC glDeleteProgram;

PFNGLGETSHADERIVPROC glGetShaderiv;
PFNGLGETPROGRAMIVPROC glGetProgramiv;
PFNGLGETSHADERINFOLOGPROC glGetShaderInfoLog;
PFNGLGETPROGRAMINFOLOGPROC glGetProgramInfoLog;

PFNGLUNIFORM1IPROC glUniform1i;
PFNGLUNIFORM4FVPROC glUniform4fv;
PFNGLUNIFORMMATRIX2FVPROC glUniformMatrix2fv;
PFNGLUNIFORM3FPROC glUniform3f;
PFNGLUNIFORM1FPROC glUniform1f;
PFNGLUNIFORMMATRIX4FVPROC glUniformMatrix4fv;
PFNGLGETUNIFORMLOCATIONPROC glGetUniformLocation;

PFNGLGENFRAMEBUFFERSPROC glGenFramebuffers;
PFNGLBINDFRAMEBUFFERPROC glBindFramebuffer;
PFNGLFRAMEBUFFERTEXTURE2DPROC glFramebufferTexture2D;

PFNGLGENRENDERBUFFERSPROC glGenRenderbuffers;
PFNGLBINDRENDERBUFFERPROC glBindRenderbuffer;
PFNGLNAMEDRENDERBUFFERSTORAGEPROC glRenderbufferStorage;
PFNGLFRAMEBUFFERRENDERBUFFERPROC glFramebufferRenderbuffer;

PFNGLDELETEFRAMEBUFFERSPROC glDeleteFramebuffers;
PFNGLDELETERENDERBUFFERSPROC glDeleteRenderbuffers;
PFNGLCHECKFRAMEBUFFERSTATUSPROC glCheckFramebufferStatus;

#ifdef OFN_DEBUG
/*char* TestFBO(GLuint tfbo);
typedef BOOL(APIENTRY* SWAPINTERVALFUNCPTR)(int);
extern SWAPINTERVALFUNCPTR wglSwapIntervalEXT;
extern HDC maindc;*/

extern cvar_t gl_consolefont;
//extern int char_texture;

int Image_WritePNGwithAlpha(char* filename, int compression, byte* pixels, int width, int height);

#define CHARSET_SIZE           512
#define CHARSET_CHAR_SIZE      CHARSET_SIZE/16
#define CHARSET_RESULT_DIV     2 // Must be 1 or 2
#define CHARSET_RESULT_SMOOTH  false // only should be true when div is 2 and want smooth scaling (doesn't always look better)

void TestCommand_f(void)
{
	char tstr[100];
	
	sprintf(tstr, "%d", CHARSET_SIZE / CHARSET_RESULT_DIV);
	
	Sys_mkdir(va("HTML_charsets/%s/%s", gl_consolefont.string, tstr));

	unsigned int* buffer;
	unsigned int subbuffer[(CHARSET_CHAR_SIZE * CHARSET_CHAR_SIZE)/CHARSET_RESULT_DIV];

	buffer = (unsigned int*)GL_LoadImagePixels(va("textures/charsets/%s", gl_consolefont.string), 0, 0, 0);

	if (!buffer)
	{
		Com_Printf("Unable to load charset for export!\n");
		return;
	}

	if (CHARSET_RESULT_DIV != 1 && CHARSET_RESULT_DIV != 2)
	{
		Com_Printf("Invalid scale for charset!\n");
		free(buffer);
		return;
	}

	int x, y, a, b, r, g, v, p, res, a2 , b2;

	for (y = 0; y < 16; y++)
	{
		for (x = 0; x < 16; x++)
		{
			for (a = 0, a2 = 0; a < CHARSET_CHAR_SIZE; a += CHARSET_RESULT_DIV, a2++)
			{
				for (b = 0, b2 = 0; b < CHARSET_CHAR_SIZE; b += CHARSET_RESULT_DIV, b2++)
				{
					if (CHARSET_RESULT_DIV == 1)
						subbuffer[a + (b * CHARSET_CHAR_SIZE)] = buffer[(x * CHARSET_CHAR_SIZE + a + (y * CHARSET_CHAR_SIZE + b) * CHARSET_SIZE)];
					else
					{
						if (CHARSET_RESULT_SMOOTH)
						{
							r = g = v = p = res = 0;

							r += buffer[(x * CHARSET_CHAR_SIZE + a + (y * CHARSET_CHAR_SIZE + (b + 1)) * CHARSET_SIZE)] & 0xFF000000;
							r += buffer[(x * CHARSET_CHAR_SIZE + (a + 1) + (y * CHARSET_CHAR_SIZE + b) * CHARSET_SIZE)] & 0xFF000000;
							r += buffer[(x * CHARSET_CHAR_SIZE + (a + 1) + (y * CHARSET_CHAR_SIZE + (b + 1)) * CHARSET_SIZE)] & 0xFF000000;
							r += buffer[(x * CHARSET_CHAR_SIZE + a + (y * CHARSET_CHAR_SIZE + b) * CHARSET_SIZE)] & 0xFF000000;

							g += buffer[(x * CHARSET_CHAR_SIZE + a + (y * CHARSET_CHAR_SIZE + (b + 1)) * CHARSET_SIZE)] & 0x00FF0000;
							g += buffer[(x * CHARSET_CHAR_SIZE + (a + 1) + (y * CHARSET_CHAR_SIZE + b) * CHARSET_SIZE)] & 0x00FF0000;
							g += buffer[(x * CHARSET_CHAR_SIZE + (a + 1) + (y * CHARSET_CHAR_SIZE + (b + 1)) * CHARSET_SIZE)] & 0x00FF0000;
							g += buffer[(x * CHARSET_CHAR_SIZE + a + (y * CHARSET_CHAR_SIZE + b) * CHARSET_SIZE)] & 0x00FF0000;

							v += buffer[(x * CHARSET_CHAR_SIZE + a + (y * CHARSET_CHAR_SIZE + (b + 1)) * CHARSET_SIZE)] & 0x0000FF00;
							v += buffer[(x * CHARSET_CHAR_SIZE + (a + 1) + (y * CHARSET_CHAR_SIZE + b) * CHARSET_SIZE)] & 0x0000FF00;
							v += buffer[(x * CHARSET_CHAR_SIZE + (a + 1) + (y * CHARSET_CHAR_SIZE + (b + 1)) * CHARSET_SIZE)] & 0x0000FF00;
							v += buffer[(x * CHARSET_CHAR_SIZE + a + (y * CHARSET_CHAR_SIZE + b) * CHARSET_SIZE)] & 0x0000FF00;

							p += buffer[(x * CHARSET_CHAR_SIZE + a + (y * CHARSET_CHAR_SIZE + (b + 1)) * CHARSET_SIZE)] & 0x000000FF;
							p += buffer[(x * CHARSET_CHAR_SIZE + (a + 1) + (y * CHARSET_CHAR_SIZE + b) * CHARSET_SIZE)] & 0x000000FF;
							p += buffer[(x * CHARSET_CHAR_SIZE + (a + 1) + (y * CHARSET_CHAR_SIZE + (b + 1)) * CHARSET_SIZE)] & 0x000000FF;
							p += buffer[(x * CHARSET_CHAR_SIZE + a + (y * CHARSET_CHAR_SIZE + b) * CHARSET_SIZE)] & 0x000000FF;

							r = rint((r >> 24) / 4);
							g = rint((g >> 16) / 4);
							v = rint((v >> 8) / 4);
							p = rint(p / 4);

							res |= (r << 24);
							res |= (g << 16);
							res |= (v << 8);
							res |= p;

							subbuffer[a2 + (b2 * (CHARSET_CHAR_SIZE / CHARSET_RESULT_DIV))] = res;
						}
						else
							subbuffer[a2 + (b2 * (CHARSET_CHAR_SIZE / CHARSET_RESULT_DIV))] = buffer[(x * CHARSET_CHAR_SIZE + a + (y * CHARSET_CHAR_SIZE + b) * CHARSET_SIZE)];
					}
				}				
			}

			Image_WritePNGwithAlpha(va("HTML_charsets/%s/%s/%d.png", gl_consolefont.string, tstr, x + (y * 16)), 0,(byte*)subbuffer, CHARSET_CHAR_SIZE / CHARSET_RESULT_DIV, CHARSET_CHAR_SIZE / CHARSET_RESULT_DIV);
		}
	}

	free(buffer);
	
	//;
	//block_drawing = true;
	//wglSwapIntervalEXT(1);
	//SwapBuffers(maindc);

	//Com_Printf("Refraction buffer: %s\n", TestFBO(refractionFrameBuffer));
	//Com_Printf("Reflection buffer: %s\n", TestFBO(reflectionFrameBuffer));
}
#endif

#ifdef _WIN32
void CopyIntegrationString_f(void);
#endif

//===========================================================================================
// Initializes all needed stuff global for the client system

void OfN_InitClient(void)
{
#ifdef OFN_DEBUG
	Cmd_AddCommand("testa", TestCommand_f);
#endif

#ifdef _WIN32
	Cmd_AddCommand("copy_integration_str", CopyIntegrationString_f);
#endif

#ifdef DOC_CHEATS
	Cvar_Register(&doc_cheats);
#endif

	// Get the static string with the GLU version
    GluVersion = gluGetString(GLU_VERSION);

	// Get the functions for water shaders
	RetrieveGlextShaderFuncs();

	// Try to compile shaders
	CompileShaders();

	// Create the shader FBO's
	CreateFBOs();

	// Create backup texture for outlines
	R_OutlinesSetup();

	// Apply antialiasing - Doesn't look right

	/*if (wglGetProcAddress("wglGetExtensionsStringARB"))
		Com_Printf("wglGetExtensions found\n");
	else
		Com_Printf("wglGetExtensions NOT found\n");*/

	/*glEnable(GL_POINT_SMOOTH);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_POLYGON_SMOOTH);

	glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);*/

	// Initialize Coronas
	int flags = TEX_COMPLAIN | TEX_MIPMAP | TEX_ALPHA | TEX_NOSCALE;
		
	coronatexture = GL_LoadTextureImage("textures/flash", NULL, 0, 0, flags);
	gunflashtexture = GL_LoadTextureImage("textures/gunflash", NULL, 0, 0, flags);
	explosionflashtexture1 = GL_LoadTextureImage("textures/explosionflash1", NULL, 0, 0, flags);
	explosionflashtexture2 = GL_LoadTextureImage("textures/explosionflash2", NULL, 0, 0, flags);
	explosionflashtexture3 = GL_LoadTextureImage("textures/explosionflash3", NULL, 0, 0, flags);
	explosionflashtexture4 = GL_LoadTextureImage("textures/explosionflash4", NULL, 0, 0, flags);
	explosionflashtexture5 = GL_LoadTextureImage("textures/explosionflash5", NULL, 0, 0, flags);
	explosionflashtexture6 = GL_LoadTextureImage("textures/explosionflash6", NULL, 0, 0, flags);
	explosionflashtexture7 = GL_LoadTextureImage("textures/explosionflash7", NULL, 0, 0, flags);
	InitCoronas(); // safe re-init	

	// Load DUDV/normal map textures
	dudvMapTexture = GL_LoadTextureImage("textures/water4DUDV", NULL, 0, 0, TEX_COMPLAIN);
	normalMapTexture = GL_LoadTextureImage("textures/normalMap", NULL, 0, 0, TEX_COMPLAIN);
}

//===========================================================================================
// Does any needed clean-up on client shutdown

void OfN_ShutdownClient(void)
{
	if (ShaderExtPresent)
	{
		glDeleteProgram(ShaderProgs);

		glDeleteFramebuffers(1, &reflectionFrameBuffer);
		glDeleteTextures(1, &reflectionTexture);
		glDeleteTextures(1, &reflectionDepthTexture); // added September 2021

		glDeleteFramebuffers(1, &refractionFrameBuffer);
		glDeleteTextures(1, &refractionTexture);
		glDeleteTextures(1, &refractionDepthTexture); // Uncommented on September 2021

		glDeleteFramebuffers(1, &outlinesFrameBuffer);
		glDeleteProgram(ShaderProgsOutlines);
		glDeleteTextures(1, &backupOutlinesTexture);
		glDeleteTextures(1, &outlinesFlatNormalsTexture);
		glDeleteTextures(1, &outlinesDepthTexture);
	}
}

//===========================================================================================
// Gets the first vector for cylinder calculation, this was ported from a Java code snippet

void OfN_Cyl_getFirstPerpVector(float x, float y, float z, vec3_t result) 
{
    result[0] = result[1] = result[2] = 0.0f;
    // That's easy.
    if (x == 0.0f || y == 0.0f || z == 0.0f) {
        if (x == 0.0f)
            result[0] = 1.0f;
        else if (y == 0.0f)
            result[1] = 1.0f;
        else
            result[2] = 1.0f;
    }
    else {
        // If xyz is all set, we set the z coordinate as first and second argument .
        // As the scalar product must be zero, we add the negated sum of x and y as third argument
        result[0] = z;      //scalp = z*x
        result[1] = z;      //scalp = z*(x+y)
        result[2] = -(x + y); //scalp = z*(x+y)-z*(x+y) = 0
        // Normalize vector
        float length = 0.0f;
        
        length += result[0] * result[0];
        length += result[1] * result[1];
        length += result[2] * result[2];

        length = (float)sqrt(length);
        for (int i = 0; i < 3; i++)
            result[i] /= length;
    }    
}

//===============================================================================================
// Actually draws the cylinder between the 2 3D coord vectors, specifying thickness and color

void OfN_drawCylinder(float x1, float y1, float z1, float x2, float y2, float z2, float thick, col_t color)
{
    int X = 0,
        Y = 1,
        Z = 2;
    // Get components of difference vector
    float x = x1 - x2,
        y = y1 - y2,
        z = z1 - z2;
    vec3_t firstPerp;
    OfN_Cyl_getFirstPerpVector(x, y, z, firstPerp);
    // Get the second perp vector by cross product
    vec3_t secondPerp;
    secondPerp[X] = y * firstPerp[Z] - z * firstPerp[Y];
    secondPerp[Y] = z * firstPerp[X] - x * firstPerp[Z];
    secondPerp[Z] = x * firstPerp[Y] - y * firstPerp[X];
    // Normalize vector
    float length = 0.0f;
    
    length += secondPerp[0] * secondPerp[0];
    length += secondPerp[1] * secondPerp[1];
    length += secondPerp[2] * secondPerp[2];

    length = (float)sqrt(length);
    for (int i = 0; i < 3; i++)
        secondPerp[i] /= length;

    // Having now our vectors, here we go:
    // First points; you can have a cone if you change the radius R1
    int ANZ = C_CYLINDER_NUM_VERTICES;  // number of vertices Original 32
    float FULL = (float)(2.0f * M_PI),
        R1 = thick;// 2.0f; // radius Original 4.0f
    
    float points[C_CYLINDER_NUM_VERTICES + 1][3]; // 32 was ANZ
    for (int i = 0; i < ANZ; i++) {
        float angle = FULL * (i / (float)ANZ);

        points[i][X] = (float)(R1 * (cos(angle) * firstPerp[X] + sin(angle) * secondPerp[X]));
        points[i][Y] = (float)(R1 * (cos(angle) * firstPerp[Y] + sin(angle) * secondPerp[Y]));
        points[i][Z] = (float)(R1 * (cos(angle) * firstPerp[Z] + sin(angle) * secondPerp[Z]));
    }
    // Set last to first
    for (int i = 0; i < 3; i++)
        points[ANZ][i] = points[0][i];
    
    glColor4ubv(color);
    glBegin(GL_TRIANGLE_FAN);
    glVertex3f(x1, y1, z1);
    for (int i = 0; i <= ANZ; i++) {
        glVertex3f(x1 + points[i][X],
            y1 + points[i][Y],
            z1 + points[i][Z]);
    }
    glEnd();

    glBegin(GL_TRIANGLE_FAN);
    glVertex3f(x2, y2, z2);
    for (int i = 0; i <= ANZ; i++) {
        glVertex3f(x2 + points[i][X],
            y2 + points[i][Y],
            z2 + points[i][Z]);
    }
    glEnd();

    glBegin(GL_QUAD_STRIP);
    for (int i = 0; i <= ANZ; i++) {
        glVertex3f(x1 + points[i][X],
            y1 + points[i][Y],
            z1 + points[i][Z]);
        glVertex3f(x2 + points[i][X],
            y2 + points[i][Y],
            z2 + points[i][Z]);
    }
    glEnd();
}

//=============================================================================================================
// Draws a sphere at the given origin, with supplied radius and color. Using given number of vertices

void OfN_drawSphere(vec3_t org, double radius, int vertnum, col_t color)
{   
    GLUquadricObj* ObjQuadric;

    ObjQuadric = gluNewQuadric();

    gluQuadricDrawStyle(ObjQuadric, GLU_FILL);
    gluQuadricOrientation(ObjQuadric, GLU_INSIDE);
    gluQuadricNormals(ObjQuadric, GLU_SMOOTH);

    glPushMatrix();

    glTranslatef(org[0], org[1], org[2]);
    glColor4ubv(color);

    gluSphere(ObjQuadric, radius, vertnum, vertnum);

    glPopMatrix();

    gluDeleteQuadric(ObjQuadric);
}

//============================================================================================================
// Draws a circular-shaped disk at the origin specified, with the given amount of vertices and color

void OfN_drawDisk(vec3_t org, float anglex, float angley, float anglez, double innerRadius, double outerRadius, int vertnum, col_t color)
{
    GLUquadricObj* ObjQuadric;

    ObjQuadric = gluNewQuadric();

    gluQuadricDrawStyle(ObjQuadric, GLU_FILL);
    gluQuadricOrientation(ObjQuadric, GLU_OUTSIDE);
    gluQuadricNormals(ObjQuadric, GLU_SMOOTH);

    glPushMatrix();

    glTranslatef(org[0], org[1], org[2]);
    glRotatef(angley, 0, 1, 0);
    glRotatef(anglex, 1, 0, 0);
    glRotatef(anglez, 0, 0, 1);
    glColor4ubv(color);

    gluDisk(ObjQuadric, innerRadius, outerRadius, vertnum, vertnum);

    glPopMatrix();

    gluDeleteQuadric(ObjQuadric);
}

//============================================================================================================
// Draws an arc-shaped plane object at the origin specified, with the given amount of vertices and color

void OfN_drawPartialDisk(vec3_t org, float anglex, float angley, float anglez, double innerRadius, double outerRadius, int vertnum, col_t color, double startangle, double sweepangle)
{
    GLUquadricObj* ObjQuadric;

    ObjQuadric = gluNewQuadric();

    gluQuadricDrawStyle(ObjQuadric, GLU_FILL);
    gluQuadricOrientation(ObjQuadric, GLU_OUTSIDE);
    gluQuadricNormals(ObjQuadric, GLU_SMOOTH);

    glPushMatrix();

    glTranslatef(org[0], org[1], org[2]);
    glRotatef(angley, 0, 1, 0);
    glRotatef(anglex, 1, 0, 0);
    glRotatef(anglez, 0, 0, 1);    
    glColor4ubv(color);

    gluPartialDisk(ObjQuadric, innerRadius, outerRadius, vertnum, vertnum, startangle, sweepangle);

    glPopMatrix();

    gluDeleteQuadric(ObjQuadric);
}

//=================================================================================================================
// Utility function to calculate the yaw angle based on 2 vectors difference

float VectoYaw(vec3_t vector1, vec3_t vector2)
{
    vec3_t yaw;
    float angleyaw;
    VectorSubtract(vector1, vector2, yaw);
    angleyaw = (atan2(yaw[1], yaw[0]) * 180 / M_PI);

    return -angleyaw;
}

//=================================================================================================================
// Utility function to calculate the pitch angle based on 2 vectors difference

float VectoPitch(vec3_t vector1, vec3_t vector2)
{    
    vec3_t dist;

    if (vector1[0] < vector2[0])
        VectorSubtract(vector2, vector1, dist);
    else
        VectorSubtract(vector1, vector2, dist);

    float forward = sqrt((double)dist[0] * dist[0] + (double)dist[1] * dist[1]);
    float anglepitch;
        
    anglepitch = (atan2(dist[2], forward) * 180 / M_PI);

    return -(anglepitch-90);
}

//=================================================================================================================================//
// VULT/EZQuake CORONA's Implementation ===========================================================================================//
//=================================================================================================================================//

//fixme: move to header
extern float bubblecolor[NUM_DLIGHTTYPES][4];
void CoronaStats(int change);

typedef struct
{
	vec3_t	origin;
	float scale;
	float growth;
	float	die;
	vec3_t color;
	float alpha;
	float fade;
	coronatype_t type;
	qboolean sighted;
	qboolean los; //to prevent having to trace a line twice
	entity_t* serialhint;//is a serial to avoid recreating stuff
	int texture;
} corona_t;

//Tei: original whas 256, upped so whas low (?!) for some games
#define MAX_CORONAS 300

corona_t	r_corona[MAX_CORONAS];

#define CORONA_SCALE 130
#define CORONA_ALPHA 1

void R_UpdateCoronas(void)
{
	int i;
	corona_t* c;
	vec3_t impact = { 0,0,0 }, normal;
	float frametime = cls.frametime;

	CoronaStats(-CoronaCount);
	for (i = 0; i < MAX_CORONAS; i++)
	{
		c = &r_corona[i];

		if (c->type == C_FREE)
			continue;
		if (c->type == C_EXPLODE)
		{
			if (c->die < cl.time && c->texture != explosionflashtexture7)
			{
				c->texture++;
				c->die = cl.time + 0.03;
			}
		}
		//First, check to see if its time to die.
		if (c->die < cl.time || c->scale <= 0 || c->alpha <= 0)
		{
			//Free this corona up.
			c->scale = 0;
			c->alpha = 0;
			c->type = C_FREE;
			c->sighted = false;
			c->serialhint = 0;//so can be reused
		}
		CoronaStats(1);
		c->scale += c->growth * frametime;
		c->alpha += c->fade * frametime;

		CL_TraceLine(r_refdef.vieworg, c->origin, impact, normal);
		if (!VectorCompare(impact, c->origin))//Can't see it, so make it fade out(faster)
		{
			c->los = false;

			c->scale = 0;
			c->alpha = 0;

			//Tei: this has been commented out for multiplayer,
			// because some coronas see trough walls and can be cheat.
			// #1041604	Eyecandy - cheaty (2nd)
			// OfN UNcommented May 2021
			if (c->type == C_FIRE)
			{
				c->fade = -0.5;
				c->growth = -1200;
			}
			else
			{
				c->fade += c->fade;
				c->growth += c->growth;
			}/*END of the UNComment*/
		}
		else
		{
			c->los = true;
			if (c->type == C_FIRE)
			{
				c->fade = 1.5;
				c->growth = 1000;
				if (c->scale > 150)
					c->scale = 150 + rand() % 15; //flicker when at full radius
				if (c->alpha > 0.2f)//TODO: make a cvar to control this
					c->alpha = 0.2f;// .. coronacontrast or something
			}
			c->sighted = true;
		}
	}

}

// September 2021
extern cvar_t gl_fog;

//R_DrawCoronas
void R_DrawCoronas(void)
{
	int i;
	int texture = 0;
	vec3_t dist, up, right;
	float fdist, scale, alpha;
	corona_t* c;

	if (gl_fog.value)
	{
		glDisable(GL_FOG);
	}
	
	/*if (!ISPAUSED)*/
	if (specialrender == SR_NORMAL)
		R_UpdateCoronas();

	VectorScale(vup, 1, up);//1.5
	VectorScale(vright, 1, right);

	GL_Bind(coronatexture);
	glEnable(GL_BLEND);
	glDepthMask(GL_FALSE);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	glShadeModel(GL_SMOOTH);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glDisable(GL_DEPTH_TEST);
	for (i = 0; i < MAX_CORONAS; i++)
	{
		c = &r_corona[i];
		if (c->type == C_FREE)
			continue;
		if (!c->los) //can't see it and
			if (!c->sighted) //haven't seen it before
				continue; //dont draw it
			//else it will be fading out, so thats 'kay

		// September 2021
		//if (specialrender == SR_REFLECTION && IsUnderWaterShader(c->origin))
		if (specialrender != SR_NORMAL && IsUnderWaterShader(c->origin))
			continue;

		VectorSubtract(r_refdef.vieworg, c->origin, dist);
		fdist = VectorLength(dist);
		if (fdist <= 24) //its like being fired into the sun
			continue;
		if (c->texture != texture)
		{
			GL_Bind(c->texture);
			texture = c->texture;
		}
		scale = (1 - 1 / fdist) * c->scale;
		alpha = c->alpha;
		glBegin(GL_QUADS);
		glColor4f(c->color[0], c->color[1], c->color[2], alpha);
		//This is the order I used to draw my vertexes in, I blame kay for this, she told me to put them that way
/*		glTexCoord2f (1,0);
		glTexCoord2f (1,1);
		glTexCoord2f (0,1);
		glTexCoord2f (0,0);*/

		glTexCoord2f(0, 0);
		glVertex3f(c->origin[0] + up[0] * (scale / 2) + (right[0] * (scale / 2) * -1), c->origin[1] + up[1] * (scale / 2) + (right[1] * (scale / 2) * -1), c->origin[2] + up[2] * (scale / 2) + (right[2] * (scale / 2) * -1));
		glTexCoord2f(1, 0);
		glVertex3f(c->origin[0] + right[0] * (scale / 2) + up[0] * (scale / 2), c->origin[1] + right[1] * (scale / 2) + up[1] * (scale / 2), c->origin[2] + right[2] * (scale / 2) + up[2] * (scale / 2));
		glTexCoord2f(1, 1);
		glVertex3f(c->origin[0] + right[0] * (scale / 2) + (up[0] * (scale / 2) * -1), c->origin[1] + right[1] * (scale / 2) + (up[1] * (scale / 2) * -1), c->origin[2] + right[2] * (scale / 2) + (up[2] * (scale / 2) * -1));
		glTexCoord2f(0, 1);
		glVertex3f(c->origin[0] + (right[0] * (scale / 2) * -1) + (up[0] * (scale / 2) * -1), c->origin[1] + (right[1] * (scale / 2) * -1) + (up[1] * (scale / 2) * -1), c->origin[2] + (right[2] * (scale / 2) * -1) + (up[2] * (scale / 2) * -1));
		glEnd();
		//Its sort of cheap, but lets draw a few more here to make the effect more obvious
		if (c->type == C_FLASH || c->type == C_BLUEFLASH)
		{
			int a;
			for (a = 0; a < 5; a++)
			{
				glBegin(GL_QUADS);
				glColor4f(c->color[0], c->color[1], c->color[2], alpha);
				glTexCoord2f(0, 0);
				glVertex3f(c->origin[0] + up[0] * (scale / 30) + (right[0] * (scale) * -1), c->origin[1] + up[1] * (scale / 30) + (right[1] * (scale) * -1), c->origin[2] + up[2] * (scale / 30) + (right[2] * (scale) * -1));
				glTexCoord2f(1, 0);
				glVertex3f(c->origin[0] + right[0] * (scale)+up[0] * (scale / 30), c->origin[1] + right[1] * (scale)+up[1] * (scale / 30), c->origin[2] + right[2] * (scale)+up[2] * (scale / 30));
				glTexCoord2f(1, 1);
				glVertex3f(c->origin[0] + right[0] * (scale)+(up[0] * (scale / 30) * -1), c->origin[1] + right[1] * (scale)+(up[1] * (scale / 30) * -1), c->origin[2] + right[2] * (scale)+(up[2] * (scale / 30) * -1));
				glTexCoord2f(0, 1);
				glVertex3f(c->origin[0] + (right[0] * (scale) * -1) + (up[0] * (scale / 30) * -1), c->origin[1] + (right[1] * (scale) * -1) + (up[1] * (scale / 30) * -1), c->origin[2] + (right[2] * (scale) * -1) + (up[2] * (scale / 30) * -1));
				glEnd();
			}
		}
		continue;
	}
	glEnable(GL_DEPTH_TEST);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glShadeModel(GL_FLAT);
	glColor3f(1, 1, 1);

	if (gl_fog.value)
		glEnable(GL_FOG);

}

//NewCorona
void NewCorona(coronatype_t type, vec3_t origin)
{
	corona_t* c = NULL;
	int i;
	qboolean corona_found = false;
	//customlight_t cst_lt = { 0 };

	/*if (ISPAUSED)
	{
		return;
	}*/

	c = r_corona;

	for (i = 0; i < MAX_CORONAS; i++, c++)
	{
		if (c->type == C_FREE)
		{
			memset(c, 0, sizeof(*c));
			corona_found = true;
			break;
		}
	}

	if (!corona_found)
	{
		//Tei: last attemp to get a valid corona to "canivalize"
		c = r_corona;
		for (i = 0; i < MAX_CORONAS; i++, c++)
		{
			//Search a fire corona that is about to die soon
			if ((c->type == C_FIRE) &&
				(c->die < (cl.time + 0.1f) || c->scale <= 0.1f || c->alpha <= 0.1f)
				)
			{
				memset(c, 0, sizeof(*c));
				corona_found = true;
				//sucesfully canivalize a fire corona that whas about to die.
				break;
			}
		}
		//If can't canivalize a corona, It exit silently
		//This is the worst case scenario, and will never happend
		return;
	}

	c->sighted = false;
	VectorCopy(origin, c->origin);
	c->type = type;
	c->los = false;
	c->texture = coronatexture;
	if (type == C_FLASH || type == C_BLUEFLASH)
	{
		/*if (type == C_BLUEFLASH)
			VectorCopy(bubblecolor[lt_blue], c->color);
		else
		{
			dlightColorEx(r_explosionlightcolor.value, r_explosionlightcolor.string, lt_explosion, false, &cst_lt);
			if (cst_lt.type == lt_custom) {
				VectorCopy(cst_lt.color, c->color);
				VectorScale(c->color, (1.0 / 255), c->color); // cast byte to float
			}
			else
				VectorCopy(bubblecolor[cst_lt.type], c->color);
			VectorMA(c->color, 1.5, c->color, c->color);
		}*/

		if (type == C_BLUEFLASH)
			VectorCopy(bubblecolor[lt_blue], c->color); // late May 2021
		else
			VectorCopy(bubblecolor[lt_explosion], c->color); // late May 2021

		c->scale = 600;
		c->die = cl.time + 0.2;
		c->alpha = 0.25;
		c->fade = 0;
		c->growth = -3000;
	}
	else if (type == C_SMALLFLASH)
	{
		c->color[0] = 1;
		c->color[1] = 0.8;
		c->color[2] = 0.3;
		c->scale = 150;
		c->die = cl.time + 0.1;
		c->alpha = 0.66;
		c->fade = 0;
		c->growth = -2000 + (rand() % 500) - 250;
	}
	else if (type == C_LIGHTNING)
	{
		VectorCopy(bubblecolor[lt_blue], c->color);
		c->scale = 80;
		c->die = cl.time + 0.01;
		c->alpha = 0.33;
		c->fade = 0;
		c->growth = 0;
	}
	else if (type == C_SMALLLIGHTNING)
	{
		VectorCopy(bubblecolor[lt_blue], c->color);
		c->scale = 40;
		c->die = cl.time + 0.01;
		c->alpha = 0.33;
		c->fade = 0;
		c->growth = 0;
	}
	else if (type == C_ROCKETLIGHT)
	{
		//dlightColorEx(r_rocketlightcolor.value, r_rocketlightcolor.string, lt_rocket, false, &cst_lt);
		c->alpha = 1;
		/*if (cst_lt.type == lt_custom)
		{
			VectorCopy(cst_lt.color, c->color);
			VectorScale(c->color, (1.0 / 255), c->color); // cast byte to float
			c->alpha = cst_lt.alpha * (1.0 / 255);
		}
		else
			VectorCopy(bubblecolor[cst_lt.type], c->color);*/

		VectorCopy(bubblecolor[lt_rocket], c->color);

		c->scale = 60;
		c->die = cl.time + 0.01;
		c->fade = 0;
		c->growth = 0;
	}
	else if (type == C_GREENLIGHT)
	{
		c->color[0] = 0;
		c->color[1] = 1;
		c->color[2] = 0;
		c->scale = 20;
		c->die = cl.time + 0.01;
		//c->alpha = 0.5;
		c->alpha = (cl.time - (int)(cl.time)) * 0.3;
		c->fade = 0;
		c->growth = 0;
	}
	else if (type == C_REDLIGHT)
	{
		c->color[0] = 1;
		c->color[1] = 0;
		c->color[2] = 0;
		c->scale = 20;
		c->die = cl.time + 0.01;
		//c->alpha = 0.5;
		c->alpha = (cl.time - (int)(cl.time)) * 0.4;
		c->fade = 0;
		c->growth = 0;
	}
	else if (type == C_BLUESPARK)
	{
		//VectorCopy(bubblecolor[lt_blue], c->color);

		//{ 60, 100, 240 };

		c->color[0] = 0.23;
		c->color[1] = 0.42;
		c->color[2] = 0.94;

		c->scale = 5;//10;//30;
		c->die = cl.time + 0.3;//+ 0.75;
		c->alpha = 0.35;//0.5;
		c->fade = -1;
		c->growth = -60;
	}
	else if (type == C_YELLOWSPARK) // For machine gibs yellow sparks, OfN May 2021
	{
		//VectorCopy(bubblecolor[lt_greenred], c->color);

		//col[0] = 210;
		//col[1] = 120;
		//col[2] = 60;
		c->color[0] = 1;
		c->color[1] = 0.5;
		c->color[2] = 0.23;

		c->scale = 10;//30;
		c->die = cl.time + 0.3;//+ 0.75;
		c->alpha = 0.5;
		c->fade = -1;
		c->growth = -60;
	}
	else if (type == C_GUNFLASH)
	{
		vec3_t normal, impact, vec;
		c->color[0] = c->color[1] = c->color[2] = 1;
		c->texture = gunflashtexture;
		c->scale = 50;
		c->die = cl.time + 0.1;
		c->alpha = 0.66;
		c->fade = 0;
		c->growth = -500;
		//A lot of the time the message is being sent just inside of a wall or something
		//I want to move it out of the wall so we can see it
		//Mainly for the hwguy
		//Sigh, if only they knew "omg see gunshots thru wall hax"
		CL_TraceLine(r_refdef.vieworg, origin, impact, normal);
		if (!VectorCompare(origin, impact))
		{
			VectorSubtract(r_refdef.vieworg, origin, vec);
			VectorNormalize(vec);
			VectorMA(origin, 2, vec, c->origin);
		}
	}
	else if (type == C_EXPLODE)
	{
		c->color[0] = c->color[1] = c->color[2] = 1;
		c->scale = 120; // EZQuake is 200
		c->die = cl.time + 0.03;
		c->alpha = 1;
		c->growth = 0;
		c->fade = 0;
		c->texture = explosionflashtexture1;
	}
	else if (type == C_WHITELIGHT)
	{
		c->color[0] = 1;
		c->color[1] = 1;
		c->color[2] = 1;
		c->scale = 40;
		c->die = cl.time + 1;
		c->alpha = 0.5;
		c->fade = -1;
		c->growth = -200;
	}
	else if (type == C_WIZLIGHT)
	{
		c->color[0] = 0;
		c->color[1] = 0.5;
		c->color[2] = 0;
		c->scale = 60;
		c->die = cl.time + 0.01;
		c->alpha = 1;
		c->fade = 0;
		c->growth = 0;
	}
	else if (type == C_KNIGHTLIGHT)
	{
		c->color[0] = 1;
		c->color[1] = 0.3;
		c->color[2] = 0;
		c->scale = 60;
		c->die = cl.time + 0.01;
		c->alpha = 1;
		c->fade = 0;
		c->growth = 0;
	}
	else if (type == C_VORELIGHT)
	{
		c->color[0] = 0.3;
		c->color[1] = 0;
		c->color[2] = 1;
		c->scale = 60;
		c->die = cl.time + 0.01;
		c->alpha = 1;
		c->fade = 0;
		c->growth = 0;
	}
}


void InitCoronas(void)
{
	corona_t* c;
	int		i;

	//VULT STATS
	CoronaCount = 0;
	CoronaCountHigh = 0;
	for (i = 0; i < MAX_CORONAS; i++)
	{
		c = &r_corona[i];
		c->type = C_FREE;
		c->los = false;
		c->sighted = false;
	}
};


//NewStaticLightCorona
//Throws down a permanent light at origin, and wont put another on top of it
//This needs fixing so it wont be called so often
void NewStaticLightCorona(coronatype_t type, vec3_t origin, entity_t* hint)
{
	corona_t* c, * e = NULL;
	int		i;
	qboolean breakage = true;

	c = r_corona;
	for (i = 0; i < MAX_CORONAS; i++, c++)
	{
		if (c->type == C_FREE)
		{
			e = c;
			breakage = false;
		}

		if (hint == c->serialhint)
			return;

		if (VectorCompare(c->origin, origin) && c->type == C_FIRE)
			return;
	}
	if (breakage) //no free coronas
		return;
	memset(e, 0, sizeof(*e));

	e->sighted = false;
	VectorCopy(origin, e->origin);
	e->type = type;
	e->los = false;
	e->texture = coronatexture;
	e->serialhint = hint;

	if (type == C_FIRE)
	{
		e->color[0] = 0.5;
		e->color[1] = 0.2;
		e->color[2] = 0.05;
		e->scale = 0.1;
		e->die = cl.time + 800;
		e->alpha = 0.05;
		e->fade = 0.5;
		e->growth = 800;
	}
}

void CoronaStats(int change)
{
	if (CoronaCount > CoronaCountHigh)
		CoronaCountHigh = CoronaCount;
	CoronaCount += change;
}

float CL_TraceLine(vec3_t start, vec3_t end, vec3_t impact, vec3_t normal)
{
	pmtrace_t trace = PM_TraceLine(start, end); /* PM_TraceLine hits bmodels and players */
	VectorCopy(trace.endpos, impact);
	if (normal)
		VectorCopy(trace.plane.normal, normal);

	return 0.0;
}

void GL_PolygonOffset(float factor, float units)
{
	if (factor || units)
	{
		glEnable(GL_POLYGON_OFFSET_FILL);
		glEnable(GL_POLYGON_OFFSET_LINE);

		glPolygonOffset(factor, units);
	}
	else
	{
		glDisable(GL_POLYGON_OFFSET_FILL);
		glDisable(GL_POLYGON_OFFSET_LINE);
	}
}

//=========================================================================================================================================
//=========================================================================================================================================
// WATER SHADER SUPPORT

#ifndef _WIN32
extern void* glXGetProcAddress(const GLubyte* procName);
#endif

void RetrieveGlextShaderFuncs(void)
{
#ifdef _WIN32
	glCreateShader = (void*)wglGetProcAddress("glCreateShader");
	glShaderSource = (void*)wglGetProcAddress("glShaderSource");
	glCompileShader = (void*)wglGetProcAddress("glCompileShader");
	glCreateProgram = (void*)wglGetProcAddress("glCreateProgram");
	glAttachShader = (void*)wglGetProcAddress("glAttachShader");
	glDeleteShader = (void*)wglGetProcAddress("glDeleteShader");
	glLinkProgram = (void*)wglGetProcAddress("glLinkProgram");
	glUseProgram = (void*)wglGetProcAddress("glUseProgram");
	glDeleteProgram = (void*)wglGetProcAddress("glDeleteProgram");

	glGetShaderiv = (void*)wglGetProcAddress("glGetShaderiv");
	glGetProgramiv = (void*)wglGetProcAddress("glGetProgramiv");
	glGetShaderInfoLog = (void*)wglGetProcAddress("glGetShaderInfoLog");
	glGetProgramInfoLog = (void*)wglGetProcAddress("glGetProgramInfoLog");

	glUniform1i = (void*)wglGetProcAddress("glUniform1i");
	glUniform4fv = (void*)wglGetProcAddress("glUniform4fv");
	glUniformMatrix2fv = (void*)wglGetProcAddress("glUniformMatrix2fv");
	glUniformMatrix4fv = (void*)wglGetProcAddress("glUniformMatrix4fv");
	glUniform3f = (void*)wglGetProcAddress("glUniform3f");
	glUniform1f = (void*)wglGetProcAddress("glUniform1f");
	glGetUniformLocation = (void*)wglGetProcAddress("glGetUniformLocation");

	glGenFramebuffers = (void*)wglGetProcAddress("glGenFramebuffers");
	glBindFramebuffer = (void*)wglGetProcAddress("glBindFramebuffer");
	glFramebufferTexture2D = (void*)wglGetProcAddress("glFramebufferTexture2D");
	
	glGenRenderbuffers = (void*)wglGetProcAddress("glGenRenderbuffers");
	glBindRenderbuffer = (void*)wglGetProcAddress("glBindRenderbuffer");
	glRenderbufferStorage = (void*)wglGetProcAddress("glRenderbufferStorage");
	glFramebufferRenderbuffer = (void*)wglGetProcAddress("glFramebufferRenderbuffer");

	glDeleteFramebuffers = (void*)wglGetProcAddress("glDeleteFramebuffers");
	glDeleteRenderbuffers = (void*)wglGetProcAddress("glDeleteRenderbuffers");

	glCheckFramebufferStatus = (void*)wglGetProcAddress("glCheckFramebufferStatus");

#else
	glCreateShader = (void*)glXGetProcAddress("glCreateShader");
	glShaderSource = (void*)glXGetProcAddress("glShaderSource");
	glCompileShader = (void*)glXGetProcAddress("glCompileShader");
	glCreateProgram = (void*)glXGetProcAddress("glCreateProgram");
	glAttachShader = (void*)glXGetProcAddress("glAttachShader");
	glDeleteShader = (void*)glXGetProcAddress("glDeleteShader");
	glLinkProgram = (void*)glXGetProcAddress("glLinkProgram");
	glUseProgram = (void*)glXGetProcAddress("glUseProgram");
	glDeleteProgram = (void*)glXGetProcAddress("glDeleteProgram");

	glGetShaderiv = (void*)glXGetProcAddress("glGetShaderiv");
	glGetProgramiv = (void*)glXGetProcAddress("glGetProgramiv");
	glGetShaderInfoLog = (void*)glXGetProcAddress("glGetShaderInfoLog");
	glGetProgramInfoLog = (void*)glXGetProcAddress("glGetProgramInfoLog");

	glUniform1i = (void*)glXGetProcAddress("glUniform1i");
	glUniform4fv = (void*)glXGetProcAddress("glUniform4fv");
	glUniformMatrix2fv = (void*)glXGetProcAddress("glUniformMatrix2fv");
	glUniformMatrix4fv = (void*)glXGetProcAddress("glUniformMatrix4fv");
	glUniform3f = (void*)glXGetProcAddress("glUniform3f");
	glUniform1f = (void*)glXGetProcAddress("glUniform1f");
	glGetUniformLocation = (void*)glXGetProcAddress("glGetUniformLocation");

	glGenFramebuffers = (void*)glXGetProcAddress("glGenFramebuffers");
	glBindFramebuffer = (void*)glXGetProcAddress("glBindFramebuffer");
	glFramebufferTexture2D = (void*)glXGetProcAddress("glFramebufferTexture2D");

	glGenRenderbuffers = (void*)glXGetProcAddress("glGenRenderbuffers");
	glBindRenderbuffer = (void*)glXGetProcAddress("glBindRenderbuffer");
	glRenderbufferStorage = (void*)glXGetProcAddress("glRenderbufferStorage");
	glFramebufferRenderbuffer = (void*)glXGetProcAddress("glFramebufferRenderbuffer");

	glDeleteFramebuffers = (void*)glXGetProcAddress("glDeleteFramebuffers");
	glDeleteRenderbuffers = (void*)glXGetProcAddress("glDeleteRenderbuffers");

	glCheckFramebufferStatus = (void*)glXGetProcAddress("glCheckFramebufferStatus");

#endif


#if 1
	if (glCreateShader &&
		glShaderSource &&
		glCompileShader &&
		glCreateProgram &&
		glAttachShader &&
		glDeleteShader &&
		glLinkProgram &&
		glUseProgram &&
		glDeleteProgram &&

		glGetShaderiv &&
		glGetProgramiv &&
		glGetShaderInfoLog &&
		glGetProgramInfoLog &&

		glUniform1i &&
		glUniform4fv &&
		glUniformMatrix2fv &&
		glUniform3f &&
		glUniform1f &&
		glUniformMatrix4fv &&

		glGetUniformLocation &&
		
		glGenFramebuffers &&
		glBindFramebuffer &&
		glFramebufferTexture2D &&
		
		glGenRenderbuffers &&
		glBindRenderbuffer &&
		glRenderbufferStorage &&
		glFramebufferRenderbuffer &&
		
		glDeleteFramebuffers &&
		glDeleteRenderbuffers)
	{
		Com_Printf("Shader &c8f8extensions&r retrieved\n");
		ShaderExtPresent = true;
	}
	else
	{
		Com_Printf("Shader &cF88extensions&r not found\n");
		ShaderExtPresent = false;
	}
#else
	Com_Printf("Shader &c8f8extensions&r retrieved\n");
	ShaderExtPresent = true;
#endif
}

#ifdef _WIN32
extern __forceinline void ShaderBegin(void)
#else
extern void ShaderBegin(void)
#endif
{
	glUseProgram(ShaderProgs);
}

#ifdef _WIN32
extern __forceinline void ShaderEnd(void)
#else
extern void ShaderEnd(void)
#endif
{
	glUseProgram(0);
}

extern cvar_t shader_ref_balance;
extern cvar_t shader_tex_balance;
extern cvar_t shader_wavesize;
extern cvar_t shader_wavestrength;
extern cvar_t shader_waverate;
extern cvar_t shader_color;
extern cvar_t shader_col_balance;
extern cvar_t shader_fresnel;
extern cvar_t shader_rip_color;
extern cvar_t shader_rip_balance;
extern cvar_t shader_light_x;
extern cvar_t shader_light_y;
extern cvar_t shader_light_z;
extern cvar_t shader_rip_shine;
extern cvar_t shader_rip_reflect;
extern cvar_t shader_displacement;
extern cvar_t shader_rip_distortion;
extern cvar_t shader_rip_source;

int HexToInt(char c);

void ShaderSetup(msurface_t* fa)
{
	GL_EnableMultitexture();
					
	GL_EnableTMU(GL_TEXTURE0_ARB);
	GL_Bind(refractionTexture);
	glUniform1i(glGetUniformLocation(ShaderProgs, "refractexture"), 0);
		
	GL_EnableTMU(GL_TEXTURE1_ARB);
	GL_Bind(reflectionTexture);
	glUniform1i(glGetUniformLocation(ShaderProgs, "reflectexture"), 1);
		
	GL_EnableTMU(GL_TEXTURE2_ARB);
	GL_Bind(fa->texinfo->texture->gl_texturenum); //ignore vscode error, it's buggy code analyze
	glUniform1i(glGetUniformLocation(ShaderProgs, "watertexture"), 2);
		
	glUniform1f(glGetUniformLocation(ShaderProgs, "time"),(float)cl.time);

	glUniform1i(glGetUniformLocation(ShaderProgs, "swidth"), glwidth);
	glUniform1i(glGetUniformLocation(ShaderProgs, "sheight"), glheight);

	GL_EnableTMU(GL_TEXTURE3_ARB);
	GL_Bind(dudvMapTexture);
	glUniform1i(glGetUniformLocation(ShaderProgs, "dudvMap"), 3);

	glUniform1f(glGetUniformLocation(ShaderProgs, "refbalance"), shader_ref_balance.value);
	glUniform1f(glGetUniformLocation(ShaderProgs, "texbalance"), shader_tex_balance.value);

	glUniform1f(glGetUniformLocation(ShaderProgs, "wavesize"), shader_wavesize.value);
	glUniform1f(glGetUniformLocation(ShaderProgs, "wavestrength"), shader_wavestrength.value);
	glUniform1f(glGetUniformLocation(ShaderProgs, "waverate"), shader_waverate.value);

	float r, g, b;
	
	if (strlen(shader_color.string) == 3)
	{
		r = HexToInt(shader_color.string[0]) / 15.0f;
		g = HexToInt(shader_color.string[1]) / 15.0f;
		b = HexToInt(shader_color.string[2]) / 15.0f;
	}
	else
		r = g = b = 0;
		
	glUniform3f(glGetUniformLocation(ShaderProgs, "color"), r, g, b);
	glUniform1f(glGetUniformLocation(ShaderProgs, "colbal"), shader_col_balance.value);

	glUniform3f(glGetUniformLocation(ShaderProgs, "origin"), r_refdef.vieworg[0], r_refdef.vieworg[1], r_refdef.vieworg[2]);

	glUniform1f(glGetUniformLocation(ShaderProgs, "fresnel"), shader_fresnel.value);

	qglActiveTexture(GL_TEXTURE4_ARB);
	GL_Bind(normalMapTexture);
	glUniform1i(glGetUniformLocation(ShaderProgs, "normalMap"), 4);

	if (strlen(shader_rip_color.string) == 3)
	{
		r = HexToInt(shader_rip_color.string[0]) / 15.0f;
		g = HexToInt(shader_rip_color.string[1]) / 15.0f;
		b = HexToInt(shader_rip_color.string[2]) / 15.0f;
	}
	else
		r = g = b = 1;
	glUniform3f(glGetUniformLocation(ShaderProgs, "lightcol"), r, g, b);
	glUniform1f(glGetUniformLocation(ShaderProgs, "ripbal"), shader_rip_balance.value);
	glUniform3f(glGetUniformLocation(ShaderProgs, "lightpos"), shader_light_x.value, shader_light_y.value, shader_light_z.value);

	glUniform1f(glGetUniformLocation(ShaderProgs, "shineDamper"), shader_rip_shine.value);
	glUniform1f(glGetUniformLocation(ShaderProgs, "reflectivity"), shader_rip_reflect.value);

	glUniform1f(glGetUniformLocation(ShaderProgs, "displacement"), shader_displacement.value);
	glUniform1f(glGetUniformLocation(ShaderProgs, "ripdist"), shader_rip_distortion.value);
	glUniform1f(glGetUniformLocation(ShaderProgs, "lsource"), shader_rip_source.value);

	float modelMat[16];
	
	glGetFloatv(GL_MODELVIEW_MATRIX, modelMat);

	glUniformMatrix4fv(glGetUniformLocation(ShaderProgs, "modelMat"), 1, GL_FALSE, modelMat);

	int farclip = max((int)r_farclip.value, 4096);
	glUniform1f(glGetUniformLocation(ShaderProgs, "far"), (float)farclip);

	qglActiveTexture(GL_TEXTURE5_ARB);
	GL_Bind(refractionDepthTexture);
	glUniform1i(glGetUniformLocation(ShaderProgs, "depthMap"), 5);
}

void CompileShaders(void)
{
	if (!ShaderExtPresent)
		return;

	const char* vertshader =
		"#version 110\n"
		"uniform mat4 modelMat;\n"
		"uniform vec3 origin;\n"
		"uniform vec3 lightpos;\n"
		"varying vec2 tc;\n"
		"varying vec3 vertexpos;\n"
		"varying vec3 fromLightVector;\n"
		"varying vec3 toCameraVector;\n"
		"void main()\n"
		"{\n"
			"tc = gl_MultiTexCoord2.xy * (1.0 / 64.0);\n"
		
			"vertexpos = gl_Vertex.xyz;\n"

			"vec4 worldPosition = modelMat * gl_Vertex;\n"
			"fromLightVector = worldPosition.xyz - lightpos;\n"
			"toCameraVector = origin - worldPosition.xyz;\n"

			"gl_Position = ftransform();\n"
		"}\n";

	const char* fragshader =
		"#version 110\n"
		"uniform sampler2D refractexture;\n"
		"uniform sampler2D reflectexture;\n"
		"uniform sampler2D watertexture;\n"
		"uniform sampler2D dudvMap;\n"
		"uniform sampler2D normalMap;\n"
		"uniform sampler2D depthMap;\n"
		"uniform float time;\n"
		"uniform float refbalance;\n"
		"uniform float texbalance;\n"
		"uniform float wavesize;\n"
		"uniform float wavestrength;\n"
		"uniform float waverate;\n"
		"uniform int swidth;\n"
		"uniform int sheight;\n"
		"uniform vec3 color;\n" 
		"uniform float colbal;\n"
		"uniform vec3 origin;\n"
		"uniform float fresnel;\n"
		"uniform float far;\n"
		"uniform vec3 lightcol;\n"
		"uniform float ripbal;\n"
		"uniform float shineDamper;\n"
		"uniform float reflectivity;\n"
		"uniform float displacement;\n"
		"uniform float ripdist;\n"
		"uniform float lsource;\n"
		"varying vec2 tc;\n"
		"varying vec3 vertexpos;\n"
		"varying vec3 fromLightVector;\n"
		"varying vec3 toCameraVector;\n"
		"const float near = 4.0;\n"
		"void main()\n"
		"{\n"
		
			"float unitTime;\n"
			"if (waverate == 0.0)\n"
				"unitTime = 0.0;\n"
			"else\n"
				"unitTime = (time / waverate) - floor(time / waverate);\n"

			"float driftFactor;\n"
			
			"if (unitTime < 0.5)\n"
				"driftFactor = 1.0 - ((1.0 - unitTime) * 2.0);\n"
			"else\n"
				"driftFactor = unitTime * 2.0 - 1.0;\n"

			// fresnel
			"vec3 eyetosurface = normalize(origin - vertexpos);\n"
			"vec3 fresnelvec = mat3(vec3(1.0, 0.0, 0.0), vec3(0.0, 1.0, 0.0), vec3(0.0, 0.0, 1.0)) * eyetosurface;\n"
			"float fresnel_fac = dot(fresnelvec, vec3(0.0, 0.0, 1.0));\n"

			"fresnel_fac = 1.0 - fresnel_fac;\n"

			"if (fresnel == 0.0)\n"
				"fresnel_fac = refbalance;\n"
			"else\n"
				"fresnel_fac = fresnel_fac * fresnel;\n"

			"fresnel_fac = clamp(fresnel_fac, 0.0, refbalance);\n"
			// end fresnel
		
			//"vec2 distortion = (texture2D(dudvMap, vec2(tc.x * wavesize + driftFactor, tc.y * wavesize).xy * 2.0 - 1.0)).rg * wavestrength;\n"
			//"vec2 distortion = ((texture2D(dudvMap, vec2(tc.x * wavesize + driftFactor, tc.y * wavesize).xy * 2.0 - 1.0)).rg * 2.0 - 1.0) * wavestrength;\n"
			//"vec2 distortion = (texture2D(dudvMap, vec2(tc.x * wavesize + driftFactor, tc.y * wavesize).xy).rg * 2.0 - 1.0) * wavestrength;\n"
			//"vec2 distortion = texture2D(dudvMap, vec2(tc.x * wavesize + driftFactor, tc.y * wavesize).xy).rg * wavestrength;\n"
			//"vec2 distortion = (texture2D(dudvMap, vec2(tc.x * wavesize + driftFactor, tc.y * wavesize).xy).rg * wavestrength) * 2.0 - 1.0;\n" //renders bad!

			//"vec2 distortion = (texture2D(dudvMap, vec2(tc.x * wavesize + driftFactor, tc.y * wavesize).xy).rg - 0.5) * wavestrength;\n" // Moved below...

			"vec2 ntc = tc + 0.1 * vec2(sin(tc.t + time), cos(tc.s + time));\n"

			"vec2 absCoords = gl_FragCoord.xy / vec2(swidth, sheight);\n"

			// depth
			"float depth = texture2D(depthMap, absCoords).r;\n"
			"float floordistance = 2.0 * near * far / (far + near - (2.0 * depth - 1.0) * (far - near));\n"

			"depth = gl_FragCoord.z;\n"
			"float waterdistance = 2.0 * near * far / (far + near - (2.0 * depth - 1.0) * (far - near));\n"
			"float waterdepth = floordistance - waterdistance;\n"
			// end depth

			"vec2 distortion = (texture2D(dudvMap, vec2(tc.x * wavesize + driftFactor, tc.y * wavesize).xy).rg - 0.5) * wavestrength * clamp(waterdepth / 20.0, 0.0, 1.0);\n" // Moved from above

			"absCoords += distortion;\n"//clamp(distortion, 0.001, 0.999);\n"

			"absCoords = clamp(absCoords, 0.001, 0.999);\n"
			
			"vec4 reflectcolor = texture2D(reflectexture, absCoords);\n"
			"vec4 refractcolor = texture2D(refractexture, absCoords);\n"

			"vec4 watercolor = texture2D(watertexture, ntc);\n"

			"vec4 color4 = vec4(color.r, color.g, color.b, 1.0);\n"

			// Normal
			"vec3 specularHighlights;\n"
			"if (ripbal == 0.0)\n"
				"specularHighlights = vec3(0.0,0.0,0.0);\n"
			"else\n"
			"{\n"
		
				//"vec4 normalMapColor = texture2D(normalMap, vec2(tc.x + unitTime * displacement, tc.y + unitTime * displacement) + distortion * ripdist);\n"
				"vec4 normalMapColor = texture2D(normalMap, vec2(tc.x * wavesize + unitTime * displacement, tc.y * wavesize + unitTime * displacement) + distortion * ripdist);\n"
				//"vec3 normal = vec3(normalMapColor.r * 2.0 - 1.0, normalMapColor.b, normalMapColor.g * 2.0 - 1.0);\n"
				//"vec3 normal = vec3(normalMapColor.r * 2.0 - 1.0, normalMapColor.g * 2.0 - 1.0, normalMapColor.b);\n"
				"vec3 normal = vec3(normalMapColor.r * 2.0 - 1.0, normalMapColor.b * 2.0 - 1.0, normalMapColor.g);\n"
				"normal = normalize(normal);\n"
				
				"vec3 viewVector = normalize(toCameraVector);\n"
				"vec3 reflectedLight = reflect(normalize(fromLightVector),normal);\n"
		
				"float damper;\n"
				"vec3 normLightVector = normalize(fromLightVector);\n"
		
				"if (lsource == 0.0)\n"
				"{\n"
					"damper = shineDamper / 10.0;\n"
				"}\n"
				"else\n"
				"{\n"
					"if (lsource != 1.0)\n"
						"damper = shineDamper / (10.0 * (1.0 - lsource));\n"
					"else\n"
						"damper = shineDamper;\n"
				"}\n"

				//"float specular = max(dot(reflectedLight,viewVector), 0.0);\n" //original was 0.0   1.0 makes full brightness
				"float specular = mix(max(dot(reflectedLight,normLightVector), 0.0),max(dot(reflectedLight,viewVector), 0.0), lsource);\n"
				"specular = pow(specular,damper);\n"
				"specularHighlights = lightcol.xyz * specular * reflectivity;\n"
		
			"}\n"
			// end normal

			"gl_FragColor = mix(mix(mix(refractcolor, reflectcolor, fresnel_fac), watercolor, texbalance), color4, colbal) + vec4(specularHighlights, 0.0) * ripbal;\n"// Normal
			"gl_FragColor.a = clamp(waterdepth / 5.0, 0.0, 1.0);\n"
		
		"}\n";

	int vert = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vert, 1, &vertshader, NULL);
	glCompileShader(vert);
	if (CheckShaderError(vert))
	{
		ShaderExtPresent = false;
		glDeleteShader(vert);
		goto error;
	}
	int frag = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(frag, 1, &fragshader, NULL);
	glCompileShader(frag);
	if (CheckShaderError(frag))
	{
		ShaderExtPresent = false;
		glDeleteShader(vert);
		glDeleteShader(frag);
		goto error;
	}
	ShaderProgs = glCreateProgram();
	glAttachShader(ShaderProgs, vert);
	glDeleteShader(vert); //preemptive cleanup is fine
	glAttachShader(ShaderProgs, frag);
	glDeleteShader(frag);
	glLinkProgram(ShaderProgs);

	if (CheckProgramError(ShaderProgs))
	{
		ShaderExtPresent = false;
		glDeleteProgram(ShaderProgs);
		goto error;
	}

	// November 2023 - Compile 'Outlines' shaders
	if (!CompileOutlinesShaders())
		goto error;

	Com_Printf("Shaders &c8f8compiled&r successfully\n");
	return;

error:
	Com_Printf("Shaders &cf88failed&r compilation\n");
}

qboolean CheckShaderError(GLuint shader)
{
	GLint loglen;
	glGetShaderiv(shader, GL_OBJECT_INFO_LOG_LENGTH_ARB, &loglen);
	if (loglen)
	{
		char str[8192];
		glGetShaderInfoLog(shader, sizeof(str), NULL, str);
		Com_Printf("Shader error: %s\n", str);
		return true;
	}

	return false;
}

qboolean CheckProgramError(GLuint program)
{
	GLint linked;
	glGetProgramiv(program, GL_OBJECT_LINK_STATUS_ARB, &linked);
	if (!linked)
	{
		char str[8192];
		glGetProgramInfoLog(program, sizeof(str), NULL, str);
		Com_Printf("GL driver error: %s\n", str);
		return true;
	}

	return false;
}

int createFrameBuffer(void)
{
	int frameBuffer;
	glGenFramebuffers(1,&frameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
	
	return frameBuffer;
}

int createTextureAttachment(int width, int height, char* textid)
{
	int texture;
	texture = GL_LoadTextureImage("textures/emptytex", textid, 0, 0, TEX_COMPLAIN);
	GL_Bind(texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		
	return texture;
}

int createDepthTextureAttachment(int width, int height, char* textid)
{
	int texture;
	texture = GL_LoadTextureImage("textures/emptytex", textid, 0, 0, TEX_COMPLAIN);
	GL_Bind(texture);	
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, NULL);
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	
	return texture;
}

int createDepthBufferAttachment(int width, int height)
{
	int depthbuffer;
	
	glGenRenderbuffers(1, &depthbuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, depthbuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
	glFramebufferRenderbuffer(GL_RENDERBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthbuffer);

	return depthbuffer;	
}

#ifdef _WIN32
extern __forceinline void bindFrameBuffer(int frameBuffer)
#else
extern void bindFrameBuffer(int frameBuffer)
#endif
{
	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
}

#ifdef _WIN32
extern __forceinline void unbindCurrentFrameBuffer(void)
#else
extern void unbindCurrentFrameBuffer(void)
#endif
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

char* TestFBO(GLuint tfbo)
{
	int errorkind = glCheckFramebufferStatus(tfbo);

	switch (errorkind)
	{
	case GL_FRAMEBUFFER_COMPLETE:
		return "Buffer complete";
		break;
	case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
		return "GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT";
		break;
	case GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS_EXT:
		return "GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS";
		break;
	case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
		return "GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT";
		break;

	case GL_FRAMEBUFFER_UNSUPPORTED:
		return "GL_FRAMEBUFFER_UNSUPPORTED";
		break;

	case 0:
		return "ZERO";
		break;

	default:
			return "Error";
			break;
	}

	return "";
}

#ifdef _WIN32
extern RECT WindowRect;
#endif

void CreateFBOs(void)
{
	if (!ShaderExtPresent)
		return;
	
#ifdef _WIN32
	int height = WindowRect.bottom - WindowRect.top;
	int width = WindowRect.right - WindowRect.left;
#else

	//extern vrect_t	scr_vrect;

	extern int scr_width;
	extern int scr_height;

	int height = scr_height;//scr_vrect.height;
	int width = scr_width;// scr_vrect.width;//vid.width;
#endif
		
	refractionTexture = createTextureAttachment(width, height, "FBO_RefractTexture");
	refractionDepthTexture = createDepthTextureAttachment(width, height, "FBO_RefractDepth");
	
	refractionFrameBuffer = createFrameBuffer();
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, refractionTexture, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, refractionDepthTexture, 0);
	//Com_Printf("Refraction buffer: %s\n", TestFBO(refractionFrameBuffer));
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	unbindCurrentFrameBuffer();

		
	reflectionTexture = createTextureAttachment(width, height, "FBO_ReflectTexture");
	reflectionDepthTexture = createDepthTextureAttachment(width, height, "FBO_ReflectDepth");

	reflectionFrameBuffer = createFrameBuffer();
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, reflectionTexture, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, reflectionDepthTexture, 0);
	//Com_Printf("Reflection buffer: %s\n", TestFBO(reflectionFrameBuffer));
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	unbindCurrentFrameBuffer();	

	//============================//
	outlinesFlatNormalsTexture = createTextureAttachment(width, height, "FBO_OutlinesTexture");
	outlinesDepthTexture = createDepthTextureAttachment(width, height, "FBO_OutlinesDepth");

	outlinesFrameBuffer = createFrameBuffer();
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, outlinesFlatNormalsTexture, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, outlinesDepthTexture, 0);
	//Com_Printf("outlines buffer: %s\n", TestFBO(outlinesFrameBuffer));
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	unbindCurrentFrameBuffer();
}

// From FTE source code, thanks Spoike!

//be aware that this generates two sorts of matricies depending on order of a+b
void Matrix4_Multiply(const float* a, const float* b, float* out)
{
	out[0] = a[0] * b[0] + a[4] * b[1] + a[8] * b[2] + a[12] * b[3];
	out[1] = a[1] * b[0] + a[5] * b[1] + a[9] * b[2] + a[13] * b[3];
	out[2] = a[2] * b[0] + a[6] * b[1] + a[10] * b[2] + a[14] * b[3];
	out[3] = a[3] * b[0] + a[7] * b[1] + a[11] * b[2] + a[15] * b[3];

	out[4] = a[0] * b[4] + a[4] * b[5] + a[8] * b[6] + a[12] * b[7];
	out[5] = a[1] * b[4] + a[5] * b[5] + a[9] * b[6] + a[13] * b[7];
	out[6] = a[2] * b[4] + a[6] * b[5] + a[10] * b[6] + a[14] * b[7];
	out[7] = a[3] * b[4] + a[7] * b[5] + a[11] * b[6] + a[15] * b[7];

	out[8] = a[0] * b[8] + a[4] * b[9] + a[8] * b[10] + a[12] * b[11];
	out[9] = a[1] * b[8] + a[5] * b[9] + a[9] * b[10] + a[13] * b[11];
	out[10] = a[2] * b[8] + a[6] * b[9] + a[10] * b[10] + a[14] * b[11];
	out[11] = a[3] * b[8] + a[7] * b[9] + a[11] * b[10] + a[15] * b[11];

	out[12] = a[0] * b[12] + a[4] * b[13] + a[8] * b[14] + a[12] * b[15];
	out[13] = a[1] * b[12] + a[5] * b[13] + a[9] * b[14] + a[13] * b[15];
	out[14] = a[2] * b[12] + a[6] * b[13] + a[10] * b[14] + a[14] * b[15];
	out[15] = a[3] * b[12] + a[7] * b[13] + a[11] * b[14] + a[15] * b[15];
}

/*typedef struct
{
	vec3_t	normal;
	float	dist;
} plane_t;*/
void StoreOriginalOrgAndAngles(void)
{
	memcpy(&OriginalRef, &r_refdef, sizeof(r_refdef));
	VectorCopy(vpn, OriginalVpn);
	VectorCopy(vup, OriginalUp);
	VectorCopy(vright, OriginalRight);
}

void RestoreOriginalOrgAndAngles(void)
{
	memcpy(&r_refdef, &OriginalRef, sizeof(r_refdef));
	VectorCopy(OriginalVpn, vpn);
	VectorCopy(OriginalUp, vup);
	VectorCopy(OriginalRight, vright);	
}

/*generates a new modelview matrix, as well as vpn vectors*/
void R_MirrorMatrix(plane_t* plane)
{
	float mirror[16];
	float view[16];
	float result[16];

	vec3_t pnorm;
	VectorNegate(plane->normal, pnorm);

	mirror[0] = 1 - 2 * pnorm[0] * pnorm[0];
	mirror[1] = -2 * pnorm[0] * pnorm[1];
	mirror[2] = -2 * pnorm[0] * pnorm[2];
	mirror[3] = 0;

	mirror[4] = -2 * pnorm[1] * pnorm[0];
	mirror[5] = 1 - 2 * pnorm[1] * pnorm[1];
	mirror[6] = -2 * pnorm[1] * pnorm[2];
	mirror[7] = 0;

	mirror[8] = -2 * pnorm[2] * pnorm[0];
	mirror[9] = -2 * pnorm[2] * pnorm[1];
	mirror[10] = 1 - 2 * pnorm[2] * pnorm[2];
	mirror[11] = 0;

	mirror[12] = -2 * pnorm[0] * plane->dist;
	mirror[13] = -2 * pnorm[1] * plane->dist;
	mirror[14] = -2 * pnorm[2] * plane->dist;
	mirror[15] = 1;

	view[0] = vpn[0];
	view[1] = vpn[1];
	view[2] = vpn[2];
	view[3] = 0;

	view[4] = -vright[0];
	view[5] = -vright[1];
	view[6] = -vright[2];
	view[7] = 0;

	view[8] = vup[0];
	view[9] = vup[1];
	view[10] = vup[2];
	view[11] = 0;

	view[12] = r_refdef.vieworg[0];
	view[13] = r_refdef.vieworg[1];
	view[14] = r_refdef.vieworg[2];
	view[15] = 1;

	//VectorMA(r_refdef.vieworg, 0.25, plane->normal, r_refdef.pvsorigin);

	Matrix4_Multiply(mirror, view, result);

	vpn[0] = result[0];
	vpn[1] = result[1];
	vpn[2] = result[2];

	vright[0] = -result[4];
	vright[1] = -result[5];
	vright[2] = -result[6];

	vup[0] = result[8];
	vup[1] = result[9];
	vup[2] = result[10];

	r_refdef.vieworg[0] = result[12];
	r_refdef.vieworg[1] = result[13];
	r_refdef.vieworg[2] = result[14];
}

static float sgn(float a)
{
	if (a > 0.0F) return (1.0F);
	if (a < 0.0F) return (-1.0F);
	return (0.0F);
}

//ignore the entire right+bottom row/column of the 4*4 matrix
void Matrix4x4_CM_Transform3x3(const float* matrix, const float* vector, float* product)
{
	product[0] = matrix[0] * vector[0] + matrix[4] * vector[1] + matrix[8] * vector[2];
	product[1] = matrix[1] * vector[0] + matrix[5] * vector[1] + matrix[9] * vector[2];
	product[2] = matrix[2] * vector[0] + matrix[6] * vector[1] + matrix[10] * vector[2];
}

//disregard the extra bit of the matrix
void Matrix4x4_CM_Transform3(const float* matrix, const float* vector, float* product)
{
	product[0] = matrix[0] * vector[0] + matrix[4] * vector[1] + matrix[8] * vector[2] + matrix[12];
	product[1] = matrix[1] * vector[0] + matrix[5] * vector[1] + matrix[9] * vector[2] + matrix[13];
	product[2] = matrix[2] * vector[0] + matrix[6] * vector[1] + matrix[10] * vector[2] + matrix[14];
}

#define DotProduct4(x,y) ((x)[0]*(y)[0]+(x)[1]*(y)[1]+(x)[2]*(y)[2]+(x)[3]*(y)[3])
#define Vector4Scale(in,scale,out)		((out)[0]=(in)[0]*scale,(out)[1]=(in)[1]*scale,(out)[2]=(in)[2]*scale,(out)[3]=(in)[3]*scale)

void R_ObliqueNearClip(float* viewmat, mplane_t* wplane)
{
	float f;
	vec4_t q, c;
	vec3_t ping, pong;
	vec4_t vplane;

	//convert world plane into view space
	Matrix4x4_CM_Transform3x3(viewmat, wplane->normal, vplane);
	VectorScale(wplane->normal, wplane->dist, ping);
	Matrix4x4_CM_Transform3(viewmat, ping, pong);
	vplane[3] = -DotProduct(pong, vplane);

	// Calculate the clip-space corner point opposite the clipping plane
	// as (sgn(clipPlane.x), sgn(clipPlane.y), 1, 1) and
	// transform it into camera space by multiplying it
	// by the inverse of the projection matrix

	q[0] = (sgn(vplane[0]) + r_refdef.m_projection_std[8]) / r_refdef.m_projection_std[0];
	q[1] = (sgn(vplane[1]) + fabs(r_refdef.m_projection_std[9])) / fabs(r_refdef.m_projection_std[5]);
	q[2] = -1.0F;
	q[3] = (1.0F + r_refdef.m_projection_std[10]) / r_refdef.m_projection_std[14];

	// Calculate the scaled plane vector
	f = 2.0F / DotProduct4(vplane, q);
	Vector4Scale(vplane, f, c);

	// Replace the third row of the projection matrix
	r_refdef.m_projection_std[2] = c[0];
	r_refdef.m_projection_std[6] = c[1];
	r_refdef.m_projection_std[10] = c[2] + 1.0F;
	r_refdef.m_projection_std[14] = c[3];
}


void Matrix4x4_CM_Projection_Inf(float* proj, float fovx, float fovy, float neard)//, qboolean d3d)
{
	float xmin, xmax, ymin, ymax;
	//double dn = (d3d ? 0 : -1), df = 1;
	double dn = -1, df = 1;

	//proj
	ymax = neard * tan(fovy * M_PI / 360.0);
	ymin = -ymax;

	if (fovx == fovy)
	{
		xmax = ymax;
		xmin = ymin;
	}
	else
	{
		xmax = neard * tan(fovx * M_PI / 360.0);
		xmin = -xmax;
	}

	proj[0] = (2 * neard) / (xmax - xmin);
	proj[4] = 0;
	proj[8] = (xmax + xmin) / (xmax - xmin);
	proj[12] = 0;

	proj[1] = 0;
	proj[5] = (2 * neard) / (ymax - ymin);
	proj[9] = (ymax + ymin) / (ymax - ymin);
	proj[13] = 0;

#if 1
	{
		const double epsilon = 1.0 / (1 << 22);
		proj[2] = 0;
		proj[6] = 0;
		proj[10] = epsilon - 1;
		proj[14] = (epsilon - (df - dn)) * neard;
	}
#elif 1
	{	//mathematical target
		const float fard = (1 << 22);
		proj[2] = 0;
		proj[6] = 0;
		proj[10] = (fard * df - neard * dn) / (neard - fard);
		proj[14] = ((df - dn) * fard * neard) / (neard - fard);
	}
#else
	//old logic
	proj[2] = 0;
	proj[6] = 0;
	proj[10] = -1 * ((float)(1 << 21) / (1 << 22));
	proj[14] = -2 * neard;
#endif

	proj[3] = 0;
	proj[7] = 0;
	proj[11] = -1;
	proj[15] = 0;
}

/*inline float sgn(float a)
{
	if (a > 0.0F) return (1.0F);
	if (a < 0.0F) return (-1.0F);
	return (0.0F);
}*/

float dot_product(float* a, float* b, int size)
{
	float dp = 0.0f;
	for (int i = 0; i < size; i++)
		dp += a[i] * b[i];
	return dp;
}

void ModifyProjectionMatrix(Vector4D* clipPlane)
{
	float       matrix[16];
	Vector4D    q;

	// Grab the current projection matrix from OpenGL
	glGetFloatv(GL_PROJECTION_MATRIX, matrix);

	// Calculate the clip-space corner point opposite the clipping plane
	// as (sgn(clipPlane.x), sgn(clipPlane.y), 1, 1) and
	// transform it into camera space by multiplying it
	// by the inverse of the projection matrix

	q.x = (sgn(clipPlane->x) + matrix[8]) / matrix[0];
	q.y = (sgn(clipPlane->y) + matrix[9]) / matrix[5];
	q.z = -1.0F;
	q.w = (1.0F + matrix[10]) / matrix[14];

	// Calculate the scaled plane vector
	//Vector4D c = clipPlane * (2.0F / Dot(clipPlane, q));

	Vector4D c;
	vec4_t clipPlaneC, c_out, q_c;
	clipPlaneC[0] = clipPlane->x;
	clipPlaneC[1] = clipPlane->y;
	clipPlaneC[2] = clipPlane->z;
	clipPlaneC[3] = clipPlane->w;

	q_c[0] = q.x;
	q_c[1] = q.y;
	q_c[2] = q.z;
	q_c[3] = q.w;

	Vector4Scale(clipPlaneC, (2.0F / dot_product(clipPlaneC, q_c,4)),c_out);
	
	c.x = c_out[0];
	c.y = c_out[1];
	c.z = c_out[2];
	c.w = c_out[3];

	// Replace the third row of the projection matrix
	matrix[2] = c.x;
	matrix[6] = c.y;
	matrix[10] = c.z + 1.0F;
	matrix[14] = c.w;

	// Load it back into OpenGL
	glMatrixMode(GL_PROJECTION);
	glLoadMatrixf(matrix);
}

//This function is GL stylie (use as 2nd arg to ML_MultMatrix4).
float* Matrix4x4_CM_NewTranslation(float x, float y, float z)
{
	static float ret[16];
	ret[0] = 1;
	ret[4] = 0;
	ret[8] = 0;
	ret[12] = x;

	ret[1] = 0;
	ret[5] = 1;
	ret[9] = 0;
	ret[13] = y;

	ret[2] = 0;
	ret[6] = 0;
	ret[10] = 1;
	ret[14] = z;

	ret[3] = 0;
	ret[7] = 0;
	ret[11] = 0;
	ret[15] = 1;
	return ret;
}

void Matrix4x4_CM_ModelViewMatrixFromAxis(float* modelview, const vec3_t pn, const vec3_t right, const vec3_t up, const vec3_t vieworg)
{
	float tempmat[16];

	tempmat[0] = right[0];
	tempmat[1] = up[0];
	tempmat[2] = -pn[0];
	tempmat[3] = 0;
	tempmat[4] = right[1];
	tempmat[5] = up[1];
	tempmat[6] = -pn[1];
	tempmat[7] = 0;
	tempmat[8] = right[2];
	tempmat[9] = up[2];
	tempmat[10] = -pn[2];
	tempmat[11] = 0;
	tempmat[12] = 0;
	tempmat[13] = 0;
	tempmat[14] = 0;
	tempmat[15] = 1;

	Matrix4_Multiply(tempmat, Matrix4x4_CM_NewTranslation(-vieworg[0], -vieworg[1], -vieworg[2]), modelview);	    // put Z going up
}

extern float processed_water_height;

qboolean IsUnderWaterShader(vec3_t org)
{
	if (specialrender == SR_REFRACTION)
		return true;
	
	//if (org[2] < processed_water_height && cl.simorg[2] > processed_water_height && PM_PointContents(org) == CONTENTS_WATER) 
	 //   Object is below water height   AND  Object is in water content           AND This is a reflection pass, not refraction (See thru)
	if (/*specialrender == SR_REFLECTION && */ org[2] < processed_water_height && PM_PointContents(org) == CONTENTS_WATER)
		return true;
		
	/*if (specialrender = SR_REFRACTION)
		if (org[2] < processed_water_height && PM_PointContents(org) == CONTENTS_WATER)
		{
			vec3_t realview;
			VectorCopy(cl.simorg, realview);

			realview[2] += -22;

			if (PM_PointContents(realview) == CONTENTS_EMPTY)
				return true;
			else
				return false;
		}*/

	return false; // skip code below this

	//    Object is above water height  AND  Object is in empty content            AND Our view contents is water
	/*if (org[2] > processed_water_height && PM_PointContents(org) == CONTENTS_EMPTY) //&& PM_PointContents(r_refdef.vieworg) == CONTENTS_WATER ) cl.viewent.
	{
		vec3_t realview;
		VectorCopy(cl.simorg, realview);
		
		realview[2] += -22;

		if (PM_PointContents(realview) == CONTENTS_WATER)
			return true;
		else
			return false;
	}
	else
		return false;*/
}
/*
void TangentToWorld(vec3_t v, vec3_t tangent, vec3_t bitangent, vec3_t normal, vec3_t result)
{
	//- const int handness = -1; // Left coordinate system
	// Vworld = Vtangent * TBN
	/*Vector3D v_world = {
	  v.x * tangent.x + v.y * bitangent.x + v.z * normal.x,
	  v.x * tangent.y + v.y * bitangent.y + v.z * normal.y,
	  v.x * tangent.z + v.y * bitangent.z + v.z * normal.z,
	};
	// Vworld = Vtangent * TBN(-1) = V * TBN(T)
	Vector3D v_world2 = {
	  v.x * tangent.x + v.y * tangent.y + v.z * tangent.z,
	  v.x * bitangent.x + v.y * bitangent.y + v.z * bitangent.z,
	  v.x * normal.x + v.y * normal.y + v.z * normal.z,
	};

	v_world2.normalize();
	// return handness * v_world; --> DOES NOT WORK
	return handness * v_world2; -- > WORKS

	result[0] = v[0] * tangent[0] + v[1] * bitangent[0] + v[2] * normal[0];
	result[1] = v[0] * tangent[1] + v[1] * bitangent[1] + v[2] * normal[1];
	result[2] = v[0] * tangent[2] + v[1] * bitangent[2] + v[2] * normal[2];


	/*result[0] = v[0] * tangent[0] + v[1] * tangent[1] + v[2] * tangent[2];
	result[1] = v[0] * bitangent[0] + v[1] * bitangent[1] + v[2] * bitangent[2];
	result[2] = v[0] * normal[0] + v[1] * normal[1] + v[2] * normal[2];

	VectorNormalize(result);
}*/

// BLOOM From EZQuake

static float Diamond8x[8][8] = {
	{0.0f, 0.0f, 0.0f, 0.1f, 0.1f, 0.0f, 0.0f, 0.0f},
	{0.0f, 0.0f, 0.2f, 0.3f, 0.3f, 0.2f, 0.0f, 0.0f},
	{0.0f, 0.2f, 0.4f, 0.6f, 0.6f, 0.4f, 0.2f, 0.0f},
	{0.1f, 0.3f, 0.6f, 0.9f, 0.9f, 0.6f, 0.3f, 0.1f},
	{0.1f, 0.3f, 0.6f, 0.9f, 0.9f, 0.6f, 0.3f, 0.1f},
	{0.0f, 0.2f, 0.4f, 0.6f, 0.6f, 0.4f, 0.2f, 0.0f},
	{0.0f, 0.0f, 0.2f, 0.3f, 0.3f, 0.2f, 0.0f, 0.0f},
	{0.0f, 0.0f, 0.0f, 0.1f, 0.1f, 0.0f, 0.0f, 0.0f} };

static float Diamond6x[6][6] = {
	{0.0f, 0.0f, 0.1f, 0.1f, 0.0f, 0.0f},
	{0.0f, 0.3f, 0.5f, 0.5f, 0.3f, 0.0f},
	{0.1f, 0.5f, 0.9f, 0.9f, 0.5f, 0.1f},
	{0.1f, 0.5f, 0.9f, 0.9f, 0.5f, 0.1f},
	{0.0f, 0.3f, 0.5f, 0.5f, 0.3f, 0.0f},
	{0.0f, 0.0f, 0.1f, 0.1f, 0.0f, 0.0f} };

static float Diamond4x[4][4] = {
	{0.3f, 0.4f, 0.4f, 0.3f},
	{0.4f, 0.9f, 0.9f, 0.4f},
	{0.4f, 0.9f, 0.9f, 0.4f},
	{0.3f, 0.4f, 0.4f, 0.3f} };

static int BLOOM_SIZE;

cvar_t      r_bloom = { "r_bloom", "1", CVAR_ALLOW_RECURSION };
cvar_t      r_bloom_alpha = { "r_bloom_alpha", "0.5", true };
cvar_t      r_bloom_diamond_size = { "r_bloom_diamond_size", "8", true };
cvar_t      r_bloom_intensity = { "r_bloom_intensity", "1", CVAR_ALLOW_RECURSION };//true }; // March 2022 - Reverted to 1 and lowered darken below from 3 to 2 //Default in EZQ is 1, but due to the code below in the main routine, not in ezquake, for some reason 1 is too low
#ifdef _WIN32
cvar_t      r_bloom_darken = { "r_bloom_darken", "2", true }; // default in EZQ was 3
#else
cvar_t      r_bloom_darken = { "r_bloom_darken", "3", true }; // default in EZQ was 3
#endif
cvar_t      r_bloom_sample_size = { "r_bloom_sample_size", "256", true };
cvar_t      r_bloom_fast_sample = { "r_bloom_fast_sample", "0", true };

int r_bloomscreentexture;
int r_bloomeffecttexture;
int r_bloombackuptexture;
int r_bloomdownsamplingtexture;

static int      r_screendownsamplingtexture_size;
static int      screen_texture_width, screen_texture_height;
static int      r_screenbackuptexture_size;

// Current refdef size:
static int  curView_x;
static int  curView_y;
static int  curView_width;
static int  curView_height;

// Texture coordinates of screen data inside screentexture.
static float screenText_tcw;
static float screenText_tch;

static int  sample_width;
static int  sample_height;

// Texture coordinates of adjusted textures.
static float sampleText_tcw;
static float sampleText_tch;

// This macro is in sample size workspace coordinates.
#define R_Bloom_SamplePass( xpos, ypos )                           \
	glBegin(GL_QUADS);                                             \
	glTexCoord2f(  0,                      sampleText_tch);        \
	glVertex2f(    xpos,                   ypos);                  \
	glTexCoord2f(  0,                      0);                     \
	glVertex2f(    xpos,                   ypos+sample_height);    \
	glTexCoord2f(  sampleText_tcw,         0);                     \
	glVertex2f(    xpos+sample_width,      ypos+sample_height);    \
	glTexCoord2f(  sampleText_tcw,         sampleText_tch);        \
	glVertex2f(    xpos+sample_width,      ypos);                  \
	glEnd();

#define R_Bloom_Quad( x, y, width, height, textwidth, textheight ) \
	glBegin(GL_QUADS);                                             \
	glTexCoord2f(  0,          textheight);                        \
	glVertex2f(    x,          y);                                 \
	glTexCoord2f(  0,          0);                                 \
	glVertex2f(    x,          y+height);                          \
	glTexCoord2f(  textwidth,  0);                                 \
	glVertex2f(    x+width,    y+height);                          \
	glTexCoord2f(  textwidth,  textheight);                        \
	glVertex2f(    x+width,    y);                                 \
	glEnd();

//=================
// R_Bloom_InitBackUpTexture
// =================
void R_Bloom_InitBackUpTexture(int width, int height)
{
	unsigned char* data;

	data = (unsigned char*)calloc(width * height, sizeof(int)); //was Q_calloc in ezq

	r_screenbackuptexture_size = width;

	r_bloombackuptexture = GL_LoadTexture("***r_bloombackuptexture***", width, height, data, 0, 4);

	free(data);//Q_free(data);
}

// =================
// R_Bloom_InitEffectTexture
// =================
void R_Bloom_InitEffectTexture(void)
{
	unsigned char* data;
	float bloomsizecheck;

	if (r_bloom_sample_size.value < 32)
		Cvar_SetValue(&r_bloom_sample_size, 32);

	// Make sure bloom size is a power of 2.
	BLOOM_SIZE = r_bloom_sample_size.value;
	bloomsizecheck = (float)BLOOM_SIZE;

	while (bloomsizecheck > 1.0f)
		bloomsizecheck /= 2.0f;

	if (bloomsizecheck != 1.0f)
	{
		BLOOM_SIZE = 32;

		while (BLOOM_SIZE < r_bloom_sample_size.value)
			BLOOM_SIZE *= 2;
	}

	// Make sure bloom size doesn't have stupid values.
	if (BLOOM_SIZE > screen_texture_width || BLOOM_SIZE > screen_texture_height)
		BLOOM_SIZE = min(screen_texture_width, screen_texture_height);

	if (BLOOM_SIZE != r_bloom_sample_size.value)
		Cvar_SetValue(&r_bloom_sample_size, BLOOM_SIZE);

	data = (unsigned char*)calloc(BLOOM_SIZE * BLOOM_SIZE, sizeof(int)); // was Q_calloc in ez

	r_bloomeffecttexture = GL_LoadTexture("***r_bloomeffecttexture***", BLOOM_SIZE, BLOOM_SIZE, data, 0, 4);

	free(data);//Q_free(data);
}

// =================
// R_Bloom_InitTextures
// =================
extern int texture_extension_number;
void R_Bloom_InitTextures(void)
{
	unsigned char* data;
	int maxtexsize, glinternalfmt;
	size_t size;

	// Find closer power of 2 to screen size.
	for (screen_texture_width = 1; screen_texture_width < glwidth; screen_texture_width *= 2);
	for (screen_texture_height = 1; screen_texture_height < glheight; screen_texture_height *= 2);

	// Disable blooms if we can't handle a texture of that size.
	glGetIntegerv(GL_MAX_TEXTURE_SIZE, &maxtexsize);
	if (screen_texture_width > maxtexsize || screen_texture_height > maxtexsize)
	{
		screen_texture_width = screen_texture_height = 0;
		Cvar_SetValue(&r_bloom, 0);
		Com_Printf("WARNING: 'R_InitBloomScreenTexture' too high resolution for Light Bloom. Effect disabled\n");
		return;
	}

	// Init the screen texture.
	size = screen_texture_width * screen_texture_height * sizeof(int);
	data = malloc(size);// Q_malloc(size);
	memset(data, 255, size);
	//r_bloomscreentexture = GL_LoadTexture ( "***r_screenbackuptexture***", screen_texture_width, screen_texture_height, data, 0, 4); // false, false, 4);

	if (!r_bloomscreentexture)
		r_bloomscreentexture = texture_extension_number++;

	//if (gl_gammacorrection.integer)
	//{
		//glinternalfmt = GL_SRGB8;
	//}
	//else
	{
		glinternalfmt = gl_solid_format;
	}

	GL_Bind(r_bloomscreentexture);
	glTexImage2D(GL_TEXTURE_2D, 0, glinternalfmt, screen_texture_width, screen_texture_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	free(data);//Q_free(data);

	// Validate bloom size and init the bloom effect texture.
	R_Bloom_InitEffectTexture();

	// If screensize is more than 2x the bloom effect texture, set up for stepped downsampling.
	r_bloomdownsamplingtexture = 0;
	r_screendownsamplingtexture_size = 0;
	if (glwidth > (BLOOM_SIZE * 2) && !r_bloom_fast_sample.value)
	{
		r_screendownsamplingtexture_size = (int)(BLOOM_SIZE * 2);
		data = calloc(r_screendownsamplingtexture_size * r_screendownsamplingtexture_size, sizeof(int));// Q_calloc in ez
		r_bloomdownsamplingtexture = GL_LoadTexture("***r_bloomdownsamplingtexture***", r_screendownsamplingtexture_size, r_screendownsamplingtexture_size, data, 0, 4);
		free(data);//Q_free(data);
	}

	// Init the screen backup texture.
	if (r_screendownsamplingtexture_size)
		R_Bloom_InitBackUpTexture(r_screendownsamplingtexture_size, r_screendownsamplingtexture_size);
	else
		R_Bloom_InitBackUpTexture(BLOOM_SIZE, BLOOM_SIZE);
}

// =================
// R_InitBloomTextures
// =================
void R_InitBloomTextures(void)
{
	BLOOM_SIZE = 0;
	if (!r_bloom.value)
	{
		return;
	}

	r_bloomscreentexture = 0;	// This came from a vid_restart, where none of the textures are valid any more.
	R_Bloom_InitTextures();
}

// =================
// R_Bloom_DrawEffect
// =================
void R_Bloom_DrawEffect(void)
{
	GL_Bind(r_bloomeffecttexture);

	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE);
	glColor4f(r_bloom_alpha.value, r_bloom_alpha.value, r_bloom_alpha.value, 1.0f);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glBegin(GL_QUADS);
	glTexCoord2f(0, sampleText_tch);
	glVertex2f(curView_x, curView_y);
	glTexCoord2f(0, 0);
	glVertex2f(curView_x, curView_y + curView_height);
	glTexCoord2f(sampleText_tcw, 0);
	glVertex2f(curView_x + curView_width, curView_y + curView_height);
	glTexCoord2f(sampleText_tcw, sampleText_tch);
	glVertex2f(curView_x + curView_width, curView_y);
	glEnd();

	glDisable(GL_BLEND);
}

#if 0
// =================
// R_Bloom_GeneratexCross - alternative bluring method
// =================
void R_Bloom_GeneratexCross(void)
{
	int         i;
	static int      BLOOM_BLUR_RADIUS = 8;
	//static float  BLOOM_BLUR_INTENSITY = 2.5f;
	float   BLOOM_BLUR_INTENSITY;
	static float intensity;
	static float range;

	// Setup sample size workspace.
	glViewport(0, 0, sample_width, sample_height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, sample_width, sample_height, 0, -10, 100);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Copy small scene into r_bloomeffecttexture.
	GL_Bind(r_bloomeffecttexture);
	//glBindTexture(GL_TEXTURE_2D, r_bloomeffecttexture);
	glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, sample_width, sample_height);

	// Start modifying the small scene corner.
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	glEnable(GL_BLEND);

	// Darkening passes.
	if (r_bloom_darken.value)
	{
		glBlendFunc(GL_DST_COLOR, GL_ZERO);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

		for (i = 0; i < r_bloom_darken.integer; i++)
		{
			R_Bloom_SamplePass(0, 0);
		}
		glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, sample_width, sample_height);
	}

	// Bluring passes.
	if (BLOOM_BLUR_RADIUS)
	{
		glBlendFunc(GL_ONE, GL_ONE);

		range = (float)BLOOM_BLUR_RADIUS;

		BLOOM_BLUR_INTENSITY = r_bloom_intensity.value;
		//diagonal-cross draw 4 passes to add initial smooth
		glColor4f(0.5f, 0.5f, 0.5f, 1.0);
		R_Bloom_SamplePass(1, 1);
		R_Bloom_SamplePass(-1, 1);
		R_Bloom_SamplePass(-1, -1);
		R_Bloom_SamplePass(1, -1);
		glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, sample_width, sample_height);

		for (i = -(BLOOM_BLUR_RADIUS + 1); i < BLOOM_BLUR_RADIUS; i++)
		{
			intensity = BLOOM_BLUR_INTENSITY / (range * 2 + 1) * (1 - fabs(i * i) / (float)(range * range));
			if (intensity < 0.05f)
			{
				continue;
			}
			glColor4f(intensity, intensity, intensity, 1.0f);
			R_Bloom_SamplePass(i, 0);
			//R_Bloom_SamplePass( -i, 0 );
		}

		glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, sample_width, sample_height);

		//for(i = 0; i < BLOOM_BLUR_RADIUS; i++)
		for (i = -(BLOOM_BLUR_RADIUS + 1); i < BLOOM_BLUR_RADIUS; i++)
		{
			intensity = BLOOM_BLUR_INTENSITY / (range * 2 + 1) * (1 - fabs(i * i) / (float)(range * range));
			if (intensity < 0.05f)
			{
				continue;
			}
			glColor4f(intensity, intensity, intensity, 1.0f);
			R_Bloom_SamplePass(0, i);
			//R_Bloom_SamplePass( 0, -i );
		}

		glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, sample_width, sample_height);
	}

	// Restore full screen workspace.
	glViewport(0, 0, glwidth, glheight);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, glwidth, glheight, 0, -10, 100);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}
#endif

// =================
// R_Bloom_GeneratexDiamonds
//=================
void R_Bloom_GeneratexDiamonds(void)
{
	int         i, j;
	static float intensity;

	// Setup sample size workspace
	glViewport(0, 0, sample_width, sample_height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, sample_width, sample_height, 0, -10, 100);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Copy small scene into r_bloomeffecttexture.
	GL_Bind(r_bloomeffecttexture);
	//glBindTexture(GL_TEXTURE_2D, r_bloomeffecttexture);
	glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, sample_width, sample_height);

	// Start modifying the small scene corner.
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	glEnable(GL_BLEND);

	// Darkening passes
	if (r_bloom_darken.value)
	{
		glBlendFunc(GL_DST_COLOR, GL_ZERO);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

		for (i = 0; i < (int)r_bloom_darken.value; i++)
		{
			R_Bloom_SamplePass(0, 0);
		}
		glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, sample_width, sample_height);
	}

	// Bluring passes.
	//glBlendFunc(GL_ONE, GL_ONE);
	glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_COLOR);

	if (r_bloom_diamond_size.value > 7 || r_bloom_diamond_size.value <= 3)
	{
		if (r_bloom_diamond_size.value != 8)
		{
			Cvar_SetValue(&r_bloom_diamond_size, 8);
		}

		for (i = 0; i < (int)r_bloom_diamond_size.value; i++)
		{
			for (j = 0; j < (int)r_bloom_diamond_size.value; j++)
			{
				intensity = r_bloom_intensity.value * 0.3 * Diamond8x[i][j];
				if (intensity < 0.01f)
				{
					continue;
				}
				glColor4f(intensity, intensity, intensity, 1.0);
				R_Bloom_SamplePass(i - 4, j - 4);
			}
		}
	}
	else if (r_bloom_diamond_size.value > 5)
	{
		if (r_bloom_diamond_size.value != 6)
		{
			Cvar_SetValue(&r_bloom_diamond_size, 6);
		}

		for (i = 0; i < (int)r_bloom_diamond_size.value; i++)
		{
			for (j = 0; j < (int)r_bloom_diamond_size.value; j++)
			{
				intensity = r_bloom_intensity.value * 0.5 * Diamond6x[i][j];
				if (intensity < 0.01f)
				{
					continue;
				}
				glColor4f(intensity, intensity, intensity, 1.0);
				R_Bloom_SamplePass(i - 3, j - 3);
			}
		}
	}
	else if (r_bloom_diamond_size.value > 3)
	{
		if (r_bloom_diamond_size.value != 4)//integer != 4)
		{
			Cvar_SetValue(&r_bloom_diamond_size, 4);
		}

		for (i = 0; i < (int)r_bloom_diamond_size.value; i++)//integer; i++)
		{
			for (j = 0; j < (int)r_bloom_diamond_size.value;j++)//.integer; j++)
			{
				intensity = r_bloom_intensity.value * 0.8f * Diamond4x[i][j];
				if (intensity < 0.01f) continue;
				glColor4f(intensity, intensity, intensity, 1.0);
				R_Bloom_SamplePass(i - 2, j - 2);
			}
		}
	}

	glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, sample_width, sample_height);

	// Restore full screen workspace.
	glViewport(0, 0, glwidth, glheight);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, glwidth, glheight, 0, -10, 100);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

// =================
// R_Bloom_DownsampleView
// =================
void R_Bloom_DownsampleView(void)
{
	glDisable(GL_BLEND);
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

	// Stepped downsample.
	if (r_screendownsamplingtexture_size)
	{
		int     midsample_width = r_screendownsamplingtexture_size * sampleText_tcw;
		int     midsample_height = r_screendownsamplingtexture_size * sampleText_tch;

		// Copy the screen and draw resized.
		GL_Bind(r_bloomscreentexture);
		glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, curView_x, glheight - (curView_y + curView_height), curView_width, curView_height);
		R_Bloom_Quad(0, glheight - midsample_height, midsample_width, midsample_height, screenText_tcw, screenText_tch);

		// Now copy into Downsampling (mid-sized) texture.
		GL_Bind(r_bloomdownsamplingtexture);
		glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, midsample_width, midsample_height);

		// Now draw again in bloom size.
		glColor4f(0.5f, 0.5f, 0.5f, 1.0f);
		R_Bloom_Quad(0, glheight - sample_height, sample_width, sample_height, sampleText_tcw, sampleText_tch);

		// Now blend the big screen texture into the bloom generation space (hoping it adds some blur).
		glEnable(GL_BLEND);
		glBlendFunc(GL_ONE, GL_ONE);
		glColor4f(0.5f, 0.5f, 0.5f, 1.0f);
		GL_Bind(r_bloomscreentexture);
		R_Bloom_Quad(0, glheight - sample_height, sample_width, sample_height, screenText_tcw, screenText_tch);
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		glDisable(GL_BLEND);
	}
	else
	{
		// Downsample simple.
		GL_Bind(r_bloomscreentexture);
		glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, curView_x, glheight - (curView_y + curView_height), curView_width, curView_height);
		R_Bloom_Quad(0, glheight - sample_height, sample_width, sample_height, screenText_tcw, screenText_tch);
	}
}

// =================
// R_BloomBlend
// =================
void R_BloomBlend(void)
{
	extern vrect_t	scr_vrect;

	if (!r_bloom.value)
	{
		return;
	}

	if (!BLOOM_SIZE || screen_texture_width < glwidth || screen_texture_height < glheight)
	{
		R_Bloom_InitTextures();
	}

	if (screen_texture_width < BLOOM_SIZE ||
		screen_texture_height < BLOOM_SIZE)
	{
		return;
	}

	// Set up full screen workspace.
	glViewport(0, 0, glwidth, glheight);
	glDisable(GL_DEPTH_TEST);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, glwidth, glheight, 0, -10, 100);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glDisable(GL_CULL_FACE);

	glDisable(GL_BLEND);
	glEnable(GL_TEXTURE_2D);

	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);//--- not in ezq OfN
	
	glColor4f(1, 1, 1, 1);

	// Setup current sizes
	curView_x = scr_vrect.x * ((float)glwidth / vid.width);
	curView_y = scr_vrect.y * ((float)glheight / vid.height);
	curView_width = scr_vrect.width * ((float)glwidth / vid.width);
	curView_height = scr_vrect.height * ((float)glheight / vid.height);
	screenText_tcw = ((float)curView_width / (float)screen_texture_width);
	screenText_tch = ((float)curView_height / (float)screen_texture_height);

	if (scr_vrect.height > scr_vrect.width)
	{
		sampleText_tcw = ((float)scr_vrect.width / (float)scr_vrect.height);
		sampleText_tch = 1.0f;
	}
	else
	{
		sampleText_tcw = 1.0f;
		sampleText_tch = ((float)scr_vrect.height / (float)scr_vrect.width);
	}

	sample_width = BLOOM_SIZE * sampleText_tcw;
	sample_height = BLOOM_SIZE * sampleText_tch;

	// Copy the screen space we'll use to work into the backup texture.
	GL_Bind(r_bloombackuptexture);
	//glBindTexture(GL_TEXTURE_2D, r_bloombackuptexture);
	glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, r_screenbackuptexture_size * sampleText_tcw, r_screenbackuptexture_size * sampleText_tch);

	// Create the bloom image.
	R_Bloom_DownsampleView();
	R_Bloom_GeneratexDiamonds();
	//R_Bloom_GeneratexCross();

	// Restore the screen-backup to the screen.
	glDisable(GL_BLEND);
	GL_Bind(r_bloombackuptexture);
	glColor4f(1, 1, 1, 1);
	R_Bloom_Quad(0,
		glheight - (r_screenbackuptexture_size * sampleText_tch),
		r_screenbackuptexture_size * sampleText_tcw,
		r_screenbackuptexture_size * sampleText_tch,
		sampleText_tcw,
		sampleText_tch);

	R_Bloom_DrawEffect();

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

//==================================================================================
// strlcat from EZQuake

size_t strlcat(char* dst, const char* src, size_t siz)
{
	register char* d = dst;
	register const char* s = src;
	register size_t n = siz;
	size_t dlen;

	/* Find the end of dst and adjust bytes left but don't go past end */
	while (n-- != 0 && *d != '\0')
		d++;
	dlen = d - dst;
	n = siz - dlen;

	if (n == 0)
		return(dlen + strlen(s));
	while (*s != '\0') {
		if (n != 1) {
			*d++ = *s;
			n--;
		}
		s++;
	}
	*d = '\0';

	return(dlen + (s - src));       /* count does not include NUL */
}

// SHA1 Code from ezquake

/*
SHA-1 in C
By Steve Reid <steve@edmweb.com>
100% Public Domain

Test Vectors (from FIPS PUB 180-1)
"abc"
  A9993E36 4706816A BA3E2571 7850C26C 9CD0D89D
"abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq"
  84983E44 1C3BD26E BAAE4AA1 F95129E5 E54670F1
A million repetitions of "a"
  34AA973C D4C4DAA4 F61EEB2B DBAD2731 6534016F

	$Id: sha1.c,v 1.11 2007-08-08 00:53:29 disconn3ct Exp $
*/

/* #define SHA1HANDSOFF * Copies data before messing with it. */

//#include <stdio.h>
//#include <string.h>
//#include "sha1.h"
//#include "common.h"

/* Hash a single 512-bit block. This is the core of the algorithm. */

void SHA1Transform(unsigned int state[5], unsigned char buffer[64])
{
	unsigned int a, b, c, d, e;
	typedef union {
		unsigned char c[64];
		unsigned int l[16];
	} CHAR64LONG16;
	CHAR64LONG16* block;
#ifdef SHA1HANDSOFF
	static unsigned char workspace[64];
	block = (CHAR64LONG16*)workspace;
	memcpy(block, buffer, 64);
#else
	block = (CHAR64LONG16*)buffer;
#endif
	/* Copy context->state[] to working vars */
	a = state[0];
	b = state[1];
	c = state[2];
	d = state[3];
	e = state[4];
	/* 4 rounds of 20 operations each. Loop unrolled. */
	R0(a, b, c, d, e, 0); R0(e, a, b, c, d, 1); R0(d, e, a, b, c, 2); R0(c, d, e, a, b, 3);
	R0(b, c, d, e, a, 4); R0(a, b, c, d, e, 5); R0(e, a, b, c, d, 6); R0(d, e, a, b, c, 7);
	R0(c, d, e, a, b, 8); R0(b, c, d, e, a, 9); R0(a, b, c, d, e, 10); R0(e, a, b, c, d, 11);
	R0(d, e, a, b, c, 12); R0(c, d, e, a, b, 13); R0(b, c, d, e, a, 14); R0(a, b, c, d, e, 15);
	R1(e, a, b, c, d, 16); R1(d, e, a, b, c, 17); R1(c, d, e, a, b, 18); R1(b, c, d, e, a, 19);
	R2(a, b, c, d, e, 20); R2(e, a, b, c, d, 21); R2(d, e, a, b, c, 22); R2(c, d, e, a, b, 23);
	R2(b, c, d, e, a, 24); R2(a, b, c, d, e, 25); R2(e, a, b, c, d, 26); R2(d, e, a, b, c, 27);
	R2(c, d, e, a, b, 28); R2(b, c, d, e, a, 29); R2(a, b, c, d, e, 30); R2(e, a, b, c, d, 31);
	R2(d, e, a, b, c, 32); R2(c, d, e, a, b, 33); R2(b, c, d, e, a, 34); R2(a, b, c, d, e, 35);
	R2(e, a, b, c, d, 36); R2(d, e, a, b, c, 37); R2(c, d, e, a, b, 38); R2(b, c, d, e, a, 39);
	R3(a, b, c, d, e, 40); R3(e, a, b, c, d, 41); R3(d, e, a, b, c, 42); R3(c, d, e, a, b, 43);
	R3(b, c, d, e, a, 44); R3(a, b, c, d, e, 45); R3(e, a, b, c, d, 46); R3(d, e, a, b, c, 47);
	R3(c, d, e, a, b, 48); R3(b, c, d, e, a, 49); R3(a, b, c, d, e, 50); R3(e, a, b, c, d, 51);
	R3(d, e, a, b, c, 52); R3(c, d, e, a, b, 53); R3(b, c, d, e, a, 54); R3(a, b, c, d, e, 55);
	R3(e, a, b, c, d, 56); R3(d, e, a, b, c, 57); R3(c, d, e, a, b, 58); R3(b, c, d, e, a, 59);
	R4(a, b, c, d, e, 60); R4(e, a, b, c, d, 61); R4(d, e, a, b, c, 62); R4(c, d, e, a, b, 63);
	R4(b, c, d, e, a, 64); R4(a, b, c, d, e, 65); R4(e, a, b, c, d, 66); R4(d, e, a, b, c, 67);
	R4(c, d, e, a, b, 68); R4(b, c, d, e, a, 69); R4(a, b, c, d, e, 70); R4(e, a, b, c, d, 71);
	R4(d, e, a, b, c, 72); R4(c, d, e, a, b, 73); R4(b, c, d, e, a, 74); R4(a, b, c, d, e, 75);
	R4(e, a, b, c, d, 76); R4(d, e, a, b, c, 77); R4(c, d, e, a, b, 78); R4(b, c, d, e, a, 79);
	/* Add the working vars back into context.state[] */
	state[0] += a;
	state[1] += b;
	state[2] += c;
	state[3] += d;
	state[4] += e;
	/* Wipe variables */
	a = b = c = d = e = 0;
}


/* SHA1Init - Initialize new context */

void SHA1Init(SHA1_CTX* context)
{
	/* SHA1 initialization constants */
	context->state[0] = 0x67452301;
	context->state[1] = 0xEFCDAB89;
	context->state[2] = 0x98BADCFE;
	context->state[3] = 0x10325476;
	context->state[4] = 0xC3D2E1F0;
	context->count[0] = context->count[1] = 0;
}


/* Run your data through this. */

void SHA1Update(SHA1_CTX* context, unsigned char* data, unsigned int len)
{
	unsigned int i, j;

	j = (context->count[0] >> 3) & 63;
	if ((context->count[0] += len << 3) < (len << 3)) context->count[1]++;
	context->count[1] += (len >> 29);
	if ((j + len) > 63) {
		memcpy(&context->buffer[j], data, (i = 64 - j));
		SHA1Transform(context->state, context->buffer);
		for (; i + 63 < len; i += 64) {
			SHA1Transform(context->state, &data[i]);
		}
		j = 0;
	}
	else i = 0;
	memcpy(&context->buffer[j], &data[i], len - i);
}


/* Add padding and return the message digest. */

void SHA1Final(unsigned char digest[DIGEST_SIZE], SHA1_CTX* context)
{
	unsigned int i, j;
	unsigned char finalcount[8];

	for (i = 0; i < 8; i++) {
		finalcount[i] = (unsigned char)((context->count[(i >= 4 ? 0 : 1)]
			>> ((3 - (i & 3)) * 8)) & 255);  /* Endian independent */
	}
	SHA1Update(context, (unsigned char*)"\200", 1);
	while ((context->count[0] & 504) != 448) {
		SHA1Update(context, (unsigned char*)"\0", 1);
	}
	SHA1Update(context, finalcount, 8);  /* Should cause a SHA1Transform() */
	for (i = 0; i < DIGEST_SIZE; i++) {
		digest[i] = (unsigned char)
			((context->state[i >> 2] >> ((3 - (i & 3)) * 8)) & 255);
	}
	/* Wipe variables */
	i = j = 0;
	memset(context->buffer, 0, 64);
	memset(context->state, 0, 20);
	memset(context->count, 0, 8);
	memset(&finalcount, 0, 8);
#ifdef SHA1HANDSOFF  /* make SHA1Transform overwrite it's own static vars */
	SHA1Transform(context->state, context->buffer);
#endif
}

//VVD: SHA1 crypt
char* bin2hex(unsigned char* d)
{
	static char	ret[DIGEST_SIZE * 2 + 1];
	int		i;
	for (i = 0; i < DIGEST_SIZE * 2; i += 2, d++)
		snprintf(ret + i, DIGEST_SIZE * 2 + 1 - i, "%02X", *d);
	return ret;
}

char* SHA1(char* string)
{
	SHA1_CTX	context;
	unsigned char	digest[DIGEST_SIZE];
	SHA1Init(&context);
	SHA1Update(&context, (unsigned char*)string, strlen(string));
	SHA1Final(digest, &context);
	return bin2hex(digest);
}

SHA1_CTX	context;
void SHA1_Init(void)
{
	SHA1Init(&context);
}
void SHA1_Update(unsigned char* string)
{
	SHA1Update(&context, string, strlen((char*)string));
}
char* SHA1_Final(void)
{
	unsigned char	digest[DIGEST_SIZE];
	SHA1Final(digest, &context);
	return bin2hex(digest);
}

//=========================================================================================================================================
// On Windows copy to the clipboard the relevant integration string for easy automation of the fog/weather/sky/shader/misc tokens

#ifdef _WIN32

char* TempVA_str_float(char* format, float valf)
{
	static char VA_str_temp[256];

	sprintf(VA_str_temp, format, valf);

	return VA_str_temp;
}

char* TempVA_str_int(char* format, int vali)
{
	static char VA_str_temp[256];

	sprintf(VA_str_temp, format, vali);

	return VA_str_temp;
}

void CopyIntegrationString_f(void)
{
	char result[1024];
	result[0] = '\"';
	result[1] = '\0';

	qboolean started = false;

	if (gl_fog.value)
	{
		strcat(result, TempVA_str_int("fm:%d ", (int)gl_fog_mode.value));

		strcat(result, TempVA_str_float("fr:%g ", gl_fog_red.value));
		strcat(result, TempVA_str_float("fg:%g ", gl_fog_green.value));
		strcat(result, TempVA_str_float("fb:%g", gl_fog_blue.value));
		
		if (gl_fog_mode.value == 0)
		{
			strcat(result, TempVA_str_int(" fn:%d", (int)gl_fog_near.value));
			strcat(result, TempVA_str_int(" ff:%d", (int)gl_fog_far.value));
		}
		else
		{
			strcat(result, TempVA_str_float(" fd:%g", gl_fog_density.value));
		}

		strcat(result, TempVA_str_int(" fs:%d", (int)gl_fog_skydist.value));

		started = true;
	}

	if (weather_mode.value != 0)
	{
		if (started)
			strcat(result, " ");

		if (weather_mode.value == 1) // Rain
		{
			strcat(result, TempVA_str_int("wm:1 wr:%d",(int)weather_rain_red.value));
			strcat(result, TempVA_str_int(" wg:%d", (int)weather_rain_green.value));
			strcat(result, TempVA_str_int(" wb:%d", (int)weather_rain_blue.value));
		}
		else // Snow
		{
			strcat(result, "wm:2");
		}

		started = true;
	}

	if (r_skyname.string[0])
	{
		if (started)
			strcat(result, " ");

		strcat(result, va("sn:%s", r_skyname.string));

		started = true;
	}

	if (r_watershader.value)
	{
		if (started)
			strcat(result, " ");

		strcat(result, "hm:1 ");

		if (strcmp(shader_ref_balance.string, shader_ref_balance.defaultvalue))
			strcat(result, TempVA_str_float("hf:%g ", shader_ref_balance.value));

		if (strcmp(shader_tex_balance.string, shader_tex_balance.defaultvalue))
			strcat(result, TempVA_str_float("hw:%g ", shader_tex_balance.value));

		if (strcmp(shader_wavesize.string, shader_wavesize.defaultvalue))
			strcat(result, TempVA_str_float("hv:%g ", shader_wavesize.value));

		if (strcmp(shader_wavestrength.string, shader_wavestrength.defaultvalue))
			strcat(result, TempVA_str_float("hs:%g ", shader_wavestrength.value));

		if (strcmp(shader_waverate.string, shader_waverate.defaultvalue))
			strcat(result, TempVA_str_float("hr:%g ", shader_waverate.value));

		if (strcmp(shader_color.string, shader_color.defaultvalue))
			strcat(result, va("hc:%s ", shader_color.string));

		if (strcmp(shader_col_balance.string, shader_col_balance.defaultvalue))
			strcat(result, TempVA_str_float("hb:%g ", shader_col_balance.value));

		if (strcmp(shader_fresnel.string, shader_fresnel.defaultvalue))
			strcat(result, TempVA_str_float("hn:%g ", shader_fresnel.value));

		if (strcmp(shader_rip_color.string, shader_rip_color.defaultvalue))
			strcat(result, va("hp:%s ", shader_rip_color.string));

		if (strcmp(shader_rip_balance.string, shader_rip_balance.defaultvalue))
			strcat(result, TempVA_str_float("ha:%g ", shader_rip_balance.value));

		if (strcmp(shader_light_x.string, shader_light_x.defaultvalue))
			strcat(result, TempVA_str_int("hx:%d ", (int)shader_light_x.value));

		if (strcmp(shader_light_y.string, shader_light_y.defaultvalue))
			strcat(result, TempVA_str_int("hy:%d ", (int)shader_light_y.value));

		if (strcmp(shader_light_z.string, shader_light_z.defaultvalue))
			strcat(result, TempVA_str_int("hz:%d ", (int)shader_light_z.value));

		if (strcmp(shader_rip_shine.string, shader_rip_shine.defaultvalue))
			strcat(result, TempVA_str_float("hh:%g ", shader_rip_shine.value));

		if (strcmp(shader_rip_reflect.string, shader_rip_reflect.defaultvalue))
			strcat(result, TempVA_str_float("hi:%g ", shader_rip_reflect.value));

		if (strcmp(shader_displacement.string, shader_displacement.defaultvalue))
			strcat(result, TempVA_str_float("hd:%g ", shader_displacement.value));

		if (strcmp(shader_rip_distortion.string, shader_rip_distortion.defaultvalue))
			strcat(result, TempVA_str_float("ht:%g ", shader_rip_distortion.value));

		if (strcmp(shader_rip_source.string, shader_rip_source.defaultvalue))
			strcat(result, TempVA_str_int("he:%d ", (int)shader_rip_source.value));

		if (result[strlen(result) - 1] == ' ')
			result[strlen(result) - 1] = '\0';

		started = true;
	}

	if (r_outlines.value)
	{
		if (started)
			strcat(result, " ");

		strcat(result, TempVA_str_int("ml:%d ", (int)r_outlines_width.value));

		if (strcmp(r_outlines_color.string,r_outlines_color.defaultvalue))
			strcat(result, va("mc:%s ", r_outlines_color.string));

		if (strcmp(r_outlines_factor.string, r_outlines_factor.defaultvalue))
			strcat(result, TempVA_str_float("mf:%g", r_outlines_factor.value));

		if (result[strlen(result) - 1] == ' ')
			result[strlen(result) - 1] = '\0';

		started = true;
	}

	if (!r_bloom.value)
	{
		if (started)
			strcat(result, " ");

		strcat(result, "mb:0");

		started = true;
	}
	else
	{
		if (strcmp(r_bloom_intensity.string, r_bloom_intensity.defaultvalue))
		{
			if (started)
				strcat(result, " ");
			
			strcat(result, TempVA_str_float("mb:1 mi:%g",r_bloom_intensity.value));
			started = true;
		}
	}

	if (!gl_caustics.value)
	{
		if (started)
			strcat(result, " ");

		strcat(result, "mw:0");
		started = true;
	}

	if (!gl_detail.value)
	{
		if (started)
			strcat(result, " ");

		strcat(result, "md:0");
		started = true;
	}

	if (!started)
	{
		Com_Printf("No relevant tokens on integration currently!\n");
		return;
	}

	if (result[strlen(result) - 1] == ' ')
		result[strlen(result) - 1] = '\0';

	strcat(result, "\"");

	Sys_CopyToClipboard(result);
	Com_Printf("Integration tokens written to the Windows clipboard\n");
}
#endif

//===================================================================================
// OfN - Draw outlines - November 2023
// I wanted a borderlands-like rendering style, kinda experimental
//===================================================================================

cvar_t r_outlines = { "r_outlines", "0",CVAR_ALLOW_RECURSION };
cvar_t r_outlines_width = { "r_outlines_width", "2",CVAR_ALLOW_RECURSION };
cvar_t r_outlines_color = { "r_outlines_color","000",CVAR_ALLOW_RECURSION };
cvar_t r_outlines_factor = { "r_outlines_factor","1",CVAR_ALLOW_RECURSION };

qboolean CompileOutlinesShaders(void)
{
	const char* vertshader_outlines =
		"#version 110\n"
		"void main()\n"
		"{\n"
			"gl_Position = ftransform();\n"
		"}\n";

	const char* fragshader_outlines =
		"#version 110\n"
		"uniform sampler2D originaltex;\n"
		"uniform sampler2D normalstex;\n"
		"uniform sampler2D depthtex;\n"
		"uniform int swidth;\n"
		"uniform int sheight;\n"
		"uniform float fwidth;\n"
		"uniform float fheight;\n"
		"uniform float linewidth;\n"
		"uniform float wfactor;\n"
		"uniform vec3 color;\n"
		"const float far = 100000.0;\n"
		"const float near = 4.0;\n"

		"float LinearizeDepth(float depth)\n"
		"{\n"
			"float z = depth * 2.0 - 1.0;\n" // back to NDC 
			"float linearDepth = (2.0 * near * far) / (z * (far - near) - (far + near));\n"
			"return (linearDepth-near)/(far-near);\n"
		"}\n"

		/*"vec4 texelFetch(sampler2D tex, ivec2 size, ivec2 coord)\n"
		"{\n"
			"return texture2D(tex, vec2(float(coord.x) / float(size.x),	float(coord.y) / float(size.y)));\n"
		"}\n"

		"vec4 textureMultisample(sampler2D sampler, ivec2 coord)\n"
		"{\n"
			"vec4 color = vec4(0.0, 0.0, 0.0, 0.0);\n"

			"for (int i = 0; i < texSamples; i++)\n"
				"color += texelFetch(sampler, coord, ivec2(i, i));\n"

			"color /= float(texSamples);\n"

			"return color;\n"
		"}\n"*/

		"void main()\n"
		"{\n"
			"float offset_w = (1.0 / fwidth) * 0.85 * wfactor;\n"
			"float offset_h = (1.0 / fheight) * 0.6 * wfactor;\n"
			"vec2 absCoords = (gl_FragCoord.xy / gl_FragCoord.w) / vec2(swidth, sheight);\n"
			"vec4 origcolor = texture2D(originaltex, absCoords);\n"
			"float depthcolor = texture2D(depthtex, absCoords).r;\n"
			"vec3 normalcolor = texture2D(normalstex, absCoords).rgb;\n"

			"float depthDiff = 0.0;\n"
			"float normalDiff = 0.0;\n"

			"if (origcolor.r > 0.9 || origcolor.b > 0.9 || origcolor.g > 0.9)\n"
			"{\n"
				"gl_FragColor = origcolor;\n"//"discard;\n"
				"return;\n"
			"}\n"

			"float depthcolor2 = LinearizeDepth(depthcolor);\n"
			"float tempcol = 0.0;\n"

			"for (float i = 0.0; i < linewidth; i += 1.0)\n"
			"{\n"
				"depthDiff += abs(depthcolor - texture2D(depthtex,absCoords + vec2(offset_w+i*offset_w, 0.0)).r);\n"
				"depthDiff += abs(depthcolor - texture2D(depthtex,absCoords + vec2(-offset_w-i*offset_w, 0.0)).r);\n"
				"depthDiff += abs(depthcolor - texture2D(depthtex,absCoords + vec2(0.0, offset_h+i*offset_h)).r);\n"
				"depthDiff += abs(depthcolor - texture2D(depthtex,absCoords + vec2(0.0, -offset_h-i*offset_h)).r);\n"

				"depthDiff += abs(depthcolor - texture2D(depthtex,absCoords + vec2(offset_w+i*offset_w, offset_h+i*offset_h)).r);\n"
				"depthDiff += abs(depthcolor - texture2D(depthtex,absCoords + vec2(-offset_w-i*offset_w, offset_h+i*offset_h)).r);\n"
				"depthDiff += abs(depthcolor - texture2D(depthtex,absCoords + vec2(offset_w+i*offset_w, -offset_h-i*offset_h)).r);\n"
				"depthDiff += abs(depthcolor - texture2D(depthtex,absCoords + vec2(-offset_w-i*offset_w, -offset_h-i*offset_h)).r);\n"
				
				"tempcol = texture2D(depthtex, absCoords + vec2(offset_w+i*offset_w, 0.0)).r;\n"
				"tempcol = LinearizeDepth(tempcol);\n"
				"depthDiff += abs(depthcolor2 - tempcol);\n"
				
				"tempcol = texture2D(depthtex, absCoords + vec2(-offset_w-i*offset_w, 0.0)).r;\n"
				"tempcol = LinearizeDepth(tempcol);\n"
				"depthDiff += abs(depthcolor2 - tempcol);\n"

				"tempcol = texture2D(depthtex, absCoords + vec2(0.0, offset_h+i*offset_h)).r;\n"
				"tempcol = LinearizeDepth(tempcol);\n"
				"depthDiff += abs(depthcolor2 - tempcol);\n"

				"tempcol = texture2D(depthtex, absCoords + vec2(0.0, -offset_h-i*offset_h)).r;\n"
				"tempcol = LinearizeDepth(tempcol);\n"
				"depthDiff += abs(depthcolor2 - tempcol);\n"

				"tempcol = texture2D(depthtex, absCoords + vec2(offset_w+i*offset_w, offset_h+i*offset_h)).r;\n"
				"tempcol = LinearizeDepth(tempcol);\n"
				"depthDiff += abs(depthcolor2 - tempcol);\n"

				"tempcol = texture2D(depthtex, absCoords + vec2(-offset_w-i*offset_w, offset_h+i*offset_h)).r;\n"
				"tempcol = LinearizeDepth(tempcol);\n"
				"depthDiff += abs(depthcolor2 - tempcol);\n"

				"tempcol = texture2D(depthtex, absCoords + vec2(offset_w+i*offset_w, -offset_h-i*offset_h)).r;\n"
				"tempcol = LinearizeDepth(tempcol);\n"
				"depthDiff += abs(depthcolor2 - tempcol);\n"

				"tempcol = texture2D(depthtex, absCoords + vec2(-offset_w-i*offset_w, -offset_h-i*offset_h)).r;\n"
				"tempcol = LinearizeDepth(tempcol);\n"
				"depthDiff += abs(depthcolor2 - tempcol);\n"

				"normalDiff += distance(normalcolor, texture2D(normalstex,absCoords + vec2(offset_w+i*offset_w, 0.0)).rgb);\n"
				"normalDiff += distance(normalcolor, texture2D(normalstex,absCoords + vec2(0.0, offset_h+i*offset_h)).rgb);\n"
				"normalDiff += distance(normalcolor, texture2D(normalstex,absCoords + vec2(-offset_w-i*offset_w, 0.0)).rgb);\n"
				"normalDiff += distance(normalcolor, texture2D(normalstex,absCoords + vec2(0.0, -offset_h-i*offset_h)).rgb);\n"
				
				"normalDiff += distance(normalcolor, texture2D(normalstex,absCoords + vec2(offset_w+i*offset_w, offset_h+i*offset_h)).rgb);\n"
				"normalDiff += distance(normalcolor, texture2D(normalstex,absCoords + vec2(-offset_w-i*offset_w, -offset_h-i*offset_h)).rgb);\n"
				"normalDiff += distance(normalcolor, texture2D(normalstex,absCoords + vec2(-offset_w-i*offset_w, offset_h+i*offset_h)).rgb);\n"
				"normalDiff += distance(normalcolor, texture2D(normalstex,absCoords + vec2(offset_w+i*offset_w, -offset_h-i*offset_h)).rgb);\n"
				
				"normalDiff = normalDiff / (linewidth * 2.0);\n"
			"}\n"

			"depthDiff = depthDiff * 64.0;\n"//128.0;\n" // TOCHECK: Kinda arbitrary
			
			"normalDiff = normalDiff * 2.0;\n"

			"float outline = normalDiff + depthDiff;\n"
			"outline = clamp(outline, 0.0, 1.0);\n"
			"vec4 outlinecolor = vec4(vec3(color), 1.0);\n"//alphares);\n"			

			"gl_FragColor = vec4(mix(origcolor,outlinecolor,outline));\n"
		"}\n";


	//======================================================//
	int vert = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vert, 1, &vertshader_outlines, NULL);
	glCompileShader(vert);
	if (CheckShaderError(vert))
	{
		ShaderExtPresent = false;
		glDeleteShader(vert);
		return false;
	}
	int frag = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(frag, 1, &fragshader_outlines, NULL);
	glCompileShader(frag);
	if (CheckShaderError(frag))
	{
		ShaderExtPresent = false;
		glDeleteShader(vert);
		glDeleteShader(frag);
		return false;
	}
	ShaderProgsOutlines = glCreateProgram();
	glAttachShader(ShaderProgsOutlines, vert);
	glDeleteShader(vert); //preemptive cleanup is fine
	glAttachShader(ShaderProgsOutlines, frag);
	glDeleteShader(frag);
	glLinkProgram(ShaderProgsOutlines);

	if (CheckProgramError(ShaderProgsOutlines))
	{
		ShaderExtPresent = false;
		glDeleteProgram(ShaderProgsOutlines);
		return false;
	}

	return true;
}

extern void R_ClearTextureChains(model_t* clmodel);
extern void R_RecursiveWorldNode(mnode_t* node, int clipflags);
extern void DrawTextureChains(model_t* model);

extern void S_ExtraUpdate(void);
//extern void R_Clear(void);

void R_DrawSurfaceNormalsForOutlines(void)
{
	specialrender = SR_FLATNORMALS;

	bindFrameBuffer(outlinesFrameBuffer);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	entity_t ent;

	memset(&ent, 0, sizeof(ent));
	ent.model = cl.worldmodel;

	R_ClearTextureChains(cl.worldmodel);

	VectorCopy(r_refdef.vieworg, modelorg);

	currententity = &ent;
	currenttexture = -1;
	//set up texture chains for the world
	R_RecursiveWorldNode(cl.worldmodel->nodes, 15);

	//draw the world sky
	/*if (r_skyboxloaded)
		R_DrawSkyBox();
	else
		R_DrawSkyChain();*/

	R_DrawEntitiesOnList(&cl_firstpassents); // uncommented

	//draw the world
	DrawTextureChains(cl.worldmodel);

	//draw the world alpha textures
	R_DrawAlphaChain();

	S_ExtraUpdate();	// don't let sound get messed up if going slow

	R_DrawEntitiesOnList(&cl_visents);
	R_DrawEntitiesOnList(&cl_alphaents);

	R_DrawWaterSurfaces(); 

	GL_DisableMultitexture();

	R_RenderDlights();
	R_DrawParticles();

	//VULT: CORONAS
	//Even if coronas gets turned off, let active ones fade out
	if (gl_coronas.value || CoronaCount)
		R_DrawCoronas();

	unbindCurrentFrameBuffer();

	specialrender = SR_NORMAL;
}

void R_OutlinesSetup(void)
{
	if (!ShaderExtPresent)
		return;

#ifdef _WIN32
	int height = WindowRect.bottom - WindowRect.top;
	int width = WindowRect.right - WindowRect.left;
#else
	extern int scr_width;
	extern int scr_height;

	int height = scr_height;//scr_vrect.height;
	int width = scr_width;// scr_vrect.width;//vid.width;
#endif

	unsigned char* data;

	data = (unsigned char*)calloc(width * height, sizeof(int));

	backupOutlinesTexture = GL_LoadTexture("***r_outlinestexture***", width, height, data, TEX_NPOT, 4);

	free(data);		
}

void R_OutlinesDraw(void)
{
#ifdef _WIN32
	int height = WindowRect.bottom - WindowRect.top;
	int width = WindowRect.right - WindowRect.left;
#else
	extern int scr_width;
	extern int scr_height;

	int height = scr_height;//scr_vrect.height;
	int width = scr_width;// scr_vrect.width;//vid.width;
#endif

	glUseProgram(ShaderProgsOutlines);

	GL_SelectTexture(GL_TEXTURE0_ARB);
	GL_Bind(backupOutlinesTexture);
	glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, width, height);

	glUniform1i(glGetUniformLocation(ShaderProgsOutlines, "originaltex"), 0);

	GL_SelectTexture(GL_TEXTURE1_ARB);
	GL_Bind(outlinesFlatNormalsTexture);
	
	glUniform1i(glGetUniformLocation(ShaderProgsOutlines, "normalstex"), 1);

	GL_SelectTexture(GL_TEXTURE2_ARB);
	GL_Bind(outlinesDepthTexture);

	glUniform1i(glGetUniformLocation(ShaderProgsOutlines, "depthtex"), 2);

	glUniform1i(glGetUniformLocation(ShaderProgsOutlines, "swidth"), width);
	glUniform1i(glGetUniformLocation(ShaderProgsOutlines, "sheight"), height);

	glUniform1f(glGetUniformLocation(ShaderProgsOutlines, "linewidth"), r_outlines_width.value);

	glUniform1f(glGetUniformLocation(ShaderProgsOutlines, "fwidth"), (float)width);
	glUniform1f(glGetUniformLocation(ShaderProgsOutlines, "fheight"), (float)height);

	glUniform1f(glGetUniformLocation(ShaderProgsOutlines, "wfactor"), r_outlines_factor.value);

	float r, g, b;

	if (strlen(r_outlines_color.string) == 3)
	{
		r = HexToInt(r_outlines_color.string[0]) / 15.0f;
		g = HexToInt(r_outlines_color.string[1]) / 15.0f;
		b = HexToInt(r_outlines_color.string[2]) / 15.0f;
	}
	else
		r = g = b = 0;

	glUniform3f(glGetUniformLocation(ShaderProgsOutlines, "color"), r, g, b);

	// Setup sample size workspace
	GL_Set2D();

	glEnable(GL_DEPTH_TEST);
	//glEnable(GL_ALPHA_TEST);
	glDepthMask(GL_TRUE);

	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

	glBegin(GL_QUADS);
	glVertex2f(0, 0);
	glVertex2f(width, 0);
	glVertex2f(width, height);
	glVertex2f(0, height);
	glEnd();

	glUseProgram(0); // Job done!

	// Restore workspace
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, width, height, 0, -99999, 99999);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	qglActiveTexture(GL_TEXTURE0_ARB);
}