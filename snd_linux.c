/*
Copyright (C) 1996-1997 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <linux/soundcard.h>
#include <stdio.h>

#include "quakedef.h"
#include "sound.h"

#include <SDL.h> // October 2022

//int audio_fd;
int snd_inited;

//static char snd_dev[64] = "/dev/dsp";

//static int tryrates[] = {11025, 22051, 44100, 8000};

// OfN
SDL_mutex *smutex;
soundhw_t *shw;
int		soundtime;
static SDL_AudioDeviceID audiodevid;

//=== OfN - From ezquake - October 2022
static void S_Update_2(void);
static void GetSoundtime2(void);

void S_LockMixer(void)
{
	SDL_LockMutex(smutex);
}

void S_UnlockMixer(void)
{
	SDL_UnlockMutex(smutex);
}

static void S_SDL_callback(void *userdata, Uint8 *stream, int len)
{
	// Mixer is run in main thread when capturing, play silence instead
	//if (Movie_IsCapturing()) {
	//	SDL_memset(stream, 0, len);
	//	return;
	//}

	S_LockMixer();
	//SDL_LockMutex(smutex);
	shw->buffer = stream;
	shw->samples = len / shw->numchannels;
	//-
	shm->samples = shw->samples;
	shm->buffer = stream;
	//-
	S_Update_2();
	shw->snd_sent += len;
	//SDL_UnlockMutex(smutex);
	S_UnlockMixer();

	// Implicit Minimized in first case
	//if ((sys_inactivesound.integer == 0 && !ActiveApp) || (sys_inactivesound.integer == 2 && Minimized) || cls.demoseeking) {
	//	SDL_memset(stream, 0, len);
	//}
}

static void GetSoundtime2(void)
{
	shw->samplepos = shw->snd_sent/2;

	if (shw->samplepos < shw->oldsamplepos) {
		shw->numwraps++; // buffer wrapped

		if (shw->paintedtime > 0x40000000) {
			// time to chop things off to avoid 32 bit limits
			shw->numwraps = 0;
			shw->paintedtime = shw->samples / shw->numchannels;
			//S_StopAllSounds (true); ALREADY COMMENTED OUT
		}
	}

	shw->oldsamplepos = shw->samplepos;

	soundtime = shw->numwraps * (shw->samples / shw->numchannels) + shw->samplepos / shw->numchannels;
}

static void S_Update_2(void)
{
	unsigned int endtime;
	int samps;

	if (!shw) {
		return;
	}

	// Updates soundtime
	GetSoundtime2();

	endtime = soundtime + shw->samples / shw->numchannels;
	soundtime = shw->paintedtime;
	samps = shw->samples / shw->numchannels;

	if (endtime - soundtime > samps) {
		endtime = soundtime + samps;
	}

	S_PaintChannels(endtime);
}
// End October 2022

qboolean SNDDMA_Init(void) {
	int rc, fmt, tmp, caps, i;
    char *s;
	struct audio_buf_info info;

	snd_inited = 0;

	// OfN - October 2022
	if (SDL_Init(SDL_INIT_AUDIO)==0)
		Com_Printf("SDL-Audio successfully initialized\n");
	else
	{		
		Com_Printf("SDL-Audio failed initialization\n");
		return 0;
	}

	smutex = SDL_CreateMutex();

	//===========================================================================================================================
	SDL_AudioSpec desired, obtained;
	soundhw_t *shw_tmp = NULL;
	int ret = 0;
	const char *requested_device = NULL;


	memset(&desired, 0, sizeof(desired));
	switch ((int)s_khz.value) {
		case 192:
			desired.freq = 192000;
			desired.samples = 2048;
			break;
		case 96:
			desired.freq = 96000;
			desired.samples = 1024;
			break;
		case 48:
			desired.freq = 48000;
			desired.samples = 512;
			break;
		case 44:
			desired.freq = 44100;
			desired.samples = 512;
			break;
		case 22:
			desired.freq = 22050;
			desired.samples = 256;
			break;
		default:
			desired.freq = 11025;
			desired.samples = 128;
			break;
	}

	desired.format = AUDIO_S16LSB;
	desired.channels = 2;
	/*if (s_desiredsamples.integer) {
		int desired_samples = 1;

		// make sure it's a power of 2
		while (desired_samples < s_desiredsamples.integer)
			desired_samples <<= 1;

		desired.samples = desired_samples;
	}*/
	desired.callback = S_SDL_callback;

	/* Make audiodevice list start from index 1 so that 0 can be system default */
	/*if (s_audiodevice.integer > 0) {
		requested_device = SDL_GetAudioDeviceName(s_audiodevice.integer - 1, 0);
	}

	if ((audiodevid = SDL_OpenAudioDevice(requested_device, 0, &desired, &obtained, 0)) <= 0) {
		Com_Printf("sound: couldn't open SDL audio: %s\n", SDL_GetError());
		if (requested_device != NULL) {
			Com_Printf("sound: retrying with default audio device\n");
			if ((audiodevid = SDL_OpenAudioDevice(NULL, 0, &desired, &obtained, 0)) <= 0) {
				Com_Printf("sound: failure again, aborting...\n");
				return false;
			}
			Cvar_LatchedSet(&s_audiodevice, "0");
		}
		return false;
	}*/

	// OfN
	if ((audiodevid = SDL_OpenAudioDevice(NULL, 0, &desired, &obtained, 0)) <= 0) {
		Com_Printf("SDL: Failed to open default device\n");
		goto fail;
	}

	if (obtained.format != AUDIO_S16LSB) {
		Com_Printf("SDL audio format %d unsupported.\n", obtained.format);
		goto fail;
	}

	if (obtained.channels != 1 && obtained.channels != 2) {
		Com_Printf("SDL audio channels %d unsupported.\n", obtained.channels);
		goto fail;
	}

	shw_tmp = Q_Calloc(1, sizeof(*shw));//Q_calloc(1, sizeof(*shw));
	if (!shw_tmp) {
		Com_Printf("Failed to alloc memory for sound structure\n");
		goto fail;
	}

	shw_tmp->khz = obtained.freq;
	shw_tmp->numchannels = obtained.channels;
	shw_tmp->samplebits = obtained.format & 0xFF;
	shw_tmp->samples = 65536;//obtained.samples;//65536; <- original number

	//Cvar_AutoSetInt(&s_desiredsamples, obtained.samples);

	shw = shw_tmp;

	//== OfN
	shm = &sn;

	shw->numwraps = shw->oldsamplepos = shw->paintedtime = shw->samplepos = shw->snd_sent = 0;

	shm->channels = shw->numchannels;
	shm->samplebits = shw->samplebits;
	shm->samplepos = shw->samplepos;
	shm->samples = shw->samples;
	shm->speed = shw->khz;
	shm->buffer = shw->buffer;
	shm->splitbuffer = 0;
	shm->submission_chunk = 1;
	
	// End OfN

	Com_Printf("Audio driver: %s @ %d Hz\n", SDL_GetCurrentAudioDriver(), obtained.freq);

	SDL_PauseAudioDevice(audiodevid, 0);

	snd_inited = true; // <-- OfN
	return true;

fail:
	//S_SDL_Shutdown();
	SDL_Quit();
	return false;

	//===========================================================================================================================

	//return 0;
	// End OfN

/*
	// open snd_dev, confirm capability to mmap, and get size of dma buffer
	if ((i = COM_CheckParm("-snddev"))&& i < com_argc - 1)
		Q_strncpyz (snd_dev, com_argv[i + 1], sizeof(snd_dev));

    audio_fd = open(snd_dev, O_RDWR);
    if (audio_fd < 0) {
		perror(snd_dev);
        Com_Printf ("Could not open %s\n", snd_dev);
		return 0;
	}

    rc = ioctl(audio_fd, SNDCTL_DSP_RESET, 0);
    if (rc < 0) {
		perror(snd_dev);
		Com_Printf ("Could not reset %s\n", snd_dev);
		close(audio_fd);
		return 0;
	}

	if (ioctl(audio_fd, SNDCTL_DSP_GETCAPS, &caps) == -1) {
		perror(snd_dev);
        Com_Printf ("Sound driver too old\n");
		close(audio_fd);
		return 0;
	}

	if (!(caps & DSP_CAP_TRIGGER) || !(caps & DSP_CAP_MMAP)) {
		Com_Printf ("Sorry but your soundcard can't do this\n");
		close(audio_fd);
		return 0;
	}

    if (ioctl(audio_fd, SNDCTL_DSP_GETOSPACE, &info) == -1) {   
        perror("GETOSPACE");
		Com_Printf ("Um, can't do GETOSPACE?\n");
		close(audio_fd);
		return 0;
    }
   
	shm = &sn;
    shm->splitbuffer = 0;

	// set sample bits & speed

    s = getenv("QUAKE_SOUND_SAMPLEBITS");
	if (s)
		shm->samplebits = atoi(s);
	else if ((i = COM_CheckParm("-sndbits")) && i + 1 < com_argc)
		shm->samplebits = atoi(com_argv[i + 1]);

	if (shm->samplebits != 16 && shm->samplebits != 8) {
        ioctl(audio_fd, SNDCTL_DSP_GETFMTS, &fmt);
        if (fmt & AFMT_S16_LE)
			shm->samplebits = 16;
        else if (fmt & AFMT_U8)
			shm->samplebits = 8;
    }

    s = getenv("QUAKE_SOUND_SPEED");
	if (s) {
		shm->speed = atoi(s);
	} else if ((i = COM_CheckParm("-sndspeed")) && i + 1 < com_argc) {
		shm->speed = atoi(com_argv[i + 1]);
	} else {
        for (i = 0; i < sizeof(tryrates) / 4; i++)
            if (!ioctl(audio_fd, SNDCTL_DSP_SPEED, &tryrates[i])) break;
        shm->speed = tryrates[i];
    }

    s = getenv("QUAKE_SOUND_CHANNELS");
    if (s)
		shm->channels = atoi(s);
	else if (COM_CheckParm("-sndmono"))
		shm->channels = 1;
	else if (COM_CheckParm("-sndstereo"))
		shm->channels = 2;
    else 
		shm->channels = 2;

	shm->samples = info.fragstotal * info.fragsize / (shm->samplebits/8);
	shm->submission_chunk = 1;

	// memory map the dma buffer

	shm->buffer = (byte *) mmap(NULL, info.fragstotal * info.fragsize, PROT_WRITE, MAP_FILE|MAP_SHARED, audio_fd, 0);
	if (!shm->buffer) {
		perror(snd_dev);
		Com_Printf ("Could not mmap %s\n", snd_dev);
		close(audio_fd);
		return 0;
	}

	tmp = 0;
	if (shm->channels == 2)
		tmp = 1;
    rc = ioctl(audio_fd, SNDCTL_DSP_STEREO, &tmp);
    if (rc < 0) {
		perror(snd_dev);
        Com_Printf ("Could not set %s to stereo=%d", snd_dev, shm->channels);
		close(audio_fd);
        return 0;
    }
	if (tmp)
		shm->channels = 2;
	else
		shm->channels = 1;

    rc = ioctl(audio_fd, SNDCTL_DSP_SPEED, &shm->speed);
    if (rc < 0) {
		perror(snd_dev);
        Com_Printf ("Could not set %s speed to %d", snd_dev, shm->speed);
		close(audio_fd);
        return 0;
    }

    if (shm->samplebits == 16) {
        rc = AFMT_S16_LE;
        rc = ioctl(audio_fd, SNDCTL_DSP_SETFMT, &rc);
        if (rc < 0) {
			perror(snd_dev);
			Com_Printf ("Could not support 16-bit data.  Try 8-bit.\n");
			close(audio_fd);
			return 0;
		}
    } else if (shm->samplebits == 8) {
        rc = AFMT_U8;
        rc = ioctl(audio_fd, SNDCTL_DSP_SETFMT, &rc);
        if (rc < 0) {
			perror(snd_dev);
			Com_Printf ("Could not support 8-bit data.\n");
			close(audio_fd);
			return 0;
		}
    } else {
		perror(snd_dev);
		Com_Printf ("%d-bit sound not supported.", shm->samplebits);
		close(audio_fd);
		return 0;
	}

	// toggle the trigger & start her up

    tmp = 0;
    rc  = ioctl(audio_fd, SNDCTL_DSP_SETTRIGGER, &tmp);
	if (rc < 0) {
		perror(snd_dev);
		Com_Printf ("Could not toggle.\n");
		close(audio_fd);
		return 0;
	}
    tmp = PCM_ENABLE_OUTPUT;
    rc = ioctl(audio_fd, SNDCTL_DSP_SETTRIGGER, &tmp);
	if (rc < 0) {
		perror(snd_dev);
		Com_Printf ("Could not toggle.\n");
		close(audio_fd);
		return 0;
	}

	shm->samplepos = 0;

	snd_inited = 1;
	return 1;
*/
}

int SNDDMA_GetDMAPos(void) {
	struct count_info count;

	if (!snd_inited) return 0;

	/*if (ioctl(audio_fd, SNDCTL_DSP_GETOPTR, &count) == -1) {
		perror(snd_dev);
		Com_Printf ("Uh, sound dead.\n");
		close(audio_fd);
		snd_inited = 0;
		return 0;
	}*/
//	shm->samplepos = (count.bytes / (shm->samplebits / 8)) & (shm->samples-1);
//	fprintf(stderr, "%d    \r", count.ptr);
	shm->samplepos = count.ptr / (shm->samplebits / 8);

	return shm->samplepos;

}

void SNDDMA_Shutdown(void) {
	/*if (snd_inited) {
		close(audio_fd);
		snd_inited = 0;
	}*/

	if (shw)
		free(shw);

	SDL_DestroyMutex(smutex);

	SDL_Quit(); // October 2022
}

//Send sound to device if buffer isn't really the dma buffer
void SNDDMA_Submit(void) { }

/*
qboolean OnChange_SKhz(cvar_t *var, char *string)
{
	int req = atoi(string);

	if (req != 192 && req != 96 && req != 48 && req != 44 && req != 22 && req != 11 && req != 8)
	{
		Com_Printf("Not a valid sampling rate, valid values are: 192, 96, 48, 44, 22, 11 and 8\n");
		return true;
	}
	
	var->value =(float)req;

	S_ClearBuffer();

	//if (known_sfx != NULL) {
	//	int i;
	//	for (i = 0; i < num_sfx; i++) {
	//		if (known_sfx[i].buf != NULL) {
	//			Q_free(known_sfx[i].buf);
	//		}
	//	}
	//}
	//Q_free(known_sfx);
	//num_sfx = 0;

	SNDDMA_Shutdown();
	SNDDMA_Init();

	return false;
}*/