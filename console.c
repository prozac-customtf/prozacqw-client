/*
Copyright (C) 1996-1997 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
// console.c

#include "quakedef.h"
#include "keys.h"
#include <fcntl.h>

#ifdef _WIN32
#include <io.h>
#else
#include <stdio.h>
#endif


#include "ignore.h"
#include "logging.h"

#define		MINIMUM_CONBUFSIZE	(1 << 15)
#define		DEFAULT_CONBUFSIZE	(1 << 16)
#define		MAXIMUM_CONBUFSIZE	(1 << 22)

console_t	con;

int			con_ormask;
//OfN int 		con_linewidth;		// characters across screen // OfN moved to console.h
int 		con_linewidth;		// characters across screen // OfN moved to console.h // September 2022 uncommented
int			con_totallines;		// total lines in console scrollback
float		con_cursorspeed = 4;

cvar_t		_con_notifylines = { "con_notifylines", "6" };//"4"};
cvar_t		con_notifytime = { "con_notifytime","6" };//"3"};		//seconds
cvar_t		con_wordwrap = {"con_wordwrap","1"};
cvar_t		con_clearnotify = {"con_clearnotify","1"};

cvar_t con_frageventstime = { "con_frageventstime", "8" }; // OfN
cvar_t con_fragevents = { "con_fragevents", "1" }; // OfN
qboolean OnChange_con_frageventsnum(cvar_t* v, char* s);
cvar_t con_frageventsnum = { "con_frageventsnum","6", 0, OnChange_con_frageventsnum };
cvar_t con_colortext = { "con_colortext","1" };

#define	NUM_CON_TIMES 16
float		con_times[NUM_CON_TIMES];	// cls.realtime time the line was generated
										// for transparent notify lines

int			con_vislines;
int			con_notifylines;			// scan lines to clear for notify lines

#define		MAXCMDLINE	256
extern	char	key_lines[32][MAXCMDLINE];
extern	int		edit_line;
extern	int		key_linepos;

//OfN
extern cvar_t  con_colorchat;
typedef struct colorchat_item colorchat_item;
struct colorchat_item
{	colorchat_item* next;
	colorchat_item* previous;
	int column_nameoffset;
	int team_color;
	//char* startline_address;
	//char* endline_address;

	int startline;
	int endline;
};
colorchat_item* last_colorchat_item = NULL;
colorchat_item* first_colorchat_item = NULL;
//--------------------------------------------------//
// Copied from fragstats.c
/*typedef enum msgtype_s {
	mt_fragged,
	mt_frags,
	mt_tkills,
	mt_tkilled,

	mt_death,
	mt_suicide,
	mt_frag,
	mt_tkill,
	mt_flagtouch,
	mt_flagdrop,
	mt_flagcap
}
msgtype_t;*/

typedef struct fragevent_s {
	msgtype_t type;
	char killername[65]; // was 32 December 2020
	char victimname[65]; // was 32 December 2020
	char weapon_str[65]; // was 48 December 2020
	double time_spawned;
	qboolean showordead;
	qboolean noweap;
	float colorpos;
} fragevent_t;

#define NUM_FRAG_EVENTS 16

fragevent_t FragEvents[NUM_FRAG_EVENTS];
void Con_SetWhite(void);
// OfN End

qboolean	con_initialized = false;
qboolean	con_suppress = false;

FILE		*qconsole_log;

void Key_ClearTyping (void) {
	key_lines[edit_line][1] = 0;	// clear any typing
	key_linepos = 1;
}

void Con_ToggleConsole_f (void) {
	Key_ClearTyping ();

	if (key_dest == key_console) {
		if (!SCR_NEED_CONSOLE_BACKGROUND)
			key_dest = key_game;
	} else {
		key_dest = key_console;
	}

	if (con_clearnotify.value)
		Con_ClearNotify ();
}

// OfN - Clean up or reset of colorchat items
void Con_ResetColorChat()
{
	if (last_colorchat_item)
	{
		colorchat_item* pscanitem;
		pscanitem = last_colorchat_item;

		if (pscanitem->previous)
		{
			for (; last_colorchat_item; )
			{
				pscanitem = last_colorchat_item->previous;
				if (pscanitem) free(pscanitem->next);
				last_colorchat_item = pscanitem;
			}

			if (pscanitem)
				free(pscanitem);
		}
		else
			free(pscanitem);

		last_colorchat_item = NULL;
	}

	first_colorchat_item = NULL;
}
// OfN - End

void Con_Clear_f (void) {
	con.numlines = 0;
	memset (con.text, ' ', con.maxsize);
	con.display = con.current;
	Con_ResetColorChat(); // OfN
	Con_SetWhite();
}

void Con_ClearNotify (void) {
	int i;

	for (i = 0; i < NUM_CON_TIMES; i++)
		con_times[i] = 0;
}

void Con_MessageMode_f (void) {
	if (cls.state != ca_active)
		return;

	chat_team = false;
	key_dest = key_message;
	chat_buffer[0] = 0;
	chat_linepos = 0;
}

void Con_MessageMode2_f (void) {
	if (cls.state != ca_active)
		return;

	chat_team = true;
	key_dest = key_message;
	chat_buffer[0] = 0;
	chat_linepos = 0;
}

// THE COLOR CHAT EVENT - OfN
void Con_ColorChat_Event(int teamcolornum, int nameoffset, int startline) {
	int endline;

	endline = con.current;

	if (endline < startline)
		return;

	if (!last_colorchat_item) // First event
	{
		last_colorchat_item = Q_Malloc(sizeof(colorchat_item));
		last_colorchat_item->startline = startline;//startaddress;
		last_colorchat_item->endline = endline;
		last_colorchat_item->team_color = teamcolornum;
		last_colorchat_item->column_nameoffset = nameoffset;
		last_colorchat_item->previous = NULL;
		last_colorchat_item->next = NULL;
		first_colorchat_item = last_colorchat_item;
	}
	else // Not the first one
	{
		colorchat_item*	pnewitem = Q_Malloc(sizeof(colorchat_item));
		pnewitem->startline = startline;//startaddress;
		pnewitem->endline = endline;
		pnewitem->next = NULL;
		pnewitem->column_nameoffset = nameoffset;
		pnewitem->team_color = teamcolornum;
		pnewitem->previous = last_colorchat_item;
		last_colorchat_item->next = pnewitem;
		last_colorchat_item = pnewitem;
	}
}

//If the line width has changed, reformat the buffer
void Con_CheckResize (void) {
	int i, j, width, oldwidth, oldtotallines, numlines, numchars;
	char *tempbuf;

	width = (vid.width >> 3) - 2;

	if (width == con_linewidth)
		return;

	if (width < 1) { // video hasn't been initialized yet
		width = 38;
		con_linewidth = width;
		con_totallines = con.maxsize / con_linewidth;
		memset (con.text, ' ', con.maxsize);
	} else {
		oldwidth = con_linewidth;
		con_linewidth = width;
		oldtotallines = con_totallines;
		con_totallines = con.maxsize / con_linewidth;
		numlines = oldtotallines;

		if (con_totallines < numlines)
			numlines = con_totallines;

		numchars = oldwidth;

		if (con_linewidth < numchars)
			numchars = con_linewidth;

		tempbuf = Hunk_TempAlloc(con.maxsize);
		memcpy (tempbuf, con.text, con.maxsize);
		memset (con.text, ' ', con.maxsize);

		for (i = 0; i < numlines; i++) {
			for (j = 0; j < numchars; j++) {
				con.text[(con_totallines - 1 - i) * con_linewidth + j] =
					tempbuf[((con.current - i + oldtotallines) % oldtotallines) * oldwidth + j];
			}
		}

		Con_ClearNotify ();
	}

	con.current = con_totallines - 1;
	con.display = con.current;
}


char readableChars[256] = {	'.', '_' , '_' , '_' , '_' , '.' , '_' , '_' , '_' , '_' , 10 , '_' , 10 , '>' , '.' , '.',
						'[', ']', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', '_', '_', '_'};


static void Con_CreateReadableChars(void) {
	int i;

	for (i = 32; i < 127; i++)
		readableChars[i] = readableChars[128 + i] = i;
	readableChars[127] = readableChars[128 + 127] = '_';

	for (i = 0; i < 32; i++)
		readableChars[128 + i] = readableChars[i];
	readableChars[128] = '_';
	readableChars[10 + 128] = '_';
	readableChars[12 + 128] = '_';
}


static void Con_InitConsoleBuffer(console_t *conbuffer, int size) {
	con.maxsize = size;
	con.text = Hunk_AllocName(con.maxsize, "console_buffer");
	con.clr = (clrinfo_t*)Hunk_AllocName(con.maxsize * sizeof(clrinfo_t), "console_clr"); // OfN November 2020
}

void Con_Init (void) {
	int i, conbufsize;

	if (dedicated)
		return;

	if (COM_CheckParm("-condebug"))
		qconsole_log = fopen(va("%s/qw/qconsole.log",com_basedir), "a");

	if ((i = COM_CheckParm("-conbufsize")) && i + 1 < com_argc) {
		conbufsize = Q_atoi(com_argv[i + 1]) << 10;
		conbufsize = bound (MINIMUM_CONBUFSIZE , conbufsize, MAXIMUM_CONBUFSIZE);
	} else {
		conbufsize = DEFAULT_CONBUFSIZE;
	}
	Con_InitConsoleBuffer(&con, conbufsize);

	con_linewidth = -1;
	Con_CheckResize ();
	Con_SetWhite(); // OfN - November 2020

	Con_CreateReadableChars();	

	con_initialized = true;
	Com_Printf ("Console initialized\n");

	Cvar_SetCurrentGroup(CVAR_GROUP_CONSOLE);
	// register our commands and cvars
	Cvar_Register (&_con_notifylines);
	Cvar_Register (&con_notifytime);
	Cvar_Register (&con_wordwrap);
	Cvar_Register (&con_clearnotify);

	Cvar_Register(&con_frageventstime); // OfN
	Cvar_Register(&con_fragevents); // OfN
	Cvar_Register(&con_frageventsnum); // OfN
	Cvar_Register(&con_colortext); // OfN

	Cvar_ResetCurrentGroup();

	Cmd_AddCommand ("toggleconsole", Con_ToggleConsole_f);
	Cmd_AddCommand ("messagemode", Con_MessageMode_f);
	Cmd_AddCommand ("messagemode2", Con_MessageMode2_f);
	Cmd_AddCommand ("clear", Con_Clear_f);

	// OfN - Zeromem all frag events
	memset(&FragEvents, 0, sizeof(fragevent_t) * NUM_FRAG_EVENTS);
}

void Con_Shutdown (void) {
	if (qconsole_log)
		fclose(qconsole_log);

	// OfN
	Con_ResetColorChat();	
}

void Con_SetColor(int idx_from, int count, int c); // November 2020
int HexToInt(char c); // November 2020
color_t RGBA_TO_COLOR(byte r, byte g, byte b, byte a); // November 2020

void Con_Linefeed (void) {
	/* ORIGINAL CODE
	con.x = 0;
	if (con.display == con.current)
		con.display++;
	con.current++;
	if (con.numlines < con_totallines)
		con.numlines++;
	memset (&con.text[(con.current%con_totallines)*con_linewidth], ' ', con_linewidth);*/

	int idx, i;
	con.x = 0;
	//con.x = con_margin;    // kazik
	if (con.display == con.current)
		con.display++;
	con.current++;
	if (con.numlines < con_totallines)
		con.numlines++;
	idx = (con.current % con_totallines) * con_linewidth;
	for (i = 0; i < con_linewidth; i++)
		con.text[idx + i] = ' ';
	Con_SetColor(idx, con_linewidth, COLOR_WHITE);

	// OfN - Moved November 2020 from Con_Print() call to linefeed
	if (con.current >= 0)
		con_times[con.current % NUM_CON_TIMES] = cls.realtime;
}

const int COLOR_WHITE = 0xFFFFFFFF; // ADDED November 2020

//Handles cursor positioning, line wrapping, etc
void Con_Print (char *txt) {
	int y, c, l, mask;
	static int cr;

	int color = COLOR_WHITE; // November 2020
	int idx, r, g, b, d; // November 2020
	char* s;

	if (qconsole_log) {
		fprintf(qconsole_log, "%s", txt);
		fflush(qconsole_log);
	}
	if (Log_IsLogging()) {
		if (log_readable.value) {
			char *s, tempbuf[4096];	

			Q_strncpyz(tempbuf, txt, sizeof(tempbuf));
			for (s = tempbuf; *s; s++)
				*s = readableChars[(unsigned char) *s];
			Log_Write(tempbuf);	
		} else {
			Log_Write(txt);	
		}
	}

	if (!con_initialized || con_suppress)
		return;

	if (txt[0] == 1 || txt[0] == 2)	{
		mask = 128;		// go to colored text
		txt++;
	} else {
		mask = 0;
	}

	while ((c = *txt)) {
		// count word length
		/* ORIGINAL CODE
		for (l = 0; l < con_linewidth; l++) {
			char d = txt[l] & 127;
			if ((con_wordwrap.value && (!txt[l] || d == 0x09 || d == 0x0D || d == 0x0A || d == 0x20)) ||
				(!con_wordwrap.value && txt[l] <= ' ')
			)
				break;
		}*/

		// get color modificator if any
		if (*txt == '&') {
			if (txt[1] == 'c' && txt[2] && txt[3] && txt[4]) {
				r = HexToInt(txt[2]);
				g = HexToInt(txt[3]);
				b = HexToInt(txt[4]);
				if (r >= 0 && g >= 0 && b >= 0) {
					color = RGBA_TO_COLOR(255 * r / 16, 255 * g / 16, 255 * b / 16, 255);
					txt += 5;
					continue; // we got color, get now normal char
				}
			}
			else if (txt[1] == 'r') {
				color = COLOR_WHITE;
				txt += 2;
				continue; // we got color, get now normal char
			}
		}

		// count word length
		for (s = txt, l = 0; s[0] && l < con_linewidth;) {
			// skip color, not count in word length
			if (*s == '&') {
				if (s[1] == 'c' && s[2] && s[3] && s[4]) {
					r = HexToInt(s[2]);
					g = HexToInt(s[3]);
					b = HexToInt(s[4]);
					if (r >= 0 && g >= 0 && b >= 0) {
						s += 5;
						continue; // we got color, get now normal char
					}
				}
				else if (s[1] == 'r') {
					s += 2;
					continue; // we got color, get now normal char
				}
			}

			//d = (s[0] & ~128); // EZQUAKE original line
			d = (s[0] & 127);
			if ((con_wordwrap.value && (!d || d == 0x09 || d == 0x0D || d == 0x0A || d == 0x20))
				|| (!con_wordwrap.value && d <= 32) // 32 is a space as well as 0x20
				)
				break;

			l++; // increase word length
			s++; // get next char
		}
		// End Novemeber 2020 from ezquake

		// word wrap
		if (l != con_linewidth && con.x + l > con_linewidth)
			con.x = 0;

		txt++;

		if (cr) {
			con.current--;
			cr = false;
		}

		if (!con.x) {
			Con_Linefeed ();
		// mark time for transparent overlay
			/*if (con.current >= 0)
				con_times[con.current % NUM_CON_TIMES] = cls.realtime;*/ // November 2020 Moved to Con_LineFeed();
		}

		switch (c) {
			case '\n':
				con.x = 0;
				break;

			case '\r':
				con.x = 0;
				cr = 1;
				break;

			default:	// display character and advance
				/* ORIGINAL CODE
				y = con.current % con_totallines;
				con.text[y * con_linewidth+con.x] = c | mask | con_ormask;
				con.x++;
				if (con.x >= con_linewidth)
					con.x = 0;*/

				if (con.x >= con_linewidth)
					Con_Linefeed();
				y = con.current % con_totallines;
				idx = y * con_linewidth + con.x;
				con.text[idx] = c | mask | con_ormask;//c | (c <= 0x7F ? mask | con_ormask : 0);	// only apply mask if in 'standard' charset
				memset(&con.clr[idx], 0, sizeof(clrinfo_t)); // zeroing whole struct
				con.clr[idx].c = color;
				//con.clr[idx].i = idx; // no, that not stupid :P
				con.x++;
				
				break;
		}
	}
}


/*
==============================================================================
DRAWING
==============================================================================
*/

//The input line scrolls horizontally if typing goes beyond the right edge
void Con_DrawInput (void) {
	int i;
	char *text, temp[MAXCMDLINE];

	if (key_dest != key_console && cls.state == ca_active)
		return;		// don't draw anything (always draw if not active)

	text = strcpy (temp, key_lines[edit_line]);

	// fill out remainder with spaces
	for (i = strlen(text); i < MAXCMDLINE; i++)
		text[i] = ' ';

	// add the cursor frame
	if ( (int)(curtime*con_cursorspeed) & 1 )
		text[key_linepos] = 11;

	//	prestep if horizontally scrolling
	if (key_linepos >= con_linewidth)
		text += 1 + key_linepos - con_linewidth;

	// draw it
	Draw_String(8, con_vislines-22, text);
}

extern void Draw_TeamCharacter(int teamno, int x, int y, int num);
extern void Draw_ColorParseCharacter(int teamno, int x, int y, int num, clrinfo_t info);

//Draws the last few lines of output transparently over the game top
void Con_DrawNotify (void) {
	int x, v, skip, maxlines, i;
	char *text, *s;
	float time;

	int idx; // November 2020

	maxlines = _con_notifylines.value;
	if (maxlines > NUM_CON_TIMES)
		maxlines = NUM_CON_TIMES;
	if (maxlines < 0)
		maxlines = 0;

	colorchat_item* current_cc_scan, *rareitem;
	current_cc_scan = last_colorchat_item;
	rareitem = last_colorchat_item;
	int current_teamcolor = -1;
	
	if (con_colorchat.value && rareitem)
	{		
		int theline;
		///for (i = con.current; i > con.current - maxlines + 1; i--) {
		for (i = con.current; i > con.current - maxlines; i--) {
			//if (i < 0)
				//continue;
			//if (numlines > numprinted) // DOES ANYTHING?
				//break;
			//if (numprinted <= 1)
				//break;

			time = con_times[i % NUM_CON_TIMES]; // Same as [(i-NUM_CON_TIMES) % NUM_CON_TIMES] ?
			if (time == 0)
				break;//continue;
						
			time = cls.realtime - time;
			if (time > con_notifytime.value)
				break;//continue;
			
			//text = con.text + (i % con_totallines)*con_linewidth;
			theline = i;// % con_totallines;

			if (theline == rareitem->startline)
			{
				if (rareitem->previous)
					rareitem = rareitem->previous;
			} // check endline
			else if (theline == rareitem->endline && rareitem->startline < con.current - maxlines + 1)
			{
				current_teamcolor = rareitem->team_color;
			}
		}

		if (rareitem->next)// && !(rareitem == first_colorchat_item && rareitem->next == last_colorchat_item))// && rareitem != first_colorchat_item) // FIXME: Comment all this and check if first item works as it should
			rareitem = rareitem->next;

		current_cc_scan = rareitem;

		// Fixme: - DOESN't WORK PROPERLY -> this for first item not to bug out at at start
		//if (current_cc_scan->previous == first_colorchat_item)
			//current_cc_scan = first_colorchat_item;
	}

	v = 0;
	for (i = con.current-maxlines + 1; i <= con.current; i++) {
		if (i < 0)
			continue;
		time = con_times[i % NUM_CON_TIMES];
		if (time == 0)
			continue;
		time = cls.realtime - time;
		if (time > con_notifytime.value)
			continue;
		//text = con.text + (i % con_totallines)*con_linewidth;

		idx = (i % con_totallines) * con_linewidth; // November 2020
		text = con.text + idx;  // November 2020

		clearnotify = 0;
		scr_copytop = 1;

		if (!con_colorchat.value)
		{
			for (x = 0; x < con_linewidth; x++)
				//Draw_Character((x + 1) << 3, v, text[x]);
				Draw_ColorParseCharacter(-16, (x + 1) << 3, v, text[x], con.clr[idx + x]);
		}
		else
		{// OfN -  add color chat support
			int theline = i;// % con_totallines;
			if (current_cc_scan)
			{
				if (theline == current_cc_scan->startline) // MAIN STARTLINE
				{
					for (x = 0; x < con_linewidth; x++)
					{
						if (x <= current_cc_scan->column_nameoffset)
							Draw_Character((x + 1) << 3, v, text[x]);
						else
							//Draw_TeamCharacter(current_cc_scan->team_color, (x + 1) << 3, v, text[x]);
							Draw_ColorParseCharacter(current_cc_scan->team_color, (x + 1) << 3, v, text[x], con.clr[idx + x]);
					}

					current_teamcolor = -1;

					if (theline == current_cc_scan->endline)
					{
						current_teamcolor = -1;
						if (current_cc_scan->next)
							current_cc_scan = current_cc_scan->next;
					}
					else
						current_teamcolor = current_cc_scan->team_color;
				}
				else if (current_teamcolor != -1) // MAIN MEANWHILE
				{
					for (x = 0; x < con_linewidth; x++)
						//Draw_TeamCharacter(current_cc_scan->team_color, (x + 1) << 3, v, text[x]);
						Draw_ColorParseCharacter(current_cc_scan->team_color, (x + 1) << 3, v, text[x], con.clr[idx + x]);
				
					if (theline == current_cc_scan->endline)
					{
						current_teamcolor = -1;
						if (current_cc_scan->next)
							current_cc_scan = current_cc_scan->next;
					}
					else
						current_teamcolor = current_cc_scan->team_color;
				} // MAIN
				else if (theline == current_cc_scan->endline) // MAIN ENDLINE
				{
					for (x = 0; x < con_linewidth; x++)
						//Draw_TeamCharacter(current_cc_scan->team_color, (x + 1) << 3, v, text[x]);
						Draw_ColorParseCharacter(current_cc_scan->team_color, (x + 1) << 3, v, text[x], con.clr[idx + x]);
					
					current_teamcolor = -1;
					if (current_cc_scan->next)
						current_cc_scan = current_cc_scan->next;
				}
				else
				{
					for (x = 0; x < con_linewidth; x++)
						//Draw_Character((x + 1) << 3, v, text[x]);
						Draw_ColorParseCharacter(-16, (x + 1) << 3, v, text[x], con.clr[idx + x]);
				}				
			}
			else
			{
				for (x = 0; x < con_linewidth; x++)
					//Draw_Character((x + 1) << 3, v, text[x]);
					Draw_ColorParseCharacter(-16, (x + 1) << 3, v, text[x], con.clr[idx + x]);
			}
		}		
		
		v += 8;
	}


	if (key_dest == key_message) {
		char temp[MAXCMDLINE + 1];

		clearnotify = 0;
		scr_copytop = 1;

		if (chat_team) {
			Draw_String (8, v, "say_team:");
			skip = 11;
		} else {
			Draw_String (8, v, "say:");
			skip = 5;
		}

		// FIXME: clean this up
		s = strcpy (temp, chat_buffer);

		// add the cursor frame
		if ((int) (curtime * con_cursorspeed) & 1) {
			if (chat_linepos == strlen(s))
				s[chat_linepos+1] = '\0';
			s[chat_linepos] = 11;
		}

		// prestep if horizontally scrolling
		if (chat_linepos + skip >= (vid.width >> 3))
			s += 1 + chat_linepos + skip - (vid.width >> 3);

		x = 0;
		while (s[x] && x+skip < (vid.width>>3)) {
			Draw_Character ( (x+skip)<<3, v, s[x]);
			x++;
		}
		v += 8;
	}

	if (v > con_notifylines)
		con_notifylines = v;
}

//Draws the console with the solid background
void Con_DrawConsole (int lines) {
	int i, j, x, y, n, rows, row;
	char *text, dlbar[1024];
#ifdef GLQUAKE
	extern cvar_t gl_conalpha;
#endif

	int idx;

	if (lines <= 0)
		return;

	// draw the background
	Draw_ConsoleBackground (lines);

	// draw the text
	con_vislines = lines;

	// changed to line things up better
	rows = (lines - 22) >> 3;		// rows of text to draw

	y = lines - 30;

	row = con.display;

	//OfN
	colorchat_item* current_cc_scan;
	current_cc_scan = last_colorchat_item;
	int current_teamcolor = -1;

	// draw from the bottom up
	if (con.display != con.current) {
	// draw arrows to show the buffer is backscrolled
		for (x = 0; x < con_linewidth; x += 4)
			Draw_Character ((x + 1) << 3, y, '^');

		y -= 8;
		rows--;
		row--;

		// OfN - Skip items
		if (con_colorchat.value && current_cc_scan)
		{
			for (int line = con.numlines; line > 0 && current_cc_scan; line--)
			{
				//text = con.text + ((con.display + line - 1) % con_totallines)*con_linewidth;
				int theline = (con.display + line - 1);

				if (theline == current_cc_scan->endline)
					current_teamcolor = current_cc_scan->team_color;
				
				if (theline == current_cc_scan->startline)
				{
					current_cc_scan = current_cc_scan->previous;
					current_teamcolor = -1;
				}
			}
		}
	}	
	
	for (i = 0; i < rows; i++, y -= 8, row--) {
		if (row < 0)
			break;
		if (con.current - row >= con_totallines)
			break;		// past scrollback wrap point

		//text = con.text + (row % con_totallines)*con_linewidth; // <-- Original code

		idx = (row % con_totallines) * con_linewidth; // November 2020
		text = con.text + idx; // November 2020

		// OfN - Color chat
		if (con_colorchat.value)
		{
			//int currentline =
			if (current_cc_scan)
			{
				if (row == current_cc_scan->endline)
				{
					for (x = 0; x < con_linewidth; x++)
					{
						if (row == current_cc_scan->startline)
						{
							if (x <= current_cc_scan->column_nameoffset)
								Draw_Character((x + 1) << 3, y, text[x]);
							else
								//Draw_TeamCharacter(current_cc_scan->team_color, (x + 1) << 3, y, text[x]);
								Draw_ColorParseCharacter(current_cc_scan->team_color, (x + 1) << 3, y, text[x], con.clr[idx + x]);
						}
						else
						{
							//Draw_TeamCharacter(current_cc_scan->team_color, (x + 1) << 3, y, text[x]);
							Draw_ColorParseCharacter(current_cc_scan->team_color, (x + 1) << 3, y, text[x], con.clr[idx + x]);
						}
					}

					if (row == current_cc_scan->startline)
					{
						current_teamcolor = -1;
						if (current_cc_scan->previous)
							current_cc_scan = current_cc_scan->previous;
					}
					else
						current_teamcolor = current_cc_scan->team_color;					
				}
				else if (current_teamcolor != -1) // TOCHECK: Should we be using current_teancolor instead of current_cc_scan->team_color?
				{
					for (x = 0; x < con_linewidth; x++)
					{
						if (row == current_cc_scan->startline)
						{
							if (x <= current_cc_scan->column_nameoffset)
								Draw_Character((x + 1) << 3, y, text[x]);
							else
								//Draw_TeamCharacter(current_cc_scan->team_color, (x + 1) << 3, y, text[x]);
								Draw_ColorParseCharacter(current_cc_scan->team_color, (x + 1) << 3, y, text[x], con.clr[idx + x]);
						}
						else
						{                                    // this?
							//Draw_TeamCharacter(current_cc_scan->team_color, (x + 1) << 3, y, text[x]);
							Draw_ColorParseCharacter(current_cc_scan->team_color, (x + 1) << 3, y, text[x], con.clr[idx + x]);
						}
					}

					if (row == current_cc_scan->startline)
					{
						current_teamcolor = -1;
						if (current_cc_scan->previous)
							current_cc_scan = current_cc_scan->previous;
					}
					else
						current_teamcolor = current_cc_scan->team_color;
				}
				else if (row == current_cc_scan->startline)
				{
					for (x = 0; x < con_linewidth; x++)
					{
						if (x <= current_cc_scan->column_nameoffset)
							Draw_Character((x + 1) << 3, y, text[x]);
						else
							//Draw_TeamCharacter(current_cc_scan->team_color, (x + 1) << 3, y, text[x]);
							Draw_ColorParseCharacter(current_cc_scan->team_color, (x + 1) << 3, y, text[x], con.clr[idx + x]);
					}

					current_teamcolor = -1;

					if (current_cc_scan->previous)
						current_cc_scan = current_cc_scan->previous;
				}
				else // Normal non-team <<-- Correct comment?
				{
					for (x = 0; x < con_linewidth; x++)
						//Draw_Character((x + 1) << 3, y, text[x]);
						Draw_ColorParseCharacter(-16, (x + 1) << 3, y, text[x], con.clr[idx+x]);
				}
			}
			else
			{
				for (x = 0; x < con_linewidth; x++)
					//Draw_Character((x + 1) << 3, y, text[x]);
					Draw_ColorParseCharacter(-16, (x + 1) << 3, y, text[x], con.clr[idx + x]);
			}
		}
		else // Original Code follows
		{
			for (x = 0; x < con_linewidth; x++)
				//Draw_Character((x + 1) << 3, y, text[x]);
				Draw_ColorParseCharacter(-16, (x + 1) << 3, y, text[x], con.clr[idx + x]);
		}		
	}

	// draw the download bar
	// figure out width
	if (cls.download) {
		if ((text = strrchr(cls.downloadname, '/')) != NULL)
			text++;
		else
			text = cls.downloadname;

		x = con_linewidth - ((con_linewidth * 7) / 40);
		y = x - strlen(text) - 8;
		i = con_linewidth/3;
		if (strlen(text) > i) {
			y = x - i - 11;
			Q_strncpyz (dlbar, text, i+1);
			strcat(dlbar, "...");
		} else {
			strcpy(dlbar, text);
		}
		strcat(dlbar, ": ");
		i = strlen(dlbar);
		dlbar[i++] = '\x80';
		// where's the dot go?
		if (cls.downloadpercent == 0)
			n = 0;
		else
			n = y * cls.downloadpercent / 100;

		for (j = 0; j < y; j++)
			if (j == n)
				dlbar[i++] = '\x83';
			else
				dlbar[i++] = '\x81';
		dlbar[i++] = '\x82';
		dlbar[i] = 0;

		sprintf(dlbar + strlen(dlbar), " %02d%%", cls.downloadpercent);

		// draw it
		y = con_vislines - 22 + 8;
		Draw_String (8, y, dlbar);
	}

	// draw the input prompt, user text, and cursor if desired
	Con_DrawInput ();
}

//===========================================================================================================================
// OfN - Gonna code this my own from scratch instead of copying and paste a lot of code I don't understand from ezquake

void Con_FireFragEvent(msgtype_t type, char* killer, char* victim, char* weapon, qboolean noweap)
{
	if (!con_fragevents.value)
		return;
	
	// Move down all the items by one
	for (int i = (int)con_frageventsnum.value - 1; i > 0; i--)
	{
		if (!FragEvents[i - 1].showordead)
		{
			FragEvents[i].showordead = false;
			continue;
		}

		FragEvents[i].type = FragEvents[i-1].type;

		strcpy(FragEvents[i].killername, FragEvents[i-1].killername);
		strcpy(FragEvents[i].victimname, FragEvents[i-1].victimname);
		strcpy(FragEvents[i].weapon_str, FragEvents[i-1].weapon_str);	

		FragEvents[i].time_spawned = FragEvents[i-1].time_spawned;
		FragEvents[i].noweap = FragEvents[i - 1].noweap;

		FragEvents[i].colorpos = FragEvents[i - 1].colorpos;

		FragEvents[i].showordead = FragEvents[i-1].showordead;				
	}

	// Create new item as first in list
	FragEvents[0].type = type;

	strcpy(FragEvents[0].killername, killer);
	strcpy(FragEvents[0].victimname, victim);
	strcpy(FragEvents[0].weapon_str, weapon);

	FragEvents[0].time_spawned = cls.realtime;
	FragEvents[0].noweap = noweap;

	FragEvents[0].colorpos = 0;

	FragEvents[0].showordead = true;
}

void Con_CheckFragEventOffs(void)
{
	for (int i = 0; i < (int)con_frageventsnum.value; i++)
	{
		if (!FragEvents[i].showordead)
			break;

		if (FragEvents[i].time_spawned + con_frageventstime.value < cls.realtime)
		{
			FragEvents[i].showordead = false;
			break;
		}
	}
}

#define FEA_RAMPUP_TIME     0.5f
#define FEA_RAMPDOWN_TIME   1.0f

float Get_FragEvent_Alpha(fragevent_t* fevent) 
{
	float tfloat = 1.0f;

	float elapsed = cls.realtime - fevent->time_spawned;

	// Is it done fade in?
	if (elapsed > FEA_RAMPUP_TIME)
	{
		// went to fade out?
		if (elapsed > (float)(con_frageventstime.value) - (float)(FEA_RAMPDOWN_TIME))
		{
			// Calculate fade out
			tfloat = (con_frageventstime.value - (elapsed))/(float)(FEA_RAMPDOWN_TIME);
		}
		else
		{
			tfloat = 1.0f; // Done rampup and too soon for rampdown, Full alpha
		}
	}
	else // Normal fade in
	{
		tfloat = (elapsed) / FEA_RAMPUP_TIME;
	}

	return tfloat;
}

void Draw_FragEventSpecial(int x, int y, char* str, byte r, byte g, byte b, byte r2, byte g2, byte b2, float* colorpos, float eventalpha);
extern void Draw_ModulatedStringAlpha(int x, int y, char* str, float red, float green, float blue, float alpha);

void Con_DrawFragEvents(void)
{
	if (!con_fragevents.value)
		return;

	int fullheight = (int)con_frageventsnum.value * 8 *4;
	float FragEventAlpha = 0.0f;

	for (int i = 0; i < (int)con_frageventsnum.value; i++)
	{
		if (!FragEvents[i].showordead)
			break;

		int posy = ( ((vid.conheight / 2) - (fullheight/2)) +i*8*4)- 4;

		FragEventAlpha = Get_FragEvent_Alpha(&FragEvents[i]);
		
		switch (FragEvents[i].type)
		{
			case mt_suicide:
				Draw_ModulatedStringAlpha(vid.conwidth - ((strlen(FragEvents[i].victimname) + 1) * 8), posy, FragEvents[i].victimname,1,1,1, FragEventAlpha);
				Draw_ModulatedStringAlpha(vid.conwidth - (11 * 8), posy + 8, "[        ]",1,1,1,FragEventAlpha);
				Draw_FragEventSpecial(vid.conwidth - (10 * 8), posy + 8, "suicides", 0xff, 0x00, 0x00, 0xff, 0x80, 0x80, &FragEvents[i].colorpos, FragEventAlpha);
				
				if (!FragEvents[i].noweap)
					Draw_ModulatedStringAlpha(vid.conwidth - ((strlen(FragEvents[i].weapon_str) + 1) * 8), posy+16, FragEvents[i].weapon_str,1,1,1,FragEventAlpha);

				break;
			case mt_frags:
			case mt_fragged:
				Draw_ModulatedStringAlpha(vid.conwidth - ((strlen(FragEvents[i].killername) + 1) * 8), posy, FragEvents[i].killername,1,1,1,FragEventAlpha);
				
				if (FragEvents[i].noweap)
				{
					Draw_ModulatedStringAlpha(vid.conwidth - (15 * 8), posy + 8, "[            ]",1,1,1,FragEventAlpha);
					Draw_FragEventSpecial(vid.conwidth - (14 * 8), posy + 8, "killed/frags", 0x00, 0xBB, 0x44, 0x80, 0xFF, 0xA8, &FragEvents[i].colorpos, FragEventAlpha);
				}
				else 
				{
					Draw_ModulatedStringAlpha(vid.conwidth - (strlen(FragEvents[i].weapon_str)+3)*8, posy + 8, "[",1,1,1,FragEventAlpha);
					Draw_ModulatedStringAlpha(vid.conwidth - 2*8, posy + 8, "]",1,1,1,FragEventAlpha);
					Draw_FragEventSpecial(vid.conwidth - (strlen(FragEvents[i].weapon_str) + 2)*8, posy + 8, FragEvents[i].weapon_str, 0x00, 0xBB, 0x44, 0x80, 0xFF, 0xA8, &FragEvents[i].colorpos, FragEventAlpha);
				}

				Draw_ModulatedStringAlpha(vid.conwidth - ((strlen(FragEvents[i].victimname) + 1) * 8), posy + 16, FragEvents[i].victimname,1,1,1,FragEventAlpha);
				break;
			case mt_tkilled:
			case mt_tkills:
				Draw_ModulatedStringAlpha(vid.conwidth - ((strlen(FragEvents[i].killername) + 1) * 8), posy, FragEvents[i].killername,1,1,1,FragEventAlpha);

				if (FragEvents[i].noweap)
				{
					Draw_ModulatedStringAlpha(vid.conwidth - (12 * 8), posy + 8, "[         ]",1,1,1,FragEventAlpha);
					Draw_FragEventSpecial(vid.conwidth - (11 * 8), posy + 8, "teamkills", 0xff, 0x66, 0x00, 0xff, 0x99, 0x22, &FragEvents[i].colorpos, FragEventAlpha);
				}
				else   
				{
					Draw_ModulatedStringAlpha(vid.conwidth - (strlen(FragEvents[i].weapon_str) + 3) * 8, posy + 8, "[",1,1,1,FragEventAlpha);
					Draw_ModulatedStringAlpha(vid.conwidth - 2 * 8, posy + 8, "]",1,1,1,FragEventAlpha);
					Draw_FragEventSpecial(vid.conwidth - (strlen(FragEvents[i].weapon_str) + 2) * 8, posy + 8, FragEvents[i].weapon_str, 0xff, 0x66, 0x00, 0xff, 0x99, 0x22, &FragEvents[i].colorpos, FragEventAlpha);
				}

				Draw_ModulatedStringAlpha(vid.conwidth - ((strlen(FragEvents[i].victimname)+1)* 8), posy + 16, FragEvents[i].victimname,1,1,1,FragEventAlpha);
				break;
			case mt_death:
				Draw_ModulatedStringAlpha(vid.conwidth - ((strlen(FragEvents[i].victimname) + 1) * 8), posy, FragEvents[i].victimname,1,1,1,FragEventAlpha);
				Draw_ModulatedStringAlpha(vid.conwidth - (16 * 8), posy + 8, "[             ]",1,1,1,FragEventAlpha);
				Draw_FragEventSpecial(vid.conwidth - (15 * 8), posy + 8, "regular death", 0xff, 0x00, 0x00, 0xff, 0x80, 0x80, &FragEvents[i].colorpos, FragEventAlpha);

				if (!FragEvents[i].noweap)
					Draw_ModulatedStringAlpha(vid.conwidth - ((strlen(FragEvents[i].weapon_str) + 1) * 8), posy + 16, FragEvents[i].weapon_str,1,1,1,FragEventAlpha);
				break;
			default:
				break;
		}
		
	}
}

qboolean OnChange_con_frageventsnum(cvar_t* v, char* s) {

	float i = strtof(s, NULL);
	int integert = (int)i;

	if (integert < 1 || integert > NUM_FRAG_EVENTS)
		return true;

	for (int z = 0; z < NUM_FRAG_EVENTS; z++)
		FragEvents[z].showordead = false;

	return false;
}

#define DFES_STEPPY_COLOR_SMALL 0.025f
#define DFES_STEPPY_COLOR_BIG 0.1f

void Draw_FragEventSpecial(int x, int y, char* str, byte r, byte g, byte b, byte r2, byte g2, byte b2, float* colorpos, float eventalpha)
{
	char tmpstr[2];
	float multcolor = *colorpos;
	float rf, gf, bf;

	for (; *str; str++)
	{
		tmpstr[0] = str[0];
		tmpstr[1] = '\0';
		
		if (tmpstr[0] == ' ')
		{
			x += 8;
			continue;
		}

		if (multcolor < 1)
		{
			rf = (r + (r2-r) * multcolor) / 255;
			gf = (g + (g2-g) * multcolor) / 255;
			bf = (b + (b2-b) * multcolor) / 255;
		}
		else
		{
			rf = (r + (r2-r) * (1-(multcolor -1))) / 255;
			gf = (g + (g2-g) * (1-(multcolor -1))) / 255;
			bf = (b + (b2-b) * (1-(multcolor -1))) / 255;
		}

		Draw_ModulatedStringAlpha(x, y, tmpstr, rf, gf, bf, eventalpha);

		x += 8;		

		multcolor += DFES_STEPPY_COLOR_BIG;
		if (multcolor > 2)
			multcolor -= 2;
	}

	*colorpos += DFES_STEPPY_COLOR_SMALL;
	if (*colorpos > 2)
		*colorpos -= 2;
}

//==========================================================================================
// Functions copied from ezquake for colored text

color_t RGBA_TO_COLOR(byte r, byte g, byte b, byte a)
{
	return ((r << 0) | (g << 8) | (b << 16) | (a << 24)) & 0xFFFFFFFF;
}

void Con_SetColor(int idx_from, int count, int c) {
	int i;

	if (idx_from < 0 || idx_from >= con.maxsize || count < 0 || idx_from + count > con.maxsize)
		Sys_Error("Con_SetColor: wrong idx");

	for (i = idx_from; i < count; i++) {
		memset(&con.clr[i], 0, sizeof(clrinfo_t)); // zeroing whole struct
		con.clr[i].c = c;
		//con.clr[i].i = i;
	}
}

void Con_SetWhite(void) {
	// no need for memset(), Con_SetColor() do it too
	//	memset (con.clr, 0, con.maxsize * sizeof(clrinfo_t)); // set whole struct array to zero

	Con_SetColor(0, con.maxsize, COLOR_WHITE); // set white color
}

int HexToInt(char c)
{
	if (isdigit(c))
		return c - '0';
	else if (c >= 'a' && c <= 'f')
		return 10 + c - 'a';
	else if (c >= 'A' && c <= 'F')
		return 10 + c - 'A';
	else
		return -1;
}