/*
Copyright (C) 1996-1997 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
// sys.h -- non-portable functions

#ifdef _WIN32
#define Sys_MSleep(x) Sleep(x)
#else
#define Sys_MSleep(x) usleep((x) * 1000)
#endif

// file IO
int	Sys_FileTime (char *path);
void Sys_mkdir (char *path);

// memory protection
void Sys_MakeCodeWriteable (unsigned long startaddr, unsigned long length);

// an error will cause the entire program to exit
void Sys_Error (char *error, ...);

// send text to the console
void Sys_Printf (char *fmt, ...);

void Sys_Quit (void);

double Sys_DoubleTime (void);

char *Sys_ConsoleInput (void);

// Perform Key_Event () callbacks until the input que is empty
void Sys_SendKeyEvents (void);

void Sys_LowFPPrecision (void);
void Sys_HighFPPrecision (void);
void Sys_SetFPCW (void);

void Sys_Init (void);

char *Sys_GetClipboardData(void);
void Sys_CopyToClipboard(char *);

// Pulseczar fix with memmove
#ifndef _WIN32

// PZ: for Ubuntu/Linux version, replace stricmp and strcmpi with strcasecmp
#define stricmp strcasecmp
#define strcmpi strcasecmp
// PZ: So, apparently you aren't supposed to use memcpy(), strcpy(), and strncpy() anywhere that memory
// might overlap -- meaning, anywhere that destination and source strings might overlap in memory.
// You are supposed to use memmove() when you aren't sure memory won't overlap.
// These functions are currently being used a lot on overlapping memory, and it's causing all kinds of
// crashes/errors. Rather than figure out which ones are safe, I'm just making them all use memmove().
// From what I have read, any performance hit when using memmove() is negligible.
#define memcpy memmove
// Adding this just in case it's needed.
//    #define Q_memcpy memmove
// can't do this one in the makefile.. doesn't undestand what to do with 'strlen'
//#define strcpy(a,b) (char*)memmove((void*)a,(const void*)b,strlen(b)+1)
#define strncpy memmove
//#define Q_strncpyz(a,b,s) MYQ_Strncpyz()
char* mystrcpy(char* a, char* b); // Function defined in common.c

#define strcpy(a,b) (char*)mystrcpy(a,b)
#else
// PZ: So, apparently you aren't supposed to use memcpy(), strcpy(), and strncpy() anywhere that memory
// might overlap -- meaning, anywhere that destination and source strings might overlap in memory.
// You are supposed to use memmove() when you aren't sure memory won't overlap.
// These functions are currently being used a lot on overlapping memory, and it's causing all kinds of
// crashes/errors. Rather than figure out which ones are safe, I'm just making them all use memmove().
// From what I have read, any performance hit when using memmove() is negligible.
#define memcpy memmove
// Adding this just in case it's needed.
//    #define Q_memcpy memmove
// can't do this one in the makefile.. doesn't undestand what to do with 'strlen'
char* mystrcpy(char* a, char* b); // Function defined in common.c

#define strcpy(a,b) (char*)mystrcpy(a,b)

#define strncpy memmove
#endif
